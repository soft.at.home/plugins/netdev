# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.11.13 - 2024-11-18(12:15:25 +0000)

### Other

- - [netdev] Change network stats size from 32 to 64 to avoid overflow of Rx Tx Bytes values.

## Release v1.11.12 - 2024-10-24(10:22:46 +0000)

### Other

- Custom neighbor table notifications for gmap-mod-ethernet-dev

## Release v1.11.11 - 2024-09-10(07:08:41 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v1.11.10 - 2024-07-23(07:38:34 +0000)

### Fixes

- Better shutdown script

## Release v1.11.9 - 2024-06-17(15:40:28 +0000)

### Fixes

- - [Cellular] Set netdev operational flags

## Release v1.11.8 - 2024-04-25(11:03:53 +0000)

### Fixes

- Netlink interface notifications and operational state values

## Release v1.11.7 - 2024-04-22(15:06:42 +0000)

### Fixes

- NetDev shows duplicated Neigh instance

## Release v1.11.6 - 2024-04-17(14:48:28 +0000)

### Fixes

- [netdev-plugin] Make it compatible with openwrt23.05 toolchain

## Release v1.11.5 - 2024-04-10(10:00:17 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.11.4 - 2024-03-20(13:50:56 +0000)

### Fixes

- wrong index numbers after pwhm restart

## Release v1.11.3 - 2024-01-31(15:50:16 +0000)

### Fixes

- [UpLinkMonitor][AP config][NetDev] UpLinkMonitor has a wrong "NetDevName" after switching from phys. port.

## Release v1.11.2 - 2023-10-13(14:11:17 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v1.11.1 - 2023-09-26(09:19:53 +0000)

### Fixes

- Old IP stays when only updating the subnetmask

## Release v1.11.0 - 2023-09-22(11:21:20 +0000)

### New

- [amxrt][no-root-user][capability drop] Netdev plugin must be adapted to run as non-root and lmited capabilities

## Release v1.10.3 - 2023-09-19(07:33:47 +0000)

### Other

- Remove MAC from old bridgetable when adding to new bridgetable

## Release v1.10.2 - 2023-07-24(12:55:58 +0000)

### Fixes

- - Calling UpdateIPAddress with the same value for the old and new parameters will remove the address

## Release v1.10.1 - 2023-07-04(15:05:09 +0000)

### Fixes

- Init script has no shutdown function

## Release v1.10.0 - 2023-05-09(14:06:03 +0000)

### New

- [UpLinkMonitor] [NetDev] Implement a request function to get the default route parameters

## Release v1.9.2 - 2023-04-27(09:29:59 +0000)

### Fixes

- Add a config option to map the unknown state to up on all interfaces

## Release v1.9.1 - 2023-04-17(11:58:02 +0000)

### Fixes

- [odl]Remove deprecated odl keywords

## Release v1.9.0 - 2023-03-23(09:53:52 +0000)

### New

- Add Support for a custom DefaultRoute! event

## Release v1.8.14 - 2023-03-13(07:17:02 +0000)

### Fixes

- Adding an address back right after deletion can cause problems

## Release v1.8.13 - 2023-03-09(09:18:31 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v1.8.12 - 2023-01-24(10:04:28 +0000)

### Fixes

- Fix copyright in the debian packages

## Release v1.8.11 - 2023-01-23(16:25:04 +0000)

### Fixes

- No event sent for State changes on bridge interfaces

## Release v1.8.10 - 2023-01-23(10:06:35 +0000)

### Fixes

- Move netlink code from IP-manager to NetDev

## Release v1.8.9 - 2022-12-09(09:17:58 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v1.8.8 - 2022-11-24(14:39:47 +0000)

## Release v1.8.7 - 2022-11-15(18:12:30 +0000)

### Fixes

- dslite0 netdev doesn't come up

## Release v1.8.6 - 2022-10-28(13:36:46 +0000)

### Other

- Add unit-tests part 2

## Release v1.8.5 - 2022-10-18(13:13:28 +0000)

### Fixes

- Netdev instability when performing firstboots

## Release v1.8.4 - 2022-10-07(08:58:49 +0000)

### Other

- Add unit-tests

## Release v1.8.3 - 2022-09-19(15:06:56 +0000)

### Other

- [CI] Fix typo in dependencies

## Release v1.8.2 - 2022-09-19(14:45:46 +0000)

### Fixes

- Route/Neigh/Addr not removed from datamodel when removed in Linux

## Release v1.8.1 - 2022-09-15(08:52:43 +0000)

### Fixes

- bad typecast

## Release v1.8.0 - 2022-09-09(13:52:37 +0000)

### Other

- expose bridgetable

## Release v1.7.13 - 2022-09-09(13:20:58 +0000)

### Fixes

- Issue HOP-1855: Revert Route/Neigh/Addr not removed from datamodel when removed in Linux

## Release v1.7.12 - 2022-09-06(06:45:44 +0000)

### Fixes

- Issue HOP-1855: Route/Neigh/Addr not removed from datamodel when removed in Linux

## Release v1.7.11 - 2022-09-02(09:54:50 +0000)

### Fixes

- No connection to lan after firstboot

## Release v1.7.10 - 2022-08-26(06:20:37 +0000)

### Fixes

- PPP instances never come up

## Release v1.7.9 - 2022-08-22(13:58:44 +0000)

### Fixes

- Failing transactions during stats read

## Release v1.7.8 - 2022-06-16(06:59:27 +0000)

### Fixes

- Remove overwriting interface status

## Release v1.7.7 - 2022-06-10(14:35:03 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v1.7.6 - 2022-05-12(13:04:00 +0000)

### Fixes

- [NetDev] Revert SAHTracez to warning

## Release v1.7.5 - 2022-05-09(15:36:01 +0000)

### Other

- [NetDev] Status of the Bridge is down, if no (active)interfaces are part of the bridge

## Release v1.7.4 - 2022-04-29(06:43:42 +0000)

### Fixes

- [amx][netdev] Route entry is not correctly removed

## Release v1.7.3 - 2022-03-25(15:26:35 +0000)

### Fixes

- possible crash when removing Links

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.7.2 - 2022-03-22(14:21:02 +0000)

### Fixes

- Link entries are not deleted fast enough

## Release v1.7.1 - 2022-03-17(10:40:31 +0000)

### Changes

- Let NetDev report the lo interface status as up when it is unknown

## Release v1.7.0 - 2022-03-17(08:51:19 +0000)

### New

- Add an Alias parameter to Link

## Release v1.6.0 - 2022-03-15(09:07:01 +0000)

### New

- Add NetLink message counters

## Release v1.5.4 - 2022-03-14(15:51:11 +0000)

### Fixes

- Memory leak, bridge_stpstate_converter not destroyed

## Release v1.5.3 - 2022-02-28(14:23:25 +0000)

### Fixes

- Make NetLink history bigger

## Release v1.5.2 - 2022-02-25(17:49:03 +0000)

### Fixes

- Stats no longer work

## Release v1.5.1 - 2022-02-24(17:16:50 +0000)

### Fixes

- Segfault when interface no longer exists when event is handled

## Release v1.5.0 - 2022-02-24(14:54:11 +0000)

### New

- Add an Alias parameter Link

## Release v1.4.3 - 2022-02-10(14:44:10 +0000)

### Fixes

- Segmentation faults in Addr, Neigh, Route and Link handlers

## Release v1.4.2 - 2022-02-02(00:41:00 +0000)

## Release v1.4.1 - 2022-01-31(17:13:51 +0000)

### Other

- Document Data Model (odl files)

## Release v1.4.0 - 2022-01-28(11:02:01 +0000)

### New

- Add bridge mib with STP object to NetDev.Link when Kind == "bridge"

## Release v1.3.7 - 2022-01-18(10:46:51 +0000)

### Fixes

- Fix some potential null ptr dereference issues

## Release v1.3.6 - 2021-11-20(11:13:15 +0000)

## Release v1.3.5 - 2021-11-18(16:45:57 +0000)

### Fixes

- [NetDev] resolve netdev crash when adding / deleting devices at a high rate (> 50/s)

## Release v1.3.4 - 2021-11-15(09:22:18 +0000)

### Fixes

- Missing mod-sahtrace dependecy in some components

## Release v1.3.3 - 2021-09-07(13:21:17 +0000)

### Fixes

- PCF-288: [Netdev] crashes when doing a /etc/init.d/network restart

## Release v1.3.2 - 2021-08-25(12:34:34 +0000)

### Changes

- redefine startup order

## Release v1.3.1 - 2021-08-05(07:32:32 +0000)

### Fixes

- [Netdev] Individual NetDev.Link.{i}. parameters can not be retrieved with ubus-cli

## Release 1.3.0 - 2021-06-29

- Emit amx-event when a NetDev.Link object is deleted
- Set index field of the n-th NetDev.Link.{n}.Index = n

## Release 1.2.0 - 2021-06-04

- First delivery of NetDev.Link.IPv4/6Route management

## Release 1.1.0 - 2021-05-27

- NetDev.Link entries now handle addresses and neighbours (IPv4 and IPv6)

## Release 1.0.0 - 2021-05-27

- First delivery of NetDev with initial datamodel
