/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <arpa/inet.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "netdev_util.h"

#define ME "netdev"

static char zeroaddr[ADDRSPACE];

const char* family2str(unsigned char family) {
    return family == AF_INET ? "IPv4" : family == AF_INET6 ? "IPv6" : "Unspec";
}

int addrlen(unsigned char family) {
    return family == AF_INET ? 4 : family == AF_INET6 ? 16 : 0;
}

const char* addr2str(unsigned char family, unsigned char* addr, bool optional) {
    static char strbuf[64];
    if(optional && addr_isNull(family, addr)) {
        strbuf[0] = '\0';
    } else if(!inet_ntop(family, addr, strbuf, 64)) {
        strbuf[0] = '\0';
        SAH_TRACEZ_ERROR(ME, "Invalid binary IP-address: family=%d(%s) address=0x%02x%02x%02x... errno=%d(%s)"
                         , family, family2str(family), addr[0], addr[1], addr[2], errno, strerror(errno));
    }
    return strbuf;
}

char* addr2str_dyn(unsigned char family, unsigned char* addr) {
    char* strbuf = calloc(sizeof(char), 64);
    if(inet_ntop(family, addr, strbuf, 64) == NULL) {
        strbuf[0] = '\0';
        SAH_TRACEZ_ERROR(ME, "Invalid binary IP-address: family=%d(%s) address=0x%02x%02x%02x... errno=%d(%s)"
                         , family, family2str(family), addr[0], addr[1], addr[2], errno, strerror(errno));
    }
    return strbuf;
}

#define IN_CGNAT(a)          (((((in_addr_t) htonl(a)) & 0xffc00000) == 0x64400000) || ((((in_addr_t) htonl(a)) & 0xffffff00) == 0xc0000000))
#ifndef IN_LOOPBACK
#define IN_LOOPBACK(a)          ((((long int) htonl(a)) & 0xff000000) == 0x7f000000)
#endif

/*PRIVATE 192.168.0.0/16 */
#define PRIVATE_16(a)   ((((in_addr_t) htonl(a)) & 0xffff0000) == 0xc0a80000)

/*PRIVATE 172.16.0.0/12 */
#define PRIVATE_20(a)   ((((in_addr_t) htonl(a)) & 0xfff00000) == 0xac100000)

/*PRIVATE 10.0.0.0/8 */
#define PRIVATE_24(a)   ((((in_addr_t) htonl(a)) & 0xff000000) == 0x0a000000)

# define IN6_IS_ADDR_GLOBAL(a) \
    (__extension__                                                              \
     ({ __const struct in6_addr* __a = (__const struct in6_addr*) (a);         \
          (__a->s6_addr32[0] & htonl(0xe0000000)) == htonl(0x20000000); }))

# define IN6_IS_ADDR_UNIQUE_LOCAL(a) \
    (__extension__                                                              \
     ({ __const struct in6_addr* __a = (__const struct in6_addr*) (a);         \
          (__a->s6_addr32[0] & htonl(0xfc000000)) == htonl(0xfc000000); }))

const char* addr2flag(unsigned char family, unsigned char* addr, bool optional) {
    static char strbuf[64];
    strbuf[0] = '\0';
    if(!addr) {
        return strbuf;
    }
    if(optional && addr_isNull(family, addr)) {
        return strbuf;
    } else if(family == AF_INET) {
        in_addr_t a = 0;
        memcpy(&a, addr, 4);
        if(IN_LOOPBACK(a)) {
            sprintf(strbuf, "@loopback");
        } else if(PRIVATE_16(a) || PRIVATE_20(a) || PRIVATE_24(a)) {
            sprintf(strbuf, "@private");
        } else if(IN_CGNAT(a)) {
            /* a cgn nat address should be of the form 100.64.0.0/10 */
            sprintf(strbuf, "@cgn");
        } else if(IN_CLASSD(a)) {
            //should not occur
            sprintf(strbuf, "@mc");
        } else if(a == INADDR_ANY) {
            //should not occur
            SAH_TRACEZ_WARNING(ME, "IPv4 address: is any address");
            sprintf(strbuf, "@any");
        } else if(a == INADDR_BROADCAST) {
            //should not occur
            SAH_TRACEZ_WARNING(ME, "IPv4 address: is broadcast address");
            sprintf(strbuf, "@broadcast");
        } else if(a == INADDR_NONE) {
            //should not occur
            SAH_TRACEZ_WARNING(ME, "IPv4 address: no valid address");
            sprintf(strbuf, "@none");
        } else {
            //assume all other addresses are valid global ip addresses.
            sprintf(strbuf, "@gua");
        }
    } else if(family == AF_INET6) {
        if(IN6_IS_ADDR_LOOPBACK(addr)) {
            sprintf(strbuf, "@loopback");
        } else if(IN6_IS_ADDR_LINKLOCAL(addr)) {
            sprintf(strbuf, "@lla");
        } else if(IN6_IS_ADDR_MULTICAST(addr)) {
            sprintf(strbuf, "@mc");
        } else if(IN6_IS_ADDR_GLOBAL(addr)) {
            sprintf(strbuf, "@gua");
        } else if(IN6_IS_ADDR_UNIQUE_LOCAL(addr)) {
            sprintf(strbuf, "@ula");
        } else {
            sprintf(strbuf, "@unknown");
        }
    }
    return strbuf;
}

unsigned char* str2addr(unsigned char family, const char* str, bool* success) {
    static unsigned char addrbuf[ADDRSPACE];
    if(success) {
        *success = false;
    }
    memset(addrbuf, 0, ADDRSPACE);
    if(!str || !*str) {
        if(success) {
            *success = true;
        }
        return addrbuf; // consider empty string as zero address
    }
    int ret = inet_pton(family, str, addrbuf);
    if(ret == 1) {
        if(success) {
            *success = true;
        }
    } else if(ret < 0) {
        SAH_TRACEZ_ERROR(ME, "Invalid IP-address: family=%d(%s) address=%s errno=%d(%s)"
                         , family, family2str(family), str, errno, strerror(errno));
    } else if(ret == 0) {
        SAH_TRACEZ_ERROR(ME, "Invalid IP-address: family=%d(%s) address=%s"
                         , family, family2str(family), str);
    }
    return addrbuf;
}

bool addr_isNull(unsigned char family, unsigned char* addr) {
    bool ret = true;

    if((addr != NULL) && (*addr != 0)) {
        ret = !memcmp(zeroaddr, addr, addrlen(family));
    }
    return ret;
}

const char* lladdr2str(const lladdr_t* lladdr) {
    static char strbuf[LLADDRSPACE * 3];
    char* strptr = strbuf;
    const unsigned char* data = lladdr->data;
    unsigned int len = lladdr->len > LLADDRSPACE ? LLADDRSPACE : lladdr->len;

    *strptr = '\0';
    while(len--) {
        strptr += snprintf(strptr, sizeof(strbuf) - (strptr - strbuf), "%s%02X", strptr == strbuf ? "" : ":", *data++);
        if(strptr - strbuf >= (signed) sizeof(strbuf)) {
            break;
        }
    }
    return strbuf;
}

const lladdr_t* str2lladdr(const char* value, bool* success) {
    static lladdr_t lladdrbuf;
    unsigned char* data = lladdrbuf.data;
    lladdrbuf.len = 0;
    if(value) {
        while(*value) {
            *data++ = (unsigned char) strtoul(value, (char**) &value, 16);
            lladdrbuf.len++;
            if(*value) { // skip separator
                value++;
            }
            if(lladdrbuf.len >= LLADDRSPACE) {
                break;
            }
        }
    }
    if(success) {
        *success = true;
    }
    return &lladdrbuf;
}

void lladdr_assign(lladdr_t* lladdr, int len, const unsigned char* data) {
    lladdr->len = len > LLADDRSPACE ? LLADDRSPACE : len;
    if(lladdr->len) {
        memcpy(lladdr->data, data, lladdr->len);
    }
}

void lladdr_copy(lladdr_t* dst, const lladdr_t* src) {
    dst->len = src->len;
    memcpy(dst->data, src->data, src->len);
}

lladdr_t* bin2lladdr(int len, const unsigned char* data) {
    static lladdr_t lladdr;
    lladdr_assign(&lladdr, len, data);
    return &lladdr;
}

// TODO sysinfo is available now in uclibc, this code can be simplified
long uptime(void) {
    // Remark using sysinfo() would be easier (it used to be not available in uclibc)
    FILE* f = fopen("/proc/uptime", "r");
    if(!f) {
        SAH_TRACEZ_ERROR(ME, "/proc/uptime does not exist!");
        return 0;
    }
    char buf[64];
    char* ptr = fgets(buf, 64, f);
    long ret = 0;
    if(ptr) {
        ret = strtol(ptr, NULL, 0);
    } else {
        SAH_TRACEZ_ERROR(ME, "/proc/uptime can not be read");
    }
    fclose(f);
    return ret;
}


void util_init(void) {
    memset(zeroaddr, 0, ADDRSPACE);
}

bool deferred_trans_create(deferred_trans_t** deferred_trans, amxd_trans_t* transaction, void* priv) {
    bool rv = false;

    when_null_trace(deferred_trans, exit, ERROR, "Failed to create new deferred transaction");
    *deferred_trans = (deferred_trans_t*) calloc(1, sizeof(deferred_trans_t));
    when_null_trace(*deferred_trans, exit, ERROR, "Failed to allocate new deferred transaction");

    (*deferred_trans)->transaction = transaction;
    (*deferred_trans)->priv = priv;

    rv = true;
exit:
    return rv;
}

void deferred_trans_clean(deferred_trans_t** deferred_trans) {
    if(!deferred_trans || !(*deferred_trans)) {
        goto exit;
    }
    amxd_trans_delete(&(*deferred_trans)->transaction);
    free(*deferred_trans);
    *deferred_trans = NULL;

exit:
    return;
}
