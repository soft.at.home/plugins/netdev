/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "netdev_bridgetable.h"
#include "netdev_parseresult.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <libmnl/libmnl.h>

#include "netdev_netlink.h"
#include "netdev_util.h"
#include <malloc.h>

#include "dm_netdev.h"
#include "dm_convert.h"
#include "dm_link_neigh.h"
#include "dm_link_route.h"

#define ME "netdev"

#define BRIDGETABLE_LINK_PATH "NetDev.Link."
#define BRIDGETABLE_PATH_FORMAT BRIDGETABLE_LINK_PATH "%" PRIu32 ".BridgeTable"

static amxc_llist_t bridgetables = {NULL, NULL};

static int s_handle_neighbour_message(rtnlmsg_t* message);
static rtnlhandler_t s_new_neighbor_handler = { RTM_NEWNEIGH, sizeof(struct ndmsg), s_handle_neighbour_message, NULL };
static rtnlhandler_t s_delete_neighbor_handler = { RTM_DELNEIGH, sizeof(struct ndmsg), s_handle_neighbour_message, NULL };
static rtnlhandler_t s_get_neighbor_handler = { RTM_GETNEIGH, sizeof(struct ndmsg), s_handle_neighbour_message, NULL };

static bool s_generate_key(char* target, size_t target_length, netdev_parseresult_bridgetable_t* parseresult) {
    int snprintf_ret = -1;
    bool ok = false;
    when_null_trace(target, error, ERROR, "NULL");
    when_null_trace(parseresult, error, ERROR, "NULL");
    when_null_trace(parseresult->mac, error, ERROR, "No mac");

    snprintf_ret = snprintf(target, target_length, "mac-%s", parseresult->mac);
    ok = snprintf_ret >= 0 && snprintf_ret < (int) target_length;
    when_false(ok, error);
    for(char* c = target; *c != '\0'; c++) {
        if(*c == ':') {
            *c = '_';
        }
    }

    return true;
error:
    return false;
}

static void s_bridgetable_entry_delete(netdev_parseresult_bridgetable_t* parseresult) {
    bool ok = false;
    amxd_status_t ret = amxd_status_unknown_error;
    char key[22] = {0};
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);
    when_null_trace(parseresult, exit, ERROR, "NULL");

    ok = s_generate_key(key, sizeof(key), parseresult);
    when_false_trace(ok, exit, ERROR, "Error formatting key");

    amxd_trans_select_pathf(&transaction, BRIDGETABLE_PATH_FORMAT, parseresult->netdev_index);
    amxd_trans_del_inst(&transaction, 0, key);

    ret = amxd_trans_apply(&transaction, netdev_get_dm());
    when_failed_trace(ret, exit, ERROR, "Failed delete " BRIDGETABLE_PATH_FORMAT ".%s: %d", parseresult->netdev_index, key, ret);

exit:
    amxd_trans_clean(&transaction);
}

static void s_bridgetable_entry_add_without_move(amxd_trans_t* transaction, netdev_parseresult_bridgetable_t* parseresult) {
    bool ok = false;
    amxd_object_t* obj = NULL;
    char key[22] = {0};

    ok = s_generate_key(key, sizeof(key), parseresult);
    when_false_trace(ok, exit, ERROR, "Error formatting key");
    obj = amxd_dm_findf(netdev_get_dm(), BRIDGETABLE_PATH_FORMAT ".%s", parseresult->netdev_index, key);
    if(obj != NULL) {
        amxd_trans_select_object(transaction, obj);
    } else {
        amxd_trans_select_pathf(transaction, BRIDGETABLE_PATH_FORMAT, parseresult->netdev_index);
        amxd_trans_add_inst(transaction, 0, key);
    }
    amxd_trans_set_value(cstring_t, transaction, "MACAddress", parseresult->mac);

exit:
    return;
}

static void s_get_bridgetable_entries(amxc_llist_t* target_path_list, const char* mac, uint32_t exclude_netdev_index) {
    amxd_object_t* link_obj = amxd_dm_findf(netdev_get_dm(), "%s", BRIDGETABLE_LINK_PATH);
    amxd_object_resolve_pathf(link_obj, target_path_list,
                              "[Kind == 'bridge_port' && Index != %" PRIu32 "].BridgeTable.[MACAddress ^= '%s'].",
                              exclude_netdev_index, mac
                              );
}

/**
 * Remove all bridgetable entries with the MAC given in `parseresult` from ports that are different
 * ports than the port described in `parseresult`.
 */
static void s_bridgetable_entry_remove_from_old_port(amxd_trans_t* transaction, netdev_parseresult_bridgetable_t* parseresult) {
    amxc_llist_t old_ports;
    amxc_llist_init(&old_ports);
    when_null_trace(parseresult, exit, ERROR, "NULL argument");
    s_get_bridgetable_entries(&old_ports, parseresult->mac, parseresult->netdev_index);
    amxc_llist_for_each(it, (&old_ports)) {
        amxc_string_t* path = amxc_string_from_llist_it(it);
        amxd_object_t* object = amxd_dm_findf(netdev_get_dm(), "%s", amxc_string_get(path, 0));
        uint32_t index = amxd_object_get_index(object);
        if(object == NULL) {
            SAH_TRACEZ_ERROR(ME, "Search yielded unexisting object %s", amxc_string_get(path, 0));
            continue;
        }
        amxd_trans_select_object(transaction, object);
        amxd_trans_select_pathf(transaction, ".^");
        amxd_trans_del_inst(transaction, index, NULL);
    }

exit:
    amxc_llist_clean(&old_ports, amxc_string_list_it_free);
}


/**
 * Add bridgetable entry in datamodel based on given parseresult.
 *
 * If the MAC moves from one port to another, i.e. the MAC given in `parseresult` already exists
 * in a bridgetable entry of a port that is different from the port described in the given
 * `parseresult`, then the bridgetable entry with the MAC is removed from the old port and added to
 * the new port.
 */
static void s_bridgetable_entry_add_and_move(netdev_parseresult_bridgetable_t* parseresult) {
    amxd_status_t ret = amxd_status_unknown_error;
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);
    when_null_trace(parseresult, exit, ERROR, "NULL");
    when_false_trace(!parseresult->disappeared, exit, ERROR, "Cannot create/modify entry that must be deleted");

    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_priv, true);

    s_bridgetable_entry_remove_from_old_port(&transaction, parseresult);
    s_bridgetable_entry_add_without_move(&transaction, parseresult);

    ret = amxd_trans_apply(&transaction, netdev_get_dm());
    when_failed_trace(ret, exit, ERROR, "Failed to apply transaction for %" PRIu32 " %s %d", parseresult->netdev_index, parseresult->mac, ret);

exit:
    amxd_trans_clean(&transaction);
}

void netdev_bridgetable_handle_parseresult(netdev_parseresult_bridgetable_t* parseresult) {
    when_null_trace(parseresult, exit, ERROR, "NULL");
    if(parseresult->disappeared) {
        s_bridgetable_entry_delete(parseresult);
    } else {
        s_bridgetable_entry_add_and_move(parseresult);
    }

exit:
    return;
}

/**
 *
 * Implements @ref amxp_deferred_fn_t
 */
static void s_deferred_handle_neighbour_message(const amxc_var_t* data UNUSED, void* priv) {
    netdev_parseresult_bridgetable_t* parseresult = priv;
    netdev_bridgetable_handle_parseresult(parseresult);
    netdev_parseresult_bridgetable_delete(&parseresult);
}

static int s_handle_neighbour_message(rtnlmsg_t* message) {
    int rv = -1;
    const struct nlmsghdr* header = NULL;
    netdev_parseresult_bridgetable_t* parseresult = NULL;
    bridgetable_t* bridgetable = NULL;
    route_t* the_defroute = NULL;
    when_null_trace(message, exit, ERROR, "NULL");

    header = rtnlmsg_nlhdr(message);
    when_null_trace(message, exit, ERROR, "Cannot extract headers from message");
    parseresult = netdev_parseresult_bridgetable_parse(header);
    when_null(parseresult, exit); // also happens if message not for us, so silent.

    bridgetable = bridgetable_find(parseresult);
    if(parseresult->disappeared) {
        // remove bridge table
        if(bridgetable != NULL) {
            bridgetable_destroy(&bridgetable);
        }
    } else {
        // add bridge table
        if(bridgetable == NULL) {
            bridgetable = bridgetable_create();
            bridgetable->mac = strdup(parseresult->mac);
            bridgetable->netdev_index = parseresult->netdev_index;
        }
    }

    when_null(bridgetable, skip_defroute);
    when_str_empty(bridgetable->mac, skip_defroute);

    the_defroute = bridgetable_bind_to_defroute(bridgetable, true);
    if(the_defroute != NULL) {
        if(default_route_event(the_defroute, true) != 0) {
            SAH_TRACEZ_INFO(ME, "Could not send default route event from bridge table event on IPv4.");
        }
    } else {
        SAH_TRACEZ_INFO(ME, "Could not send default route event from bridge table event on IPv4. Defroute empty.");
    }

    the_defroute = bridgetable_bind_to_defroute(bridgetable, false);
    if(the_defroute != NULL) {
        if(default_route_event(the_defroute, true) != 0) {
            SAH_TRACEZ_INFO(ME, "Could not send default route event from bridge table event on IPv6.");
        }
    } else {
        SAH_TRACEZ_INFO(ME, "Could not send default route event from bridge table event on IPv6. Defroute empty.");

    }

skip_defroute:
    rv = amxp_sigmngr_deferred_call(NULL, s_deferred_handle_neighbour_message, NULL, parseresult);
exit:
    return rv;
}

void netdev_bridgetable_init(void) {
    rtnl_register_handler(&s_new_neighbor_handler);
    rtnl_register_handler(&s_delete_neighbor_handler);
    rtnl_register_handler(&s_get_neighbor_handler);
}

bool netdev_bridgetable_start(void) {
    bool ret = false;
    rtnlmsg_t* message = rtnlmsg_instance();
    rtnlmsg_initialize(message, RTM_GETNEIGH, sizeof(struct ndmsg), NLM_F_REQUEST | NLM_F_ROOT | NLM_F_REQUEST | NLM_F_DUMP);
    struct ndmsg* nd = (struct ndmsg*) rtnlmsg_hdr(message);
    nd->ndm_family = AF_BRIDGE;
    nd->ndm_state = NUD_REACHABLE;

    ret = rtnl_send(netdev_get_nlfd(), message);
    when_false_trace(ret, error, ERROR, "Failed sending netlink message");

    ret = rtnl_read(netdev_get_nlfd(), message, true);
    when_false_trace(ret, error, ERROR, "Failed reading netlink message");

    return true;

error:
    return false;
}

void bridgetable_destroy(bridgetable_t** bridgetable) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(bridgetable, exit, ERROR, "No route provided, can not remove route");
    free((*bridgetable)->mac);
    amxc_llist_it_take(&(*bridgetable)->it);
    free(*bridgetable);
    *bridgetable = NULL;
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

bridgetable_t* bridgetable_create(void) {
    SAH_TRACEZ_IN(ME);
    bridgetable_t* bridgetable = (bridgetable_t*) calloc(1, sizeof(bridgetable_t));
    amxc_llist_it_init(&bridgetable->it);
    amxc_llist_append(&bridgetables, &bridgetable->it);
    bridgetable->mac = NULL;
    SAH_TRACEZ_OUT(ME);
    return bridgetable;
}

bridgetable_t* bridgetable_find(netdev_parseresult_bridgetable_t* ref) {
    SAH_TRACEZ_IN(ME);
    bridgetable_t* bridgetable = NULL;
    when_null_trace(ref, exit, ERROR, "No reference provided to find bridgetable");
    amxc_llist_iterate(it, &bridgetables) {
        bridgetable = amxc_container_of(it, bridgetable_t, it);
        if((strcmp(ref->mac, bridgetable->mac) == 0)
           && (ref->netdev_index != bridgetable->netdev_index)) {
            bridgetable->old = true;
        }
        if((strcmp(ref->mac, bridgetable->mac) == 0)
           && (ref->netdev_index == bridgetable->netdev_index)) {
            bridgetable->old = false;
            break;
        }
        bridgetable = NULL;
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return bridgetable;
}

int bridgetable_mac_to_netdev_index(const char* mac) {
    SAH_TRACEZ_IN(ME);
    bridgetable_t* bridgetable = NULL;
    int index = -1;
    when_str_empty_trace(mac, exit, ERROR, "No mac provided to find bridgetable index");
    amxc_llist_iterate(it, &bridgetables) {
        bridgetable = amxc_container_of(it, bridgetable_t, it);
        if((strcmp(bridgetable->mac, mac) == 0) && (bridgetable->old == false)) {
            index = bridgetable->netdev_index;
            break;
        }
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return index;
}

void bridgetable_cleanup(void) {
    SAH_TRACEZ_IN(ME);
    while(!amxc_llist_is_empty(&bridgetables)) {
        amxc_llist_it_t* it = amxc_llist_get_first(&bridgetables);
        bridgetable_t* bridgetable = amxc_container_of(it, bridgetable_t, it);
        bridgetable_destroy(&bridgetable);
    }
    SAH_TRACEZ_OUT(ME);
}