/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <netinet/in.h>
#include <linux/rtnetlink.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "netdev_netlink.h"
#include "dm_link_addr.h"
#include "dm_link_route.h"
#include "netdev_rpc_utils.h"
#include "netdev_rpc.h"
#include "netdev.h"

#define IPV4 4
#define IPV6 6
#define ME "rpc"

static bool ip_already_exist(amxd_object_t* link_obj, bool ipv4, const char* ip_addr, int prefixlen) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    amxd_object_t* ip_addr_obj = NULL;
    int family = ipv4 ? IPV4 : IPV6;
    addr_t* addr = NULL;

    SAH_TRACEZ_INFO(ME, "Looking for IPv%dAddr.[Address == '%s' && PrefixLen == %d].", family, ip_addr, prefixlen);
    ip_addr_obj = amxd_object_findf(link_obj, "IPv%dAddr.[Address == '%s' && PrefixLen == %d].", family, ip_addr, prefixlen);

    when_null(ip_addr_obj, exit);
    when_null(ip_addr_obj->priv, exit);
    addr = (addr_t*) ip_addr_obj->priv;

    // When the address is no longer active (it will be removed), we should consider the address as non existent
    when_false(addr->active, exit);

    // If the address is still active but marked for deletion from the DM, prevent it by stopping the mercy timer.
    // Since the address will be already removed form Linux, we should return false so the address will be set again.
    if((addr->mercytimer != NULL) && (addr->mercytimer->state != amxp_timer_off)) {
        amxp_timer_stop(addr->mercytimer);
        goto exit;
    }

    rv = true;
    SAH_TRACEZ_WARNING(ME, "'%s/%d' already set on %s", ip_addr, prefixlen, link_obj->name);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}



amxd_status_t _AddIPAddress(amxd_object_t* link_obj, UNUSED amxd_function_t* func, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_invalid_value;
    bool ipv4 = GET_BOOL(args, "IPv4");
    const cstring_t ip_addr = GET_CHAR(args, "IPAddress");
    int prefixlen = GET_UINT32(args, "PrefixLen");
    unsigned char family = ipv4 ? AF_INET : AF_INET6;

    when_str_empty_trace(ip_addr, exit, ERROR, "No IP address provided to set");
    when_true_trace(prefixlen <= 0, exit, ERROR, "Invalid prefix length");

    if(!ip_already_exist(link_obj, ipv4, ip_addr, prefixlen)) {
        SAH_TRACEZ_INFO(ME, "Adding %s/%d to %s", ip_addr, prefixlen, link_obj->name);
        when_false_trace(nl_ip_address(RTM_NEWADDR, family, ip_addr, link_obj->name, prefixlen),
                         exit, ERROR, "Failed to set '%s/%d' on %s", ip_addr, prefixlen, link_obj->name);
    }

    status = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _UpdateIPAddress(amxd_object_t* link_obj, UNUSED amxd_function_t* func, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_invalid_value;
    bool ipv4 = GET_BOOL(args, "IPv4");
    const cstring_t ip_addr = GET_CHAR(args, "IPAddress");
    const cstring_t old_ip_addr = GET_CHAR(args, "OldIPAddress");
    int prefixlen = GET_UINT32(args, "PrefixLen");
    int old_prefixlen = GET_UINT32(args, "OldPrefixLen");
    unsigned char family = ipv4 ? AF_INET : AF_INET6;

    when_str_empty_trace(ip_addr, exit, ERROR, "No IP address provided to set");
    when_str_empty_trace(old_ip_addr, exit, ERROR, "No IP address provided to replace");
    when_true_trace(prefixlen <= 0, exit, ERROR, "Invalid prefix length");
    when_true_trace(old_prefixlen <= 0, exit, ERROR, "Invalid old prefix length");

    SAH_TRACEZ_INFO(ME, "Changing %s/%d to %s/%d on %s", old_ip_addr, old_prefixlen, ip_addr, prefixlen, link_obj->name);

    if((prefixlen != old_prefixlen) || (0 != strncmp(ip_addr, old_ip_addr, strlen(ip_addr)))) {
        when_false_trace(nl_ip_address(RTM_DELADDR, family, old_ip_addr, link_obj->name, old_prefixlen),
                         exit, ERROR, "Failed to delete '%s/%d'", old_ip_addr, old_prefixlen);
    }

    if(!ip_already_exist(link_obj, ipv4, ip_addr, prefixlen)) {
        SAH_TRACEZ_INFO(ME, "Adding %s/%d to %s", ip_addr, prefixlen, link_obj->name);
        when_false_trace(nl_ip_address(RTM_NEWADDR, family, ip_addr, link_obj->name, prefixlen),
                         exit, ERROR, "Failed to set '%s/%d' on %s", ip_addr, prefixlen, link_obj->name);
    }

    status = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _DeleteIPAddress(amxd_object_t* link_obj, UNUSED amxd_function_t* func, amxc_var_t* args, UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_invalid_value;
    bool ipv4 = GET_BOOL(args, "IPv4");
    const cstring_t ip_addr = GET_CHAR(args, "IPAddress");
    int prefixlen = GET_UINT32(args, "PrefixLen");
    unsigned char family = ipv4 ? AF_INET : AF_INET6;

    when_str_empty_trace(ip_addr, exit, ERROR, "No IP address provided to delete");
    when_true_trace(prefixlen <= 0, exit, ERROR, "Invalid prefix length");

    SAH_TRACEZ_INFO(ME, "Deleting %s/%d from %s", ip_addr, prefixlen, link_obj->name);

    when_false_trace(nl_ip_address(RTM_DELADDR, family, ip_addr, link_obj->name, prefixlen),
                     exit, ERROR, "Failed to delete '%s/%d' from %s", ip_addr, prefixlen, link_obj->name);

    status = amxd_status_ok;

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _GetDefaultRoute(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func, UNUSED amxc_var_t* args, amxc_var_t* ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = route_convert_to_default_route_var(route_lookup_ipv4_default_route(), true, ret);
    SAH_TRACEZ_OUT(ME);
    return status;
}
