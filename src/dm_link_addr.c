/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>

#include "netdev_netlink.h"
#include "netdev_util.h"

#include "dm_netdev.h"
#include "dm_convert.h"
#include "dm_link_addr.h"
#include "dm_link_route.h"

#define ME "netdev"

static amxc_llist_t addrs = {NULL, NULL};

static converter_t* flags_converter = NULL;
static converter_t* scope_converter = NULL;

static unsigned char* rtnlmsg_address(rtnlmsg_t* msg) {
    struct rtattr* rta = rtnlmsg_attr(msg, IFA_LOCAL);
    if(!rta) {
        rta = rtnlmsg_attr(msg, IFA_ADDRESS);
    }
    return rta ? (unsigned char*) RTA_DATA(rta) : NULL;
}

static unsigned char* rtnlmsg_peer(rtnlmsg_t* msg) {
    struct rtattr* rta_address = NULL, * rta_local = rtnlmsg_attr(msg, IFA_LOCAL);
    if(rta_local) {
        rta_address = rtnlmsg_attr(msg, IFA_ADDRESS);
    }
    if(rta_local && rta_address &&
       !memcmp(RTA_DATA(rta_local), RTA_DATA(rta_address), addrlen(((struct ifaddrmsg*) rtnlmsg_hdr(msg))->ifa_family))) {
        rta_address = NULL;
    }
    return rta_address ? (unsigned char*) RTA_DATA(rta_address) : NULL;
}

static void addr_deferred_destroy(const amxc_var_t* data, void* priv) {
    SAH_TRACEZ_IN(ME);
    addr_t* addr = (addr_t*) priv;

    when_null(addr, exit);

    if(GETP_BOOL(data, "deleteObject")) {
        amxd_trans_t transaction;
        amxd_trans_init(&transaction);
        amxd_trans_select_pathf(&transaction, "NetDev.Link.%d.%sAddr.", addr->link, family2str(addr->family));
        amxd_trans_del_inst(&transaction, addr->index, NULL);
        if(amxd_status_ok != amxd_trans_apply(&transaction, netdev_get_dm())) {
            SAH_TRACEZ_WARNING(ME, "Transaction failed");
        }
        amxd_trans_clean(&transaction);
    }
    amxp_timer_delete(&addr->mercytimer);
    amxc_llist_it_take(&addr->it);
    free(addr);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void addr_destroy(addr_t* addr, bool deleteObject) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t var_deferred_data;

    amxc_var_init(&var_deferred_data);
    amxc_var_set_type(&var_deferred_data, AMXC_VAR_ID_HTABLE);

    when_null_trace(addr, exit, ERROR, "No address structure provided");
    when_false_trace(addr->active, exit, INFO, "Address already set to be destroyed");

    amxc_var_add_key(bool, &var_deferred_data, "deleteObject", deleteObject);
    // Set to inactive to prevent use between this point and deferred_destroy call
    addr->active = false;
    if(addr->object) {
        addr->object->priv = NULL;
        addr->object = NULL;
    }
    amxp_sigmngr_deferred_call(NULL, addr_deferred_destroy, &var_deferred_data, addr);

exit:
    amxc_var_clean(&var_deferred_data);
    SAH_TRACEZ_OUT(ME);
    return;
}

// callback function; answers to amxp_timer_cb_t signature
static void addr_expireMercy(UNUSED amxp_timer_t* timer, void* userdata) {
    addr_destroy((addr_t*) userdata, true);
}

addr_t* addr_create(void) {
    addr_t* addr = (addr_t*) calloc(1, sizeof(addr_t));
    amxp_timer_new(&addr->mercytimer, addr_expireMercy, addr);
    amxc_llist_it_init(&addr->it);
    amxc_llist_append(&addrs, &addr->it);
    addr->active = true;
    return addr;
}

int addr_copy(addr_t* dst, addr_t* src) {
    int rv = -1;

    when_null_trace(dst, exit, ERROR, "No destination address struct provided");
    when_null_trace(src, exit, ERROR, "No source address struct provided");

    dst->link = src->link;
    dst->family = src->family;
    dst->prefixlen = src->prefixlen;
    dst->flags = src->flags;
    dst->scope = src->scope;
    memcpy(dst->address, src->address, addrlen(dst->family));
    memcpy(dst->peer, src->peer, addrlen(dst->family));
    dst->preferred_lifetime = src->preferred_lifetime;
    dst->valid_lifetime = src->valid_lifetime;
    dst->created_timestamp = src->created_timestamp;
    dst->updated_timestamp = src->updated_timestamp;

    rv = 0;
exit:
    return rv;
}

static bool addr_match(addr_t* a1, addr_t* a2) {
    bool rv = false;
    when_null_trace(a1, exit, ERROR, "First address is invalid, can not try to match");
    when_null_trace(a2, exit, ERROR, "Second address is invalid, can not try to match");

    rv = (a1->link == a2->link) &&
        (a1->family == a2->family) &&
        (memcmp(a1->address, a2->address, addrlen(a2->family)) == 0);

exit:
    return rv;
}

addr_t* addr_find(addr_t* ref) {
    addr_t* addr = NULL;
    when_null_trace(ref, exit, ERROR, "No reference provided to find address");
    amxc_llist_iterate(it, &addrs) {
        addr = amxc_container_of(it, addr_t, it);
        if(addr_match(addr, ref) && (addr->active == true)) {
            break;
        }
        addr = NULL;
    }
exit:
    return addr;
}

/**
 * Used to convert a rtnetlink message to an address struct
 * @param addr empty address struct to be filled with the data in the message.
 *  Do NOT provide an addr that contains any data or that is put in the addrs list
 * (so no address that was created with the addr_create function)
 * @param msg rtnl message
 * @return 0 if ok, -1 in case of error
 */
int msg2addr(addr_t* addr, rtnlmsg_t* msg) {
    int rv = -1;
    struct ifaddrmsg* ifaddr = NULL;
    unsigned char* address = NULL;
    unsigned char* peer = NULL;
    struct rtattr* rta = NULL;
    when_null_trace(addr, exit, ERROR, "No address structure provided");
    memset(addr, 0, sizeof(addr_t));
    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    ifaddr = (struct ifaddrmsg*) rtnlmsg_hdr(msg);
    addr->family = ifaddr->ifa_family;
    addr->prefixlen = ifaddr->ifa_prefixlen;
    addr->flags = ifaddr->ifa_flags;
    addr->scope = ifaddr->ifa_scope;
    addr->link = ifaddr->ifa_index;
    address = rtnlmsg_address(msg);
    if(address) {
        memcpy(addr->address, address, addrlen(addr->family));
    }
    peer = rtnlmsg_peer(msg);
    if(peer) {
        memcpy(addr->peer, peer, addrlen(addr->family));
    }
    rta = rtnlmsg_attr(msg, IFA_CACHEINFO);
    if(rta) {
        unsigned long now = (unsigned long) uptime();
        struct ifa_cacheinfo* cacheinfo = (struct ifa_cacheinfo*) RTA_DATA(rta);
        addr->preferred_lifetime = cacheinfo->ifa_prefered == 0xffffffff ? 0 : now + cacheinfo->ifa_prefered;
        addr->valid_lifetime = cacheinfo->ifa_valid == 0xffffffff ? 0 : now + cacheinfo->ifa_valid;
        addr->created_timestamp = cacheinfo->cstamp / 100;
        addr->updated_timestamp = cacheinfo->tstamp / 100;
    }

    rv = 0;
exit:
    return rv;
}

static void new_addr_deferred_transaction(const amxc_var_t* data, void* priv) {
    amxd_status_t ret = amxd_status_invalid_value;
    deferred_trans_t* deferred_trans = (deferred_trans_t*) priv;
    amxd_trans_t* transaction = NULL;
    addr_t* addr = NULL;

    when_null_trace(deferred_trans, exit, ERROR, "No deferred transaction provided");
    transaction = deferred_trans->transaction;
    addr = (addr_t*) deferred_trans->priv;
    when_null_trace(transaction, exit, ERROR, "No transaction provided");
    when_null_trace(addr, exit, ERROR, "No address structure provided");
    when_false_trace(addr->active, exit, INFO, "Address set to be destroyed, not updating it anymore");

    ret = amxd_trans_apply(transaction, netdev_get_dm());
    if(ret != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply transaction, return %d", ret);
    }

    if(GETP_BOOL(data, "new_instance")) {
        addr->object = amxd_dm_findf(netdev_get_dm(), "NetDev.Link.%d.%sAddr.%s",
                                     addr->link, family2str(addr->family), GETP_CHAR(data, "pathbuf"));
        if(addr->object == NULL) {
            addr_destroy(addr, false);
            goto exit;
        }
        addr->object->priv = addr;
        addr->index = addr->object->index;
    }
    SAH_TRACEZ_INFO(ME, "Deferred address transaction successful");

exit:
    deferred_trans_clean(&deferred_trans);
}

amxd_trans_t* create_transaction_from_addr(addr_t* addr, bool new_inst) {
    SAH_TRACEZ_IN(ME);
    char pathbuf[40];
    amxd_trans_t* trans = NULL;

    when_null_trace(addr, exit, ERROR, "No address provide to create transaction");

    amxd_trans_new(&trans);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    amxd_trans_set_attr(trans, amxd_tattr_change_priv, true);

    if(new_inst) {
        sprintf(pathbuf, "dyn%u", addr->dyn_index);
        amxd_trans_select_pathf(trans, "NetDev.Link.%d.%sAddr", addr->link, family2str(addr->family));
        amxd_trans_add_inst(trans, 0, pathbuf);
    } else {
        amxd_trans_select_pathf(trans, "NetDev.Link.%d.%sAddr.dyn%u.", addr->link, family2str(addr->family), addr->dyn_index);
    }

    amxd_trans_set_value(cstring_t, trans, "Address", addr2str(addr->family, addr->address, false));
    amxd_trans_set_value(cstring_t, trans, "Peer", addr2str(addr->family, addr->peer, true));
    amxd_trans_set_value(uint32_t, trans, "PrefixLen", addr->prefixlen);
    amxd_trans_set_value(cstring_t, trans, "Flags", convert_bin2str(flags_converter, addr->flags));
    amxd_trans_set_value(cstring_t, trans, "Scope", convert_bin2str(scope_converter, addr->scope));
    amxd_trans_set_value(cstring_t, trans, "TypeFlags", addr2flag(addr->family, addr->address, false));
    amxd_trans_set_value(uint32_t, trans, "PreferredLifetime", addr->preferred_lifetime);
    amxd_trans_set_value(uint32_t, trans, "ValidLifetime", addr->valid_lifetime);
    amxd_trans_set_value(uint32_t, trans, "CreatedTimestamp", addr->created_timestamp);
    amxd_trans_set_value(uint32_t, trans, "UpdatedTimestamp", addr->updated_timestamp);

exit:
    SAH_TRACEZ_OUT(ME);
    return trans;
}

int handleNewAddr(rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    addr_t ws;
    addr_t* addr = NULL;
    char pathbuf[40];
    deferred_trans_t* deferred_trans = NULL;
    amxd_trans_t* transaction = NULL;
    amxc_var_t var_deferred_data;

    amxc_var_init(&var_deferred_data);
    amxc_var_set_type(&var_deferred_data, AMXC_VAR_ID_HTABLE);

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");
    when_failed_trace(msg2addr(&ws, msg), exit, ERROR, "Failed to convert msg to address");
    addr = addr_find(&ws);
    if(addr != NULL) {
        SAH_TRACEZ_INFO(ME, "Existing 'addr' found");
        amxp_timer_stop(addr->mercytimer);
    } else {
        SAH_TRACEZ_INFO(ME, "Creating new 'addr'; NetDev.Link.%d.%sAddr", ws.link, family2str(ws.family));
        addr = addr_create();
        addr->dyn_index = ({static unsigned counter = 0; counter++;});
        sprintf(pathbuf, "dyn%u", addr->dyn_index);

        amxc_var_add_key(bool, &var_deferred_data, "new_instance", true);
        amxc_var_add_key(cstring_t, &var_deferred_data, "pathbuf", pathbuf);
        SAH_TRACEZ_INFO(ME, "pathbuf = %s", pathbuf);
    }
    addr_copy(addr, &ws);

    transaction = create_transaction_from_addr(addr, GET_BOOL(&var_deferred_data, "new_instance"));
    when_null_trace(transaction, exit, ERROR, "Failed to create transaction");

    deferred_trans_create(&deferred_trans, transaction, addr);
    rv = amxp_sigmngr_deferred_call(NULL, new_addr_deferred_transaction, &var_deferred_data, deferred_trans);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to defer the new address transaction");
        deferred_trans_clean(&deferred_trans);
    }

exit:
    amxc_var_clean(&var_deferred_data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int handleDelAddr(rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    addr_t ws;
    addr_t* addr = NULL;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    when_failed_trace(msg2addr(&ws, msg), exit, ERROR, "Failed to convert msg to address");
    addr = addr_find(&ws);

    when_null_trace(addr, exit, ERROR, "No matching address found to delete");
    rv = amxp_timer_start(addr->mercytimer, 100);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void printAddrMsg(FILE* f, rtnlmsg_t* msg) {
    fprintf(f, "%s{",
            rtnlmsg_type(msg) == RTM_NEWADDR ? "RTM_NEWADDR" :
            rtnlmsg_type(msg) == RTM_DELADDR ? "RTM_DELADDR" :
            rtnlmsg_type(msg) == RTM_GETADDR ? "RTM_GETADDR" : "<UNKNOWN>");
    struct ifaddrmsg* ifaddr = (struct ifaddrmsg*) rtnlmsg_hdr(msg);
    fprintf(f, " family=%u(%s)", ifaddr->ifa_family, family2str(ifaddr->ifa_family));
    fprintf(f, " prefixlen=%u", ifaddr->ifa_prefixlen);
    fprintf(f, " flags=0x%x(%s)", ifaddr->ifa_flags, convert_bin2str(flags_converter, ifaddr->ifa_flags));
    fprintf(f, " scope=%u(%s)", ifaddr->ifa_scope, convert_bin2str(scope_converter, ifaddr->ifa_scope));
    fprintf(f, " index=%u", ifaddr->ifa_index);
    struct rtattr* rta;
    unsigned int rtalen = rtnlmsg_attrLen(msg);
    for(rta = rtnlmsg_attrFirst(msg); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
        switch(rta->rta_type) {
        case IFA_ADDRESS:   fprintf(f, " address=%s", addr2str(ifaddr->ifa_family, (unsigned char*) RTA_DATA(rta), false)); break;
        case IFA_LOCAL:     fprintf(f, " local=%s", addr2str(ifaddr->ifa_family, (unsigned char*) RTA_DATA(rta), false)); break;
        case IFA_BROADCAST: fprintf(f, " broadcast=%s", addr2str(ifaddr->ifa_family, (unsigned char*) RTA_DATA(rta), false)); break;
        case IFA_ANYCAST:   fprintf(f, " anycast=%s", addr2str(ifaddr->ifa_family, (unsigned char*) RTA_DATA(rta), false)); break;
        case IFA_LABEL:     fprintf(f, " label=%s", (const char*) RTA_DATA(rta)); break;
        case IFA_CACHEINFO: {
            struct ifa_cacheinfo* ci = (struct ifa_cacheinfo*) RTA_DATA(rta);
            fprintf(f, " cacheinfo{ preferred=%lu valid=%lu cstamp=%lu tstamp=%lu }",
                    (unsigned long) ci->ifa_prefered, (unsigned long) ci->ifa_valid,
                    (unsigned long) ci->cstamp, (unsigned long) ci->tstamp);
            break;
        }
        default:
            fprintf(f, " attr[%u](%lu byte%s)", rta->rta_type, (long unsigned int) RTA_PAYLOAD(rta), RTA_PAYLOAD(rta) == 1 ? "" : "s");
            break;
        }
    }
    fprintf(f, " }");
}

amxd_status_t _addr_destroy(amxd_object_t* object,
                            UNUSED amxd_param_t* param,
                            amxd_action_t reason,
                            UNUSED const amxc_var_t* const args,
                            UNUSED amxc_var_t* const retval,
                            UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_ok;
    addr_t* addr = NULL;

    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    if((object == NULL) || (amxd_object_get_type(object) == amxd_object_template)) {
        goto exit;
    }

    addr = (addr_t*) object->priv;
    when_null(addr, exit);
    addr_destroy(addr, false);

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static rtnlhandler_t newAddrHandler = { RTM_NEWADDR, sizeof(struct ifaddrmsg), handleNewAddr, printAddrMsg };
static rtnlhandler_t delAddrHandler = { RTM_DELADDR, sizeof(struct ifaddrmsg), handleDelAddr, printAddrMsg };
static rtnlhandler_t getAddrHandler = { RTM_GETADDR, sizeof(struct ifaddrmsg), NULL, printAddrMsg };

converter_t* init_addr_flags_converter(void) {
    converter_t* converter = converter_create("addr_flags", converter_attribute_flags);
    converter_loadEntry(converter, IFA_F_SECONDARY, "secondary");
    converter_loadEntry(converter, IFA_F_NODAD, "nodad");
    converter_loadEntry(converter, IFA_F_OPTIMISTIC, "optimistic");
    converter_loadEntry(converter, IFA_F_DADFAILED, "dadfailed");
    converter_loadEntry(converter, IFA_F_HOMEADDRESS, "homeaddress");
    converter_loadEntry(converter, IFA_F_DEPRECATED, "deprecated");
    converter_loadEntry(converter, IFA_F_TENTATIVE, "tentative");
    converter_loadEntry(converter, IFA_F_PERMANENT, "permanent");
    return converter;
}

converter_t* init_addr_scope_converter(void) {
    converter_t* converter = converter_create("addr_scope", converter_attribute_fallback);
    converter_loadEntry(converter, RT_SCOPE_UNIVERSE, "global");
    converter_loadEntry(converter, RT_SCOPE_SITE, "site");
    converter_loadEntry(converter, RT_SCOPE_HOST, "host");
    converter_loadEntry(converter, RT_SCOPE_LINK, "link");
    converter_loadEntry(converter, RT_SCOPE_NOWHERE, "nowhere");
    converter_loadTable(converter, amxd_dm_findf(netdev_get_dm(), "NetDev.ConversionTable.Scope"));
    return converter;
}

void addr_init_converters(void) {
    flags_converter = init_addr_flags_converter();
    scope_converter = init_addr_scope_converter();
}

bool addr_init(void) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_ok;
    amxd_param_t* parameter = NULL;

    addr_init_converters();

    const char* path[] = {"NetDev.Link.IPv4Addr", "NetDev.Link.IPv6Addr"};
    int i;
    for(i = 0; i < 2; i++) {
        amxd_object_t* netdev_addr = amxd_dm_findf(netdev_get_dm(), path[i], NULL);
        parameter = amxd_object_get_param_def(netdev_addr, "Flags");
        rv |= amxd_param_add_action_cb(parameter, action_param_validate, param_validate_converter, flags_converter);
        parameter = amxd_object_get_param_def(netdev_addr, "Scope");
        rv |= amxd_param_add_action_cb(parameter, action_param_validate, param_validate_converter, scope_converter);
    }

    rtnl_register_handler(&newAddrHandler);
    rtnl_register_handler(&delAddrHandler);
    rtnl_register_handler(&getAddrHandler);

    SAH_TRACEZ_OUT(ME);
    return (rv == amxd_status_ok);
}

bool addr_start(void) {
    SAH_TRACEZ_IN(ME);

    rtnlmsg_t* msg = rtnlmsg_instance();
    rtnlmsg_initialize(msg, RTM_GETADDR, sizeof(struct ifaddrmsg), NLM_F_REQUEST | NLM_F_DUMP);
    if(!rtnl_send(netdev_get_nlfd(), msg)) {
        return false;
    }
    if(!rtnl_read(netdev_get_nlfd(), msg, true)) {
        return false;
    }

    SAH_TRACEZ_OUT(ME);
    return true;
}

void addr_cleanup(void) {
    while(!amxc_llist_is_empty(&addrs)) {
        amxc_llist_it_t* it = amxc_llist_get_first(&addrs);
        addr_t* addr = amxc_container_of(it, addr_t, it);
        addr_destroy(addr, false);
        amxc_llist_it_take(it);
    }

    converter_destroy(&flags_converter);
    converter_destroy(&scope_converter);
}
