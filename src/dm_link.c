/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>
#include <linux/if.h>
#include <linux/if_arp.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "netdev_netlink.h"
#include "netdev_util.h"

#include "dm_netdev.h"
#include "dm_convert.h"
#include "dm_link.h"
#include "dm_link_route.h"

#define ME "netdev"

static void printLinkMsg(FILE* f, rtnlmsg_t* msg);
static void bridge_print(FILE* f, struct rtattr* data);

static rtnlhandler_t newLinkHandler = { RTM_NEWLINK, sizeof(struct ifinfomsg), handleNewLink, printLinkMsg };
static rtnlhandler_t delLinkHandler = { RTM_DELLINK, sizeof(struct ifinfomsg), handleDelLink, printLinkMsg };
static rtnlhandler_t getLinkHandler = { RTM_GETLINK, sizeof(struct ifinfomsg), NULL, printLinkMsg };

static converter_t* type_converter = NULL;
static converter_t* flags_converter = NULL;
static converter_t* state_converter = NULL;

static converter_t* bridge_stpstate_converter = NULL;

static amxc_llist_t link_infos = {NULL, NULL};
static amxc_llist_t links = {NULL, NULL};
static link_info_t bridge_info = {
    .kind = "bridge",
    .print = bridge_print,
};

static struct rtattr* nestedAttr(struct rtattr* rta, unsigned short type) {
    int rtalen = RTA_PAYLOAD(rta);
    for(rta = RTA_DATA(rta); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
        if(rta->rta_type == type) {
            return rta;
        }
    }
    return NULL;
}

static void s_set_mib(amxd_trans_t* transaction, const char* mib_name) {
    if(amxd_dm_get_mib(netdev_get_dm(), mib_name) == NULL) {
        int ret = amxo_parser_load_mib(netdev_get_parser(), netdev_get_dm(), mib_name);
        when_failed_trace(ret, exit, ERROR, "Error loading mib '%s'", mib_name);
    }
    // Add mib to NetDev.Link.N.
    amxd_trans_add_mib(transaction, mib_name);
exit:
    return;
}

void new_link_deferred_transaction(const amxc_var_t* data, void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t ret = amxd_status_invalid_value;
    deferred_trans_t* deferred_trans = (deferred_trans_t*) priv;
    amxd_trans_t* transaction = NULL;
    link_t* link = NULL;

    when_null_trace(deferred_trans, exit, ERROR, "No deferred transaction provided");
    transaction = deferred_trans->transaction;
    link = (link_t*) deferred_trans->priv;
    when_null_trace(transaction, exit, ERROR, "No transaction provided");
    when_null_trace(link, exit, ERROR, "No link structure provided");
    when_false_trace(link->active, exit, INFO, "Link set to be destroyed, not updating it anymore");

    SAH_TRACEZ_INFO(ME, "Handling transaction for NetDev.link.%d", link->index);

    ret = amxd_trans_apply(transaction, netdev_get_dm());
    if(ret != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply transaction, return %d", ret);
    }

    if(GETP_BOOL(data, "NewInstance")) {
        link->object = amxd_dm_findf(netdev_get_dm(), "NetDev.Link.%d.", link->index);
        if(link->object == NULL) {
            SAH_TRACEZ_ERROR(ME, "Could not get instance 'NetDev.Link.%d.'", link->index);
            link_destroy(link, false);
            goto exit;
        }
        link->object->priv = link;
        SAH_TRACEZ_INFO(ME, "Deferred transaction on NetDev.Link.%d. successful", link->index);
    }

exit:
    deferred_trans_clean(&deferred_trans);
    SAH_TRACEZ_OUT(ME);
}

static void link_deferred_destroy(const amxc_var_t* data, void* priv) {
    SAH_TRACEZ_IN(ME);
    link_t* link = (link_t*) priv;

    when_null(link, exit);
    if(GETP_BOOL(data, "deleteObject")) {
        int rv = -1;
        SAH_TRACEZ_INFO(ME, "Deleting NetDev.link.%d from the datamodel", link->index);
        amxd_trans_t transaction;
        amxd_trans_init(&transaction);
        amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
        amxd_trans_select_pathf(&transaction, "NetDev.Link.");
        amxd_trans_del_inst(&transaction, link->index, NULL);
        rv = amxd_trans_apply(&transaction, netdev_get_dm());
        if(amxd_status_ok != rv) {
            SAH_TRACEZ_WARNING(ME, "Failed to delete instance %d, return %d", link->index, rv);
        }
        amxd_trans_clean(&transaction);
    }
    amxc_llist_it_take(&link->it);
    free(link);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

link_t* link_create(int index) {
    SAH_TRACEZ_IN(ME);
    link_t* link = (link_t*) calloc(1, sizeof(link_t));

    SAH_TRACEZ_INFO(ME, "Creating new link with index '%d'", index);

    link->index = index;
    link->active = true;
    amxc_llist_it_init(&link->it);
    amxc_llist_append(&links, &link->it);
    SAH_TRACEZ_OUT(ME);
    return link;
}

void link_destroy(link_t* link, bool deleteObject) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t var_deferred_data;

    amxc_var_init(&var_deferred_data);
    amxc_var_set_type(&var_deferred_data, AMXC_VAR_ID_HTABLE);

    when_null_trace(link, exit, ERROR, "No link provided, can not remove link");
    when_false_trace(link->active, exit, INFO, "Link already set to be destroyed");

    amxc_var_add_key(bool, &var_deferred_data, "deleteObject", deleteObject);
    // Set to inactive to prevent use between this point and deferred_destroy call
    link->active = false;
    if(link->object) {
        link->object->priv = NULL;
        link->object = NULL;
    }
    amxp_sigmngr_deferred_call(NULL, link_deferred_destroy, &var_deferred_data, link);

exit:
    amxc_var_clean(&var_deferred_data);
    SAH_TRACEZ_OUT(ME);
    return;
}

link_t* link_find(int index) {
    SAH_TRACEZ_IN(ME);
    link_t* link = NULL;
    amxc_llist_for_each(it, &links) {
        link = amxc_container_of(it, link_t, it);
        if((link->index == index) && (link->active == true)) {
            break;
        }
        link = NULL;
    }
    SAH_TRACEZ_OUT(ME);
    return link;
}

int update_link_info_from_msg(link_t* link, rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct ifinfomsg* ifinfo = NULL;
    struct rtattr* rta = NULL;

    when_null_trace(link, exit, ERROR, "No link structure provided");
    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    ifinfo = (struct ifinfomsg*) rtnlmsg_hdr(msg);
    when_null_trace(ifinfo, exit, ERROR, "No interface info message found");
    link->type = ifinfo->ifi_type;
    link->flags = ifinfo->ifi_flags;

    rta = rtnlmsg_attr(msg, IFLA_IFNAME);
    if(rta != NULL) {
        snprintf(link->name, LINKNAMESPACE, "%s", (const char*) RTA_DATA(rta));
    }
    rta = rtnlmsg_attr(msg, IFLA_ADDRESS);
    if(rta != NULL) {
        lladdr_assign(&link->lladdr, RTA_PAYLOAD(rta), (unsigned char*) RTA_DATA(rta));
    }
    rta = rtnlmsg_attr(msg, IFLA_TXQLEN);
    if(rta != NULL) {
        link->txqueuelen = *(unsigned int*) RTA_DATA(rta);
    }
    rta = rtnlmsg_attr(msg, IFLA_MTU);
    if(rta != NULL) {
        link->mtu = *(unsigned int*) RTA_DATA(rta);
    }
    rta = rtnlmsg_attr(msg, IFLA_OPERSTATE);
    if(rta != NULL) {
        link->state = *(unsigned char*) RTA_DATA(rta);

        // If name == lo and state == unknown (0) -> set state to up (6)
        if((strcmp(link->name, "lo") == 0) && (link->state == 0)) {
            link->state = 6;
        }
        // If name == wwan* and state == unknown (0) -> set state to up (6)
        if((strncmp(link->name, "wwan", 4) == 0) && (link->state == 0)) {
            link->state = 6;
        }
        if((link->type == ARPHRD_PPP) && (link->state == 0)) {
            link->state = 6;
        }
        if((link->type == ARPHRD_TUNNEL6) && (link->state == 0)) {
            link->state = 6;
        }
    }

    rta = rtnlmsg_attr(msg, IFLA_MASTER);
    if(rta != NULL) {
        link->master = *(unsigned int*) RTA_DATA(rta);
    }

    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_trans_t* create_transaction_from_link(link_t* link, bool new_inst) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t* trans = NULL;
    amxd_trans_new(&trans);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    amxd_trans_set_attr(trans, amxd_tattr_change_priv, true);

    when_null_trace(link, exit, ERROR, "No link structure provided");

    if(new_inst) {
        amxd_trans_select_pathf(trans, "NetDev.Link.");
        amxd_trans_add_inst(trans, link->index, NULL);
        SAH_TRACEZ_INFO(ME, "Creating new NetDev.Link.%d. with transaction", link->index);
    } else {
        SAH_TRACEZ_INFO(ME, "Selecting NetDev.Link.%d. for transaction", link->index);
        amxd_trans_select_pathf(trans, "NetDev.Link.%d", link->index);
    }

    amxd_trans_set_value(cstring_t, trans, "Name", link->name);
    if(new_inst) {
        amxd_trans_set_value(cstring_t, trans, "Alias", link->name);
    }

    amxd_trans_set_value(cstring_t, trans, "Type", convert_bin2str(type_converter, link->type));
    amxd_trans_set_value(cstring_t, trans, "Flags", convert_bin2str(flags_converter, link->flags));
    amxd_trans_set_value(cstring_t, trans, "LLAddress", lladdr2str(&link->lladdr));
    amxd_trans_set_value(cstring_t, trans, "State", convert_bin2str(state_converter, link->state));
    amxd_trans_set_value(int32_t, trans, "Index", link->index);
    amxd_trans_set_value(uint32_t, trans, "TxQueueLen", link->txqueuelen);
    amxd_trans_set_value(uint32_t, trans, "MTU", link->mtu);
    amxd_trans_set_value(uint32_t, trans, "Master", link->master);

exit:
    SAH_TRACEZ_OUT(ME);
    return trans;
}

int add_bridge_info_to_transaction(link_t* link, rtnlmsg_t* msg, amxd_trans_t* trans, bool new_inst) {
    int rv = -1;
    struct rtattr* rta = NULL;
    const char* kind_str = "undefined";
    bool link_is_bridge = false;
    bool link_is_port = false;
    struct rtattr* bridge_data = NULL;

    when_null_trace(link, exit, ERROR, "No link structure provided");
    when_null_trace(trans, exit, ERROR, "No transaction provided");
    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    // Process LINKINFO / bridge
    rta = rtnlmsg_attr(msg, IFLA_LINKINFO);
    if(rta != NULL) {
        struct rtattr* kind = nestedAttr(rta, IFLA_INFO_KIND);
        if(kind != NULL) {
            kind_str = RTA_DATA(kind);
            SAH_TRACEZ_INFO(ME, "'kind' = '%s' found ", kind_str);
            if(!strcmp("bridge", kind_str)) {
                link_is_bridge = true;
                bridge_data = nestedAttr(rta, IFLA_INFO_DATA);
            }
        }
    }

    // Note: low confidence if this is the proper way to identify a port.
    link_is_port = link->type == ARPHRD_ETHER && !link_is_bridge;

    if(link_is_port) {
        // Note: for ports, "kind" as reported by netlink is undefined, so this deviates from
        //       what netlink technically reports.
        kind_str = "bridge_port";
    }

    amxd_trans_set_value(cstring_t, trans, "Kind", kind_str);

    if(new_inst) {
        if(link_is_bridge) {
            s_set_mib(trans, "bridge");
        } else if(link_is_port) {
            s_set_mib(trans, "bridge_port");
        }
    }

    if((bridge_data != NULL)) {
        struct rtattr* data = bridge_data;
        amxd_trans_select_pathf(trans, "NetDev.Link.%d.STP.", link->index);

        // fill in STP data
        int datalen = RTA_PAYLOAD(data);
        for(data = RTA_DATA(data); RTA_OK(data, datalen); data = RTA_NEXT(data, datalen)) {
            switch(data->rta_type) {
            case IFLA_BR_FORWARD_DELAY:
                amxd_trans_set_value(uint32_t, trans, "ForwardDelay", *(int32_t*) RTA_DATA(data));
                break;
            case IFLA_BR_HELLO_TIME:
                amxd_trans_set_value(uint32_t, trans, "HelloTime", *(int32_t*) RTA_DATA(data));
                break;
            case IFLA_BR_MAX_AGE:
                amxd_trans_set_value(uint32_t, trans, "MaxAge", *(int32_t*) RTA_DATA(data));
                break;
            case IFLA_BR_AGEING_TIME:
                amxd_trans_set_value(uint32_t, trans, "AgingTime", *(int32_t*) RTA_DATA(data));
                break;
            case IFLA_BR_STP_STATE:
                amxd_trans_set_value(cstring_t, trans, "State",
                                     convert_bin2str(bridge_stpstate_converter, *(int32_t*) RTA_DATA(data)));
                break;
            case IFLA_BR_PRIORITY:
                amxd_trans_set_value(uint32_t, trans, "Priority", *(int32_t*) RTA_DATA(data));
                break;
            default:
                SAH_TRACEZ_INFO(ME, " attr[%u](%lu byte%s)", data->rta_type, (long unsigned int) RTA_PAYLOAD(data), RTA_PAYLOAD(data) == 1 ? "" : "s");
                break;
            }
        }
    }
    rv = 0;

exit:
    return rv;
}

static void copy_stats_to_stats_obj(link_t* link, rtnlmsg_t* msg) {
    struct rtattr* rta = NULL;

    when_null_trace(link, exit, ERROR, "No link structure provided");
    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
    rta = rtnlmsg_attr(msg, IFLA_STATS64);
#else
    rta = rtnlmsg_attr(msg, IFLA_STATS);
#endif
    if(rta != NULL) {
        amxd_object_t* stats_obj = amxd_dm_findf(netdev_get_dm(), "NetDev.Link.%d.Stats.", link->index);
#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
        struct rtnl_link_stats64* stats = (struct rtnl_link_stats64*) RTA_DATA(rta);
        size_t size = sizeof(struct rtnl_link_stats64);
#else
        struct rtnl_link_stats* stats = (struct rtnl_link_stats*) RTA_DATA(rta);
        size_t size = sizeof(struct rtnl_link_stats);
#endif
        if(stats_obj != NULL) {
            if(stats_obj->priv == NULL) {
                stats_obj->priv = calloc(1, size);
            }
            memcpy(stats_obj->priv, stats, size);
        }
    }
exit:
    return;
}

int handleNewLink(rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct ifinfomsg* ifinfo = NULL;
    struct rtattr* rta = NULL;
    link_t* link = NULL;
    bool new_inst = false;
    amxc_var_t deferred_data;
    deferred_trans_t* deferred_trans = NULL;
    amxd_trans_t* transaction = NULL;
    amxc_var_init(&deferred_data);
    amxc_var_set_type(&deferred_data, AMXC_VAR_ID_HTABLE);

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");
    ifinfo = (struct ifinfomsg*) rtnlmsg_hdr(msg);
    when_null_trace(ifinfo, exit, ERROR, "No interface info message found");
    link = link_find(ifinfo->ifi_index);

    if(link) {
        struct nlmsghdr* nlmsghdr = rtnlmsg_nlhdr(msg);
        char new_name[LINKNAMESPACE];
        memset(new_name, 0, LINKNAMESPACE);
        SAH_TRACEZ_INFO(ME, "Found existing link with index %d", link->index);
        when_null_trace(nlmsghdr, exit, ERROR, "No netlink msg header found");
        if(nlmsghdr->nlmsg_seq < link->seqthreshold) {
            amxd_trans_delete(&transaction);
            goto exit;
        } else {
            link->seqthreshold = 0;
        }
        rta = rtnlmsg_attr(msg, IFLA_IFNAME);
        if(rta != NULL) {
            snprintf(new_name, LINKNAMESPACE, "%s", (const char*) RTA_DATA(rta));
        }
        if(strncmp(link->name, new_name, LINKNAMESPACE) != 0) {
            SAH_TRACEZ_WARNING(ME, "Recreating Link %s as %s", link->name, new_name);
            link_destroy(link, true);
            new_inst = true;
        }
    } else {
        rta = rtnlmsg_attr(msg, IFLA_WIRELESS);
        when_not_null_trace(rta, exit, INFO, "Wireless event, ignoring as new interface");
        SAH_TRACEZ_INFO(ME, "Found a new link with index %d", ifinfo->ifi_index);
        new_inst = true;
    }

    amxc_var_add_key(bool, &deferred_data, "NewInstance", new_inst);
    if(new_inst) {
        link = link_create(ifinfo->ifi_index);
    }

    when_failed_trace(update_link_info_from_msg(link, msg), exit, ERROR, "Failed to update the link info");
    if(!(link->flags & IFF_UP)) {
        route_deleteIPv4Routes(link->index);
    }

    transaction = create_transaction_from_link(link, new_inst);
    when_null_trace(transaction, exit, ERROR, "Failed to create transaction from link");
    add_bridge_info_to_transaction(link, msg, transaction, new_inst);
    deferred_trans_create(&deferred_trans, transaction, link);
    rv = amxp_sigmngr_deferred_call(NULL, new_link_deferred_transaction, &deferred_data, deferred_trans);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to defer the new link transaction");
        deferred_trans_clean(&deferred_trans);
    }

    copy_stats_to_stats_obj(link, msg);

    rv = 0;
exit:
    amxc_var_clean(&deferred_data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int handleDelLink(rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct ifinfomsg* ifinfo = NULL;
    link_t* link = NULL;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    ifinfo = (struct ifinfomsg*) rtnlmsg_hdr(msg);
    when_null_trace(ifinfo, exit, ERROR, "No interface info message found");
    link = link_find(ifinfo->ifi_index);
    when_null_trace(link, exit, ERROR, "No link could be found to remove");

    update_link_info_from_msg(link, msg);
    when_true_trace((link->flags & (IFF_UP | IFF_RUNNING)), exit, INFO, "Interface is still up and running, not deleting it");

    rtnlmsg_initialize(msg, RTM_GETLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST);
    ifinfo = (struct ifinfomsg*) rtnlmsg_hdr(msg);
    ifinfo->ifi_index = link->index;
    rtnl_send(netdev_get_nlfd(), msg);
    link_destroy(link, true);

    rv = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void printLinkMsg(FILE* f, rtnlmsg_t* msg) {
    struct ifinfomsg* ifinfo = NULL;
    bool breakout = false;
    struct rtattr* rta = NULL;
    unsigned int rtalen = 0;

    fprintf(f, "%s{",
            rtnlmsg_type(msg) == RTM_NEWLINK ? "RTM_NEWLINK" :
            rtnlmsg_type(msg) == RTM_DELLINK ? "RTM_DELLINK" :
            rtnlmsg_type(msg) == RTM_GETLINK ? "RTM_GETLINK" : "<UNKNOWN>");

    ifinfo = (struct ifinfomsg*) rtnlmsg_hdr(msg);

    fprintf(f, " index=%d type=%d(%s)", ifinfo->ifi_index, ifinfo->ifi_type, convert_bin2str(type_converter, ifinfo->ifi_type));
    fprintf(f, " flags=0x%x(%s)", ifinfo->ifi_flags, convert_bin2str(flags_converter, ifinfo->ifi_flags));
    fprintf(f, " change=0x%x(%s)", ifinfo->ifi_change, convert_bin2str(flags_converter, ifinfo->ifi_change));

    rtalen = rtnlmsg_attrLen(msg);
    for(rta = rtnlmsg_attrFirst(msg); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
        switch(rta->rta_type) {
        case IFLA_IFNAME:    fprintf(f, " name=%s", (const char*) RTA_DATA(rta)); break;
        case IFLA_ADDRESS:   fprintf(f, " lladdress=%s", lladdr2str(bin2lladdr(RTA_PAYLOAD(rta), (unsigned char*) RTA_DATA(rta)))); break;
        case IFLA_BROADCAST: fprintf(f, " broadcast=%s", lladdr2str(bin2lladdr(RTA_PAYLOAD(rta), (unsigned char*) RTA_DATA(rta)))); break;
        case IFLA_TXQLEN:    fprintf(f, " txqlen=%u", *(unsigned int*) RTA_DATA(rta)); break;
        case IFLA_MTU:       fprintf(f, " mtu=%u", *(unsigned int*) RTA_DATA(rta)); break;
        case IFLA_QDISC:     fprintf(f, " qdisc=%s", (const char*) RTA_DATA(rta)); break;
        case IFLA_MAP:       fprintf(f, " map"); break;
#ifdef CONFIG_64_BITS_NETWORK_STATISTICS
        case IFLA_STATS64:   fprintf(f, " stats"); break;
#else
        case IFLA_STATS:     fprintf(f, " stats"); break;
#endif
        case IFLA_LINKMODE:  fprintf(f, " linkmode=%u", *(unsigned char*) RTA_DATA(rta)); break;
        case IFLA_OPERSTATE: {
            unsigned char operstate = *(unsigned char*) RTA_DATA(rta);
            fprintf(f, " operstate=%u(%s)", operstate, convert_bin2str(state_converter, operstate));
            break;
        }
        case IFLA_LINKINFO: {
            link_info_t* info = NULL;
            struct rtattr* kind = nestedAttr(rta, IFLA_INFO_KIND);
            struct rtattr* data = nestedAttr(rta, IFLA_INFO_DATA);

            if(!kind || !data) {
                break;
            }
            amxc_llist_iterate(it, &link_infos) {
                info = amxc_container_of(it, link_info_t, it);
                if(!strcmp(info->kind, RTA_DATA(kind))) {
                    break;
                }
            }
            fprintf(f, " %s=", (const char*) RTA_DATA(kind));
            if(info && info->print) {
                info->print(f, data);
            } else {
                fprintf(f, "(%" PRIu32 "byte%s)", (uint32_t) RTA_PAYLOAD(data), RTA_PAYLOAD(data) == 1 ? "" : "s");
            }
            break;
        }
        default: {
            fprintf(f, " attr[%u](%" PRIu32 "byte%s)", rta->rta_type, (uint32_t) RTA_PAYLOAD(rta), RTA_PAYLOAD(rta) == 1 ? "" : "s");
            if(rta->rta_type > IFLA_MAX) {
                SAH_TRACEZ_ERROR(ME, "Got rta with unknown rta_type=%u > IFLA_MAX=%u (rtnlmsg=%p rta=%p)", rta->rta_type, IFLA_MAX, msg, rta);
                breakout = true;
            }
            break;
        }
        }

        if(breakout) {
            break;
        }
    }
    fprintf(f, " }");
}

static void bridge_print(FILE* f, struct rtattr* data) {
    int datalen = RTA_PAYLOAD(data);
    fprintf(f, "{");
    for(data = RTA_DATA(data); RTA_OK(data, datalen); data = RTA_NEXT(data, datalen)) {
        switch(data->rta_type) {
        case IFLA_BR_FORWARD_DELAY:
            fprintf(f, " forward_delay=%d", *(int32_t*) RTA_DATA(data));
            break;
        case IFLA_BR_HELLO_TIME:
            fprintf(f, " hello_time=%d", *(int32_t*) RTA_DATA(data));
            break;
        case IFLA_BR_MAX_AGE:
            fprintf(f, " max_age=%d", *(int32_t*) RTA_DATA(data));
            break;
        case IFLA_BR_AGEING_TIME:
            fprintf(f, " ageing_time=%d", *(int32_t*) RTA_DATA(data));
            break;
        case IFLA_BR_STP_STATE:
            fprintf(f, " stp_state=%s", convert_bin2str(bridge_stpstate_converter, *(int32_t*) RTA_DATA(data)));
            break;
        case IFLA_BR_PRIORITY:
            fprintf(f, " priority=%d", *(int32_t*) RTA_DATA(data));
            break;
        default:
            fprintf(f, " attr[%u](%lu byte%s)", data->rta_type, (long unsigned int) RTA_PAYLOAD(data), RTA_PAYLOAD(data) == 1 ? "" : "s");
            break;
        }
    }
    fprintf(f, " }");
}

converter_t* init_link_type_converter(void) {
    converter_t* converter = converter_create("test_link_type", converter_attribute_fallback);
    converter_loadEntry(converter, ARPHRD_ETHER, "ether");
    converter_loadEntry(converter, ARPHRD_ATM, "atm");
    converter_loadEntry(converter, ARPHRD_VOID, "void");
    converter_loadEntry(converter, ARPHRD_NONE, "none");
    converter_loadEntry(converter, ARPHRD_PPP, "ppp");
    converter_loadEntry(converter, ARPHRD_TUNNEL, "tunnel");
    converter_loadEntry(converter, ARPHRD_TUNNEL6, "tunnel6");
    converter_loadEntry(converter, ARPHRD_LOOPBACK, "loopback");
    return converter;
}

converter_t* init_link_flags_converter(void) {
    converter_t* converter = converter_create("link_flags", converter_attribute_flags);
    converter_loadEntry(converter, IFF_UP, "up");
    converter_loadEntry(converter, IFF_BROADCAST, "broadcast");
    converter_loadEntry(converter, IFF_DEBUG, "debug");
    converter_loadEntry(converter, IFF_LOOPBACK, "loopback");
    converter_loadEntry(converter, IFF_POINTOPOINT, "pointopoint");
    converter_loadEntry(converter, IFF_NOTRAILERS, "notrailers");
    converter_loadEntry(converter, IFF_RUNNING, "running");
    converter_loadEntry(converter, IFF_NOARP, "noarp");
    converter_loadEntry(converter, IFF_PROMISC, "promisc");
    converter_loadEntry(converter, IFF_ALLMULTI, "allmulti");
    converter_loadEntry(converter, IFF_MASTER, "master");
    converter_loadEntry(converter, IFF_SLAVE, "slave");
    converter_loadEntry(converter, IFF_MULTICAST, "multicast");
    converter_loadEntry(converter, IFF_PORTSEL, "portsel");
    converter_loadEntry(converter, IFF_AUTOMEDIA, "automedia");
    converter_loadEntry(converter, IFF_DYNAMIC, "dynamic");
    converter_loadEntry(converter, IFF_NOMULTIPATH, "nomultipath");
    return converter;
}

converter_t* init_link_state_converter(void) {
    converter_t* converter = converter_create("link_state", converter_attribute_fallback);
    converter_loadEntry(converter, 0, "unknown");
    converter_loadEntry(converter, 1, "notpresent");
    converter_loadEntry(converter, 2, "down");
    converter_loadEntry(converter, 3, "lowerlayerdown");
    converter_loadEntry(converter, 4, "testing");
    converter_loadEntry(converter, 5, "dormant");
    converter_loadEntry(converter, 6, "up");
    return converter;
}

converter_t* init_link_bridge_stpstate_converter(void) {
    converter_t* converter = converter_create("stpstate", converter_attribute_fallback);
    converter_loadEntry(converter, 0, "Disabled");
    converter_loadEntry(converter, 1, "Listening");
    converter_loadEntry(converter, 2, "Learning");
    converter_loadEntry(converter, 3, "Forwarding");
    converter_loadEntry(converter, 4, "Blocking");
    converter_loadDefault(converter, "Broken");
    return converter;
}

void link_init_converters(void) {
    type_converter = init_link_type_converter();
    flags_converter = init_link_flags_converter();
    state_converter = init_link_state_converter();
    bridge_stpstate_converter = init_link_bridge_stpstate_converter();
}

bool dm_link_init(void) {
    bool rv = false;

    link_init_converters();
    link_register_info(&bridge_info);

    when_failed(rtnl_register_handler(&newLinkHandler), exit);
    when_failed(rtnl_register_handler(&delLinkHandler), exit);
    when_failed(rtnl_register_handler(&getLinkHandler), exit);

    rv = true;
exit:
    return rv;
}

bool dm_link_start(void) {
    SAH_TRACEZ_IN(ME);
    bool retval = false;

    rtnlmsg_t* msg = rtnlmsg_instance();
    rtnlmsg_initialize(msg, RTM_GETLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST | NLM_F_DUMP);

    when_false_trace(rtnl_send(netdev_get_nlfd(), msg), exit, ERROR, "Link start failed (rtnl_send)");
    when_false_trace(rtnl_read(netdev_get_nlfd(), msg, true), exit, ERROR, "Link start failed (rtnl_read)");

    SAH_TRACEZ_INFO(ME, "Link start OK");
    retval = true;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

void dm_link_cleanup(void) {
    while(!amxc_llist_is_empty(&links)) {
        amxc_llist_it_t* it = amxc_llist_get_first(&links);
        link_t* link = amxc_container_of(it, link_t, it);
        link_destroy(link, false);
        amxc_llist_it_take(&link->it);
    }

    converter_destroy(&type_converter);
    converter_destroy(&flags_converter);
    converter_destroy(&state_converter);
    converter_destroy(&bridge_stpstate_converter);
}

bool read_link(amxd_object_t* object) {
    SAH_TRACEZ_IN(ME);

    int index = amxd_object_get_index(object);
    rtnlmsg_t* msg = NULL;
    struct ifinfomsg* ifinfo = NULL;

    when_true(index <= 0, exit);

    msg = rtnlmsg_instance();
    rtnlmsg_initialize(msg, RTM_GETLINK, sizeof(struct ifinfomsg), NLM_F_REQUEST);
    ifinfo = (struct ifinfomsg*) rtnlmsg_hdr(msg);

    ifinfo->ifi_index = index;
    if(!rtnl_send(netdev_get_nlfd(), msg)) {
        SAH_TRACEZ_NOTICE(ME, "rtnl_send failed!");
        goto exit;
    }
    rtnl_read(netdev_get_nlfd(), msg, true);

exit:
    SAH_TRACEZ_OUT(ME);
    return true;
}

bool read_stats(amxd_object_t* object) {
    return read_link(amxd_object_get_parent(object));
}

void link_register_info(link_info_t* info) {
    amxc_llist_append(&link_infos, &info->it);
}

void link_unregister_info(link_info_t* info) {
    amxc_llist_it_take(&info->it);
}
