/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/rtnetlink.h>

#include "netdev_netlink.h"
#include "netdev_util.h"
#include <net/if.h>

#include "dm_netdev.h"
#include "dm_convert.h"
#include "dm_link_route.h"
#include "netdev_bridgetable.h"


#define ME "netdev"

struct rtmetric {
    struct rtattr hdr;
    unsigned int data;
};

static amxc_llist_t routes = {NULL, NULL};

static converter_t* table_converter = NULL;
static converter_t* protocol_converter = NULL;
static converter_t* scope_converter = NULL;
static converter_t* type_converter = NULL;


static bool route_match(route_t* r1, route_t* r2) {
    bool rv = false;
    when_null_trace(r1, exit, ERROR, "First route is invalid, can not try to match");
    when_null_trace(r2, exit, ERROR, "Second route is invalid, can not try to match");

    rv = (r1->family == r2->family) &&
        (r1->dstlen == r2->dstlen) &&
        (r1->table == r2->table) &&
        (r1->protocol == r2->protocol) &&
        (r1->scope == r2->scope) &&
        (r1->type == r2->type) &&
        (r1->priority == r2->priority) &&
        (r1->oif == r2->oif) &&
        (memcmp(r1->dst, r2->dst, addrlen(r1->family)) == 0);

exit:
    return rv;
}

route_t* route_find(route_t* ref) {
    route_t* route = NULL;
    when_null_trace(ref, exit, ERROR, "No reference provided to find route");
    amxc_llist_iterate(it, &routes) {
        route = amxc_container_of(it, route_t, it);
        if(route_match(route, ref) && (route->active == true)) {
            break;
        }
        route = NULL;
    }
exit:
    return route;
}

static void route_deferred_destroy(const amxc_var_t* data, void* priv) {
    SAH_TRACEZ_IN(ME);
    route_t* route = (route_t*) priv;

    when_null(route, exit);

    if(GETP_BOOL(data, "deleteObject")) {
        amxd_trans_t transaction;
        amxd_trans_init(&transaction);
        amxd_trans_select_pathf(&transaction, "NetDev.Link.%d.%sRoute.",
                                route->oif, family2str(route->family));
        amxd_trans_del_inst(&transaction, route->index, NULL);
        if(amxd_status_ok != amxd_trans_apply(&transaction, netdev_get_dm())) {
            SAH_TRACEZ_WARNING(ME, "Transaction failed");
        }
        amxd_trans_clean(&transaction);
    }
    amxp_timer_delete(&route->mercytimer);
    amxc_llist_it_take(&route->it);
    free(route);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void route_destroy(route_t* route, bool deleteObject) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t var_deferred_data;

    amxc_var_init(&var_deferred_data);
    amxc_var_set_type(&var_deferred_data, AMXC_VAR_ID_HTABLE);

    when_null_trace(route, exit, ERROR, "No route provided, can not remove route");
    when_false_trace(route->active, exit, INFO, "Route already set to be destroyed");

    amxc_var_add_key(bool, &var_deferred_data, "deleteObject", deleteObject);
    // Set to inactive to prevent use between this point and deferred_destroy call
    route->active = false;
    if(route->object != NULL) {
        route->object->priv = NULL;
        route->object = NULL;
    }
    amxp_sigmngr_deferred_call(NULL, route_deferred_destroy, &var_deferred_data, route);

exit:
    amxc_var_clean(&var_deferred_data);
    SAH_TRACEZ_OUT(ME);
    return;
}

// callback function; answers to amxp_timer_cb_t signature
static void route_expireMercy(UNUSED amxp_timer_t* timer, void* userdata) {
    route_destroy((route_t*) userdata, true);
}

route_t* route_create(void) {
    route_t* route = (route_t*) calloc(1, sizeof(route_t));
    amxp_timer_new(&route->mercytimer, route_expireMercy, route);
    amxc_llist_it_init(&route->it);
    amxc_llist_append(&routes, &route->it);
    route->active = true;
    route->default_route = false;
    return route;
}

int route_copy(route_t* dst, route_t* src) {
    int rv = -1;

    when_null_trace(dst, exit, ERROR, "No destination route struct provided");
    when_null_trace(src, exit, ERROR, "No source route struct provided");

    dst->family = src->family;
    dst->dstlen = src->dstlen;
    dst->table = src->table;
    dst->protocol = src->protocol;
    dst->scope = src->scope;
    dst->type = src->type;
    memcpy(dst->dst, src->dst, addrlen(dst->family));
    dst->priority = src->priority;
    dst->oif = src->oif;
    memcpy(dst->gateway, src->gateway, addrlen(dst->family));
    memcpy(dst->prefsrc, src->prefsrc, addrlen(dst->family));
    dst->mtu = src->mtu;
    dst->advmss = src->advmss;
    dst->hoplimit = src->hoplimit;

    rv = 0;
exit:
    return rv;
}

/**
 * Used to convert a rtnetlink message to a route struct
 * @param route empty route struct to be filled with the data in the message.
 *  Do NOT provide a route that contains any data or that is put in the routes list
 * (so no route that was created with the route_create function)
 * @param msg rtnl message
 * @return 0 if ok, -1 in case of error
 */
int msg2route(route_t* route, rtnlmsg_t* msg) {
    int rv = -1;
    struct rtmsg* rt = NULL;
    struct rtattr* rta = NULL;

    when_null_trace(route, exit, ERROR, "No route structure provided");
    memset(route, 0, sizeof(route_t));
    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    rt = (struct rtmsg*) rtnlmsg_hdr(msg);

    route->family = rt->rtm_family;
    route->dstlen = rt->rtm_dst_len;
    route->table = rt->rtm_table; // ignore the RTA_TABLE attribute and only consider rtmsg->rtm_table.
    route->protocol = rt->rtm_protocol;
    route->scope = rt->rtm_scope;
    route->type = rt->rtm_type;

    if((rta = rtnlmsg_attr(msg, RTA_DST))) {
        memcpy(route->dst, RTA_DATA(rta), addrlen(route->family));
    }
    if((rta = rtnlmsg_attr(msg, RTA_PRIORITY))) {
        route->priority = *(int*) RTA_DATA(rta);
    }
    if((rta = rtnlmsg_attr(msg, RTA_OIF))) {
        route->oif = *(int*) RTA_DATA(rta);
    }
    if((rta = rtnlmsg_attr(msg, RTA_GATEWAY))) {
        memcpy(route->gateway, RTA_DATA(rta), addrlen(route->family));
    }
    if((rta = rtnlmsg_attr(msg, RTA_PREFSRC))) {
        memcpy(route->prefsrc, RTA_DATA(rta), addrlen(route->family));
    }
    if((rta = rtnlmsg_attr(msg, RTA_METRICS))) {
        struct rtmetric* metric, * metric0 = (struct rtmetric*) RTA_DATA(rta);
        int nmetrics = RTA_PAYLOAD(rta) / NLMSG_ALIGN(sizeof(struct rtmetric));
        for(metric = metric0; metric - metric0 < nmetrics; metric++) {
            switch(metric->hdr.rta_type) {
            case RTAX_MTU:        route->mtu = metric->data; break;
            case RTAX_ADVMSS:     route->advmss = metric->data; break;
            case RTAX_HOPLIMIT:   route->hoplimit = metric->data; break;
            }
        }
    }

    rv = 0;
exit:
    return rv;
}

static void new_route_deferred_transaction(const amxc_var_t* data, void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t ret = amxd_status_invalid_value;
    deferred_trans_t* deferred_trans = (deferred_trans_t*) priv;
    amxd_trans_t* transaction = NULL;
    route_t* route = NULL;

    when_null_trace(deferred_trans, exit, ERROR, "No deferred transaction provided");
    transaction = deferred_trans->transaction;
    route = (route_t*) deferred_trans->priv;
    when_null_trace(transaction, exit, ERROR, "No transaction provided");
    when_null_trace(route, exit, ERROR, "No addr structure provided");
    when_false_trace(route->active, exit, INFO, "Route set to be destroyed, not updating it anymore");
    when_false_trace(route->active, exit, ERROR, "Cannot add inactive route");

    ret = amxd_trans_apply(transaction, netdev_get_dm());
    if(ret != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to apply transaction, return %d", ret);
    }

    if(GETP_BOOL(data, "new_instance")) {
        route->object = amxd_dm_findf(netdev_get_dm(), "NetDev.Link.%d.%sRoute.%s",
                                      route->oif, family2str(route->family), GETP_CHAR(data, "pathbuf"));
        if(route->object == NULL) {
            route_destroy(route, false);
            goto exit;
        }
        route->index = route->object->index;
        route->object->priv = route;
    }
    SAH_TRACEZ_INFO(ME, "Deferred route transaction successful");

exit:
    deferred_trans_clean(&deferred_trans);
    SAH_TRACEZ_OUT(ME);
}

amxd_trans_t* create_transaction_from_route(route_t* route, bool new_inst) {
    SAH_TRACEZ_IN(ME);
    char pathbuf[40];
    amxd_trans_t* trans = NULL;

    when_null_trace(route, exit, ERROR, "No route provide to create transaction");

    amxd_trans_new(&trans);
    amxd_trans_set_attr(trans, amxd_tattr_change_ro, true);
    amxd_trans_set_attr(trans, amxd_tattr_change_priv, true);

    if(new_inst) {
        sprintf(pathbuf, "dyn%u", route->dyn_index);
        amxd_trans_select_pathf(trans, "NetDev.Link.%d.%sRoute", route->oif, family2str(route->family));
        amxd_trans_add_inst(trans, 0, pathbuf);
    } else {
        amxd_trans_select_pathf(trans, "NetDev.Link.%d.%sRoute.dyn%u.", route->oif, family2str(route->family), route->dyn_index);
    }

    amxd_trans_set_value(uint8_t, trans, "DstLen", route->dstlen);
    amxd_trans_set_value(cstring_t, trans, "Table", convert_bin2str(table_converter, route->table));
    amxd_trans_set_value(cstring_t, trans, "Protocol", convert_bin2str(protocol_converter, route->protocol));
    amxd_trans_set_value(cstring_t, trans, "Scope", convert_bin2str(scope_converter, route->scope));
    amxd_trans_set_value(cstring_t, trans, "Type", convert_bin2str(type_converter, route->type));
    amxd_trans_set_value(cstring_t, trans, "Dst", addr2str(route->family, route->dst, false));
    amxd_trans_set_value(int32_t, trans, "Priority", route->priority);
    amxd_trans_set_value(cstring_t, trans, "Gateway", addr2str(route->family, route->gateway, true));
    amxd_trans_set_value(cstring_t, trans, "PrefSrc", addr2str(route->family, route->prefsrc, true));
    amxd_trans_set_value(uint32_t, trans, "AdvMSS", route->advmss);
    amxd_trans_set_value(uint32_t, trans, "MTU", route->mtu);
    amxd_trans_set_value(uint32_t, trans, "HopLimit", route->hoplimit);

exit:
    SAH_TRACEZ_OUT(ME);
    return trans;
}

int handleNewRoute(rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    route_t ws;
    route_t* route = NULL;
    char pathbuf[40];
    deferred_trans_t* deferred_trans = NULL;
    amxd_trans_t* transaction = NULL;
    amxc_var_t var_deferred_data;
    char* gateway = NULL;

    amxc_var_init(&var_deferred_data);
    amxc_var_set_type(&var_deferred_data, AMXC_VAR_ID_HTABLE);

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    when_failed_trace(msg2route(&ws, msg), exit, ERROR, "Failed to convert msg to route");
    // ignore some of the routes as hack to avoid abundance of uninteresting routing events on the target
    when_true_status((ws.protocol == RTPROT_UNSPEC), exit, rv = 0);
    when_true_status((ws.table == RT_TABLE_LOCAL), exit, rv = 0);

    route = route_find(&ws);
    if(route != NULL) {
        SAH_TRACEZ_INFO(ME, "Existing 'route' found");
        amxp_timer_stop(route->mercytimer);
    } else {
        SAH_TRACEZ_INFO(ME, "Creating new 'route'; NetDev.Link.%d.%sRoute", ws.oif, family2str(ws.family));
        route = route_create();
        route->dyn_index = ({static unsigned counter = 0; counter++;});
        sprintf(pathbuf, "dyn%u", route->dyn_index);

        amxc_var_add_key(bool, &var_deferred_data, "new_instance", true);
        amxc_var_add_key(cstring_t, &var_deferred_data, "pathbuf", pathbuf);
        SAH_TRACEZ_INFO(ME, "pathbuf = %s", pathbuf);
    }
    route_copy(route, &ws);
    if(set_if_default_route(route)) {
        gateway = addr2str_dyn(route->family, route->gateway);
        SAH_TRACEZ_INFO(ME, "Default route with gateway:%s !", gateway);
    }

    transaction = create_transaction_from_route(route, GET_BOOL(&var_deferred_data, "new_instance"));
    when_null_trace(transaction, exit, ERROR, "Failed to create transaction");

    deferred_trans_create(&deferred_trans, transaction, route);
    rv = amxp_sigmngr_deferred_call(NULL, new_route_deferred_transaction, &var_deferred_data, deferred_trans);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to defer the new route transaction");
        deferred_trans_clean(&deferred_trans);
    }

    //Test if the route is default or not. If yes, send event
    if(default_route_event(route, true) != 0) {
        SAH_TRACEZ_INFO(ME, "No default route event to send");
    }
exit:
    free(gateway);
    amxc_var_clean(&var_deferred_data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int handleDelRoute(rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    route_t ws;
    route_t* route = NULL;
    char* dst = NULL;
    char* gateway = NULL;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    when_failed_trace(msg2route(&ws, msg), exit, ERROR, "Failed to convert msg to route");

    dst = addr2str_dyn(ws.family, ws.dst);
    gateway = addr2str_dyn(ws.family, ws.gateway);
    SAH_TRACEZ_INFO(ME, "Deleting this route dst %s and gateway %s", dst, gateway);

    route = route_find(&ws);

    when_null_trace(route, exit, ERROR, "No matching route found to delete");
    if(default_route_event(route, false) != 0) {
        SAH_TRACEZ_INFO(ME, "Could not send default route event");
    }
    rv = amxp_timer_start(route->mercytimer, 100);

exit:
    free(dst);
    free(gateway);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void printRouteMsg(FILE* f, rtnlmsg_t* msg) {
    fprintf(f, "%s{",
            rtnlmsg_type(msg) == RTM_NEWROUTE ? "RTM_NEWROUTE" :
            rtnlmsg_type(msg) == RTM_DELROUTE ? "RTM_DELROUTE" :
            rtnlmsg_type(msg) == RTM_GETROUTE ? "RTM_GETROUTE" : "<UNKNOWN>");
    struct rtmsg* rt = (struct rtmsg*) rtnlmsg_hdr(msg);
    fprintf(f, " family=%u(%s)", rt->rtm_family, family2str(rt->rtm_family));
    fprintf(f, " dst_len=%u src_len=%u tos=%u", rt->rtm_dst_len, rt->rtm_src_len, rt->rtm_tos);
    fprintf(f, " table=%u(%s)", rt->rtm_table, convert_bin2str(table_converter, rt->rtm_table));
    fprintf(f, " protocol=%u(%s)", rt->rtm_protocol, convert_bin2str(protocol_converter, rt->rtm_protocol));
    fprintf(f, " scope=%u(%s)", rt->rtm_scope, convert_bin2str(scope_converter, rt->rtm_scope));
    fprintf(f, " type=%u(%s)", rt->rtm_type, convert_bin2str(type_converter, rt->rtm_type));
    fprintf(f, " flags=0x%x", rt->rtm_flags);
    struct rtattr* rta;
    unsigned int rtalen = rtnlmsg_attrLen(msg);
    for(rta = rtnlmsg_attrFirst(msg); RTA_OK(rta, rtalen); rta = RTA_NEXT(rta, rtalen)) {
        switch(rta->rta_type) {
        case RTA_DST:      fprintf(f, " dst=%s", addr2str(rt->rtm_family, (unsigned char*) RTA_DATA(rta), false)); break;
        case RTA_SRC:      fprintf(f, " src=%s", addr2str(rt->rtm_family, (unsigned char*) RTA_DATA(rta), false)); break;
        case RTA_IIF:      fprintf(f, " iif=%d", *(int*) RTA_DATA(rta)); break;
        case RTA_OIF:      fprintf(f, " oif=%d", *(int*) RTA_DATA(rta)); break;
        case RTA_GATEWAY:  fprintf(f, " gateway=%s", addr2str(rt->rtm_family, (unsigned char*) RTA_DATA(rta), false)); break;
        case RTA_PRIORITY: fprintf(f, " priority=%d", *(int*) RTA_DATA(rta)); break;
        case RTA_PREFSRC:  fprintf(f, " prefsrc=%s", addr2str(rt->rtm_family, (unsigned char*) RTA_DATA(rta), false)); break;
        case RTA_TABLE: {
            unsigned int table = *(unsigned int*) RTA_DATA(rta);
            fprintf(f, " table=%u(%s)", table, convert_bin2str(table_converter, table));
            break;
        }
        case RTA_METRICS: {
            fprintf(f, " metrics{");
            struct rtmetric* metric, * metric0 = (struct rtmetric*) RTA_DATA(rta);
            int nmetrics = RTA_PAYLOAD(rta) / NLMSG_ALIGN(sizeof(struct rtmetric));
            for(metric = metric0; metric - metric0 < nmetrics; metric++) {
                switch(metric->hdr.rta_type) {
                case RTAX_LOCK:       fprintf(f, " lock=0x%x", metric->data); break;
                case RTAX_MTU:        fprintf(f, " mtu=%u", metric->data); break;
                case RTAX_WINDOW:     fprintf(f, " window=%u", metric->data); break;
                case RTAX_RTT:        fprintf(f, " rtt=%u", metric->data); break;
                case RTAX_RTTVAR:     fprintf(f, " rttvar=%u", metric->data); break;
                case RTAX_SSTHRESH:   fprintf(f, " ssthresh=%u", metric->data); break;
                case RTAX_CWND:       fprintf(f, " cwnd=%u", metric->data); break;
                case RTAX_ADVMSS:     fprintf(f, " advmss=%u", metric->data); break;
                case RTAX_REORDERING: fprintf(f, " reordering=%u", metric->data); break;
                case RTAX_HOPLIMIT:   fprintf(f, " hoplimit=%u", metric->data); break;
                case RTAX_INITCWND:   fprintf(f, " initcwnd=%u", metric->data); break;
                case RTAX_FEATURES:   fprintf(f, " features=0x%x", metric->data); break;
                case RTAX_RTO_MIN:    fprintf(f, " rtomin=%u", metric->data); break;
                default:
                    fprintf(f, "metric[%u]=%u", metric->hdr.rta_type, metric->data);
                    break;
                }
            }
            fprintf(f, " }");
            break;
        }
        case RTA_CACHEINFO: {
            struct rta_cacheinfo* ci = (struct rta_cacheinfo*) RTA_DATA(rta);
            fprintf(f, " cacheinfo{ clntref=%lu lastuse=%lu expires=%lu error=%lu used=%lu id=%lu ts=%lu tsage=%lu }"
                    , (unsigned long) ci->rta_clntref, (unsigned long) ci->rta_lastuse
                    , (unsigned long) ci->rta_expires, (unsigned long) ci->rta_error
                    , (unsigned long) ci->rta_used, (unsigned long) ci->rta_id
                    , (unsigned long) ci->rta_ts, (unsigned long) ci->rta_tsage
                    );
            break;
        }
        default:
            fprintf(f, " attr[%u](%lu byte%s)", rta->rta_type, (long unsigned int) RTA_PAYLOAD(rta), RTA_PAYLOAD(rta) == 1 ? "" : "s");
            break;
        }
    }
    fprintf(f, " }");
}

amxd_status_t _route_destroy(amxd_object_t* object,
                             UNUSED amxd_param_t* param,
                             amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             UNUSED amxc_var_t* const retval,
                             UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);

    amxd_status_t status = amxd_status_ok;
    route_t* route = NULL;

    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    if((object == NULL) || (amxd_object_get_type(object) == amxd_object_template)) {
        goto exit;
    }

    route = (route_t*) object->priv;
    when_null(route, exit);
    route_destroy(route, false);

exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static rtnlhandler_t newRouteHandler = { RTM_NEWROUTE, sizeof(struct rtmsg), handleNewRoute, printRouteMsg };
static rtnlhandler_t delRouteHandler = { RTM_DELROUTE, sizeof(struct rtmsg), handleDelRoute, printRouteMsg };
static rtnlhandler_t getRouteHandler = { RTM_GETROUTE, sizeof(struct rtmsg), NULL, printRouteMsg };

converter_t* init_route_table_converter(void) {
    converter_t* converter = converter_create("route_table", converter_attribute_fallback);
    converter_loadEntry(converter, RT_TABLE_UNSPEC, "unspec");
    converter_loadEntry(converter, RT_TABLE_DEFAULT, "default");
    converter_loadEntry(converter, RT_TABLE_MAIN, "main");
    converter_loadEntry(converter, RT_TABLE_LOCAL, "local");
    converter_loadTable(converter, amxd_dm_findf(netdev_get_dm(), "NetDev.ConversionTable.Table"));
    return converter;
}

converter_t* init_route_protocol_converter(void) {
    converter_t* converter = converter_create("route_protocol", converter_attribute_fallback);
    converter_loadEntry(converter, RTPROT_UNSPEC, "unspec");
    converter_loadEntry(converter, RTPROT_REDIRECT, "redirect");
    converter_loadEntry(converter, RTPROT_KERNEL, "kernel");
    converter_loadEntry(converter, RTPROT_BOOT, "boot");
    converter_loadEntry(converter, RTPROT_STATIC, "static");
    converter_loadTable(converter, amxd_dm_findf(netdev_get_dm(), "NetDev.ConversionTable.Protocol"));
    return converter;
}

converter_t* init_route_scope_converter(void) {
    converter_t* converter = converter_create("route_scope", converter_attribute_fallback);
    converter_loadEntry(converter, RT_SCOPE_UNIVERSE, "global");
    converter_loadEntry(converter, RT_SCOPE_SITE, "site");
    converter_loadEntry(converter, RT_SCOPE_HOST, "host");
    converter_loadEntry(converter, RT_SCOPE_LINK, "link");
    converter_loadEntry(converter, RT_SCOPE_NOWHERE, "nowhere");
    converter_loadTable(converter, amxd_dm_findf(netdev_get_dm(), "NetDev.ConversionTable.Scope"));
    return converter;
}

converter_t* init_route_type_converter(void) {
    converter_t* converter = converter_create("route_type", converter_attribute_none);
    converter_loadEntry(converter, RTN_UNSPEC, "unspec");
    converter_loadEntry(converter, RTN_UNICAST, "unicast");
    converter_loadEntry(converter, RTN_LOCAL, "local");
    converter_loadEntry(converter, RTN_BROADCAST, "broadcast");
    converter_loadEntry(converter, RTN_ANYCAST, "anycast");
    converter_loadEntry(converter, RTN_MULTICAST, "multicast");
    converter_loadEntry(converter, RTN_BLACKHOLE, "blackhole");
    converter_loadEntry(converter, RTN_UNREACHABLE, "unreachable");
    converter_loadEntry(converter, RTN_PROHIBIT, "prohibit");
    converter_loadEntry(converter, RTN_THROW, "throw");
    converter_loadEntry(converter, RTN_NAT, "nat");
    return converter;
}

void route_init_converters(void) {
    table_converter = init_route_table_converter();
    protocol_converter = init_route_protocol_converter();
    scope_converter = init_route_scope_converter();
    type_converter = init_route_type_converter();
}

bool route_init(void) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_ok;
    amxd_param_t* parameter = NULL;

    route_init_converters();

    const char* path[] = {"NetDev.Link.IPv4Route", "NetDev.Link.IPv6Route"};
    int i;
    for(i = 0; i < 2; i++) {
        amxd_object_t* netdev_addr = amxd_dm_findf(netdev_get_dm(), path[i], NULL);
        parameter = amxd_object_get_param_def(netdev_addr, "Table");
        rv |= amxd_param_add_action_cb(parameter, action_param_validate, param_validate_converter, table_converter);
        parameter = amxd_object_get_param_def(netdev_addr, "Protocol");
        rv |= amxd_param_add_action_cb(parameter, action_param_validate, param_validate_converter, protocol_converter);
        parameter = amxd_object_get_param_def(netdev_addr, "Scope");
        rv |= amxd_param_add_action_cb(parameter, action_param_validate, param_validate_converter, scope_converter);
        parameter = amxd_object_get_param_def(netdev_addr, "Type");
        rv |= amxd_param_add_action_cb(parameter, action_param_validate, param_validate_converter, type_converter);
    }

    rtnl_register_handler(&newRouteHandler);
    rtnl_register_handler(&delRouteHandler);
    rtnl_register_handler(&getRouteHandler);

    SAH_TRACEZ_OUT(ME);
    return (rv == amxd_status_ok);
}

bool route_start(void) {
    SAH_TRACEZ_IN(ME);

    rtnlmsg_t* msg = rtnlmsg_instance();
    rtnlmsg_initialize(msg, RTM_GETROUTE, sizeof(struct rtmsg), NLM_F_REQUEST | NLM_F_DUMP);
    if(!rtnl_send(netdev_get_nlfd(), msg)) {
        return false;
    }
    if(!rtnl_read(netdev_get_nlfd(), msg, true)) {
        return false;
    }

    SAH_TRACEZ_OUT(ME);
    return true;
}

void route_clear(void) {
    while(!amxc_llist_is_empty(&routes)) {
        amxc_llist_it_t* it = amxc_llist_get_first(&routes);
        route_t* route = amxc_container_of(it, route_t, it);
        route_destroy(route, false);
        amxc_llist_it_take(it);
    }
}

void route_cleanup(void) {
    route_clear();

    converter_destroy(&table_converter);
    converter_destroy(&protocol_converter);
    converter_destroy(&scope_converter);
    converter_destroy(&type_converter);
}

void route_deleteIPv4Routes(int link) {
    SAH_TRACEZ_IN(ME);
    route_t* route = NULL;
    amxc_llist_for_each(it, &routes) {
        route = amxc_container_of(it, route_t, it);
        if((route->family == AF_INET) && (route->oif == link)) {
            route_destroy(route, true);
        }
    }
    SAH_TRACEZ_OUT(ME);
}

bool set_if_default_route(route_t* route) {
    SAH_TRACEZ_IN(ME);
    bool ipv4 = strcmp("IPv4", family2str(route->family)) == 0;
    char* dst = addr2str_dyn(route->family, route->dst);
    char* gateway = NULL;
    bool def_dst = strcmp(ipv4 ? "0.0.0.0" : "::", dst) == 0;
    bool def_dst_len = route->dstlen == 0;

    when_false(def_dst && def_dst_len, exit);
    route->default_route = true;

    route->def_neigh = neigh_find_w_defroute(route, ipv4);
    gateway = addr2str_dyn(route->family, route->gateway);
    SAH_TRACEZ_INFO(ME, "Setting as default dst %s and default gateway %s", dst, gateway);
exit:
    free(dst);
    free(gateway);
    SAH_TRACEZ_OUT(ME);
    return route->default_route;
}

amxd_status_t route_convert_to_default_route_var(route_t* defroute, bool active, amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_value;
    int index = -1;
    char ifname[ADDRSPACE];
    char phys_ifname[ADDRSPACE];
    char* gateway_addr = NULL;
    const char* lla_addr = NULL;
    bool is_valid = false;
    neigh_t* neigh = NULL;

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    when_null_status(defroute, exit, rv = amxd_status_ok);

    when_false(defroute->default_route, exit);

    neigh = defroute->def_neigh;

    // fill default route data
    amxc_var_add_key(bool, data, "Active", active);

    amxc_var_add_key(cstring_t, data, "InterfaceName", (if_indextoname(defroute->oif, ifname) == NULL) ? "" : ifname);
    amxc_var_add_key(uint32_t, data, "InterfaceIndex", defroute->oif);

    gateway_addr = addr2str_dyn(defroute->family, defroute->gateway);
    amxc_var_add_key(cstring_t, data, "DefaultGateway", gateway_addr);

    lla_addr = (neigh != NULL) ? lladdr2str(&neigh->lladdr) : "";
    amxc_var_add_key(cstring_t, data, "MACAddress", lla_addr);

    index = bridgetable_mac_to_netdev_index(lla_addr);
    is_valid = if_indextoname(index, phys_ifname) != NULL;
    amxc_var_add_key(cstring_t, data, "PhysBridgeInterface", is_valid ? phys_ifname : "");

    SAH_TRACEZ_INFO(ME, "Sending event/method with ifname:%s index:%d Default Gateway address:%s Mac address:%s Physical bridge interface:%s",
                    ifname,
                    defroute->oif,
                    gateway_addr,
                    (neigh != NULL) ? lla_addr : "No MAC address",
                    is_valid ? phys_ifname : "");

    rv = amxd_status_ok;
exit:
    free(gateway_addr);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t default_route_event(route_t* defroute, bool active) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rv = amxd_status_invalid_value;
    amxc_var_t data;
    amxd_object_t* obj = amxd_dm_findf(netdev_get_dm(), "NetDev.");
    amxc_var_init(&data);

    when_null(obj, exit);
    when_null_trace(defroute, exit, ERROR, "Default route is NULL");

    rv = route_convert_to_default_route_var(defroute, active, &data);
    when_failed(rv, exit);

    //Send the event
    amxd_object_emit_signal(obj, "nd:default-route", &data);
    rv = amxd_status_ok;

exit:
    amxc_var_clean(&data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

bool neigh_defroute_match(neigh_t* neigh, route_t* defroute, bool ipv4) {
    SAH_TRACEZ_IN(ME);
    bool match = false;
    char* route_addr = NULL;
    char* neigh_addr = NULL;

    when_null_trace(neigh, exit, ERROR, "Neighbour is NULL");
    when_null_trace(defroute, exit, ERROR, "Default route is NULL");
    neigh_addr = addr2str_dyn(neigh->family, neigh->dst);
    route_addr = addr2str_dyn(defroute->family, defroute->gateway);

    when_null_trace(route_addr, exit, ERROR, "Strdup failed on default route address");
    when_null_trace(neigh_addr, exit, ERROR, "Strdup failed on neighbour address");
    match = defroute->default_route
        && defroute->active
        && neigh->active
        && (neigh->link == defroute->oif)
        && (strcmp(route_addr, neigh_addr) == 0)
        && (ipv4 ? (strcmp(family2str(defroute->family), "IPv4") == 0) : (strcmp(family2str(defroute->family), "IPv6") == 0));

exit:
    free(route_addr);
    free(neigh_addr);
    SAH_TRACEZ_OUT(ME);
    return match;
}

bool bridgetable_defroute_match(bridgetable_t* bridgetable, route_t* defroute, bool ipv4) {
    SAH_TRACEZ_IN(ME);
    bool match = false;
    neigh_t* neigh = NULL;

    when_null_trace(bridgetable, exit, ERROR, "Bridgetable is NULL");
    when_null_trace(defroute, exit, ERROR, "Default route is NULL");

    neigh = defroute->def_neigh;
    match = defroute->default_route
        && defroute->active
        && (strcmp((neigh != NULL) ? lladdr2str(&neigh->lladdr) : "", bridgetable->mac) == 0)
        && (ipv4 ? (strcmp(family2str(defroute->family), "IPv4") == 0) : (strcmp(family2str(defroute->family), "IPv6") == 0));

exit:
    SAH_TRACEZ_OUT(ME);
    return match;
}

route_t* bridgetable_bind_to_defroute(bridgetable_t* bridgetable, bool ipv4) {
    SAH_TRACEZ_IN(ME);
    route_t* route = NULL;
    when_null_trace(bridgetable, exit, ERROR, "No bridgetable provided to find route");
    amxc_llist_iterate(it, &routes) {
        route = amxc_container_of(it, route_t, it);
        if(bridgetable_defroute_match(bridgetable, route, ipv4)) {
            route->def_bridgetable = bridgetable;
            break;
        }
        route = NULL;
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return route;
}

route_t* neigh_bind_to_defroute(neigh_t* neigh, bool ipv4) {
    SAH_TRACEZ_IN(ME);
    route_t* route = NULL;
    when_null_trace(neigh, exit, ERROR, "No neigh provided to find route");
    amxc_llist_iterate(it, &routes) {
        route = amxc_container_of(it, route_t, it);
        if(neigh_defroute_match(neigh, route, ipv4)) {
            route->def_neigh = neigh;
            break;
        }
        route = NULL;
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return route;
}

route_t* route_lookup_ipv4_default_route(void) {
    SAH_TRACEZ_IN(ME);
    route_t* route = NULL;

    amxc_llist_iterate(it, &routes) {
        route = amxc_container_of(it, route_t, it);
        if(route->default_route && (route->family == AF_INET)) {
            break;
        }
        route = NULL;
    }

    SAH_TRACEZ_OUT(ME);
    return route;
}
