/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include <errno.h>
#include <net/if.h>

#include <amxc/amxc.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "netdev_util.h"
#include "netdev_netlink.h"
#include "dm_netdev.h"
#include "netdev_rpc_utils.h"

#define ME "rpc"

bool nl_ip_address(uint8_t action_type,
                   unsigned char family,
                   const char* ip_addr,
                   const char* ifname,
                   uint8_t prefixlen) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    unsigned short flags = 0;
    unsigned char addr_buf[sizeof(struct in6_addr)];
    rtnlmsg_t* msg = rtnlmsg_instance();

    memset(&addr_buf, 0, sizeof(addr_buf));

    when_str_empty_trace(ip_addr, exit, ERROR, "IP address should not be empty");
    when_str_empty_trace(ifname, exit, ERROR, "Ifname should not be empty");

    switch(action_type) {
    case RTM_NEWADDR:
        flags = NLM_F_CREATE | NLM_F_EXCL | NLM_F_REQUEST; // Old netdev used NLM_F_REQUEST | NLM_F_CREATE | NLM_F_REPLACE
        break;
    case RTM_DELADDR:
        flags = NLM_F_REQUEST;
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Unkown action type, failed to update IP address");
        goto exit;
        break;
    }

    rtnlmsg_initialize(msg, action_type, sizeof(struct ifaddrmsg), flags);

    struct ifaddrmsg* ifaddr = (struct ifaddrmsg*) rtnlmsg_hdr(msg);
    ifaddr->ifa_family = family;
    ifaddr->ifa_prefixlen = prefixlen;
    ifaddr->ifa_index = if_nametoindex(ifname);
    ifaddr->ifa_scope = RT_SCOPE_UNIVERSE;

    when_false_trace(ifaddr->ifa_index > 0, exit, ERROR, "Could not find interface index for '%s' %d(%s)", ifname, errno, strerror(errno));

    // Convert IPAddress to binary and copy it into the message
    inet_pton(family, ip_addr, addr_buf);
    rtnlmsg_addAttr(msg, IFA_LOCAL, addrlen(family), addr_buf);

    rv = rtnl_send(netdev_get_nlfd(), msg);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}
