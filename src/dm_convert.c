/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "netdev.h"

#include "dm_convert.h"

#define ME "netdev"

typedef struct {
    amxc_llist_it_t it;
    unsigned long bin;
    char* str;
} converter_entry_t;

static amxc_llist_t converters = {NULL, NULL};
static char flagstrbuf[256];

static converter_entry_t* converter_entry_create(converter_t* converter, unsigned long bin, const char* str) {
    converter_entry_t* entry = (converter_entry_t*) calloc(1, sizeof(converter_entry_t));
    entry->bin = bin;
    entry->str = str ? strdup(str) : NULL;
    amxc_llist_append(&converter->list, &entry->it);
    return entry;
}

static void converter_entry_destroy(amxc_llist_it_t* it) {
    converter_entry_t* entry = amxc_container_of(it, converter_entry_t, it);
    free(entry->str);
    free(entry);
}

static const char* _bin2str(converter_t* converter, unsigned long bin) {
    converter_entry_t* entry = NULL;

    amxc_llist_iterate(it, &converter->list) {
        entry = amxc_container_of(it, converter_entry_t, it);
        if(entry->bin == bin) {
            break;
        }
        entry = NULL;
    }

    if(entry) {
        return entry->str;
    } else if(converter->attributes & converter_attribute_fallback) {
        if(converter->default_value != NULL) {
            return converter->default_value;
        } else {
            static char fallbackbuf[20];
            sprintf(fallbackbuf, "%lu", bin);
            return fallbackbuf;
        }
    } else {
        if(!(converter->attributes & converter_attribute_flags)) {
            SAH_TRACEZ_WARNING(ME, "Converter \"%s\" caught invalid binary value 0x%lx", converter->name, bin);
        }
        return "";
    }
}

static unsigned long _str2bin(converter_t* converter, const char* str, bool* success) {
    converter_entry_t* entry = NULL;

    amxc_llist_iterate(it, &converter->list) {
        entry = amxc_container_of(it, converter_entry_t, it);
        if(!strcmp(entry->str ? entry->str : "", str ? str : "")) {
            break;
        }
        entry = NULL;
    }

    if(entry) {
        if(success) {
            *success = true;
        }
        return entry->bin;
    } else if(converter->attributes & converter_attribute_fallback) {
        char* end = NULL;
        unsigned long ret = str ? strtol(str, &end, 0) : 0;
        if(success) {
            *success = !end || !*end;
        }

        if(end && *end) {
            SAH_TRACEZ_WARNING(ME, "Converter \"%s\" caught invalid string value %s", converter->name, str ? str : "(null)");
        }

        return ret;
    } else {
        if(success) {
            *success = false;
        }
        SAH_TRACEZ_WARNING(ME, "Converter \"%s\" caught invalid string value %s", converter->name, str ? str : "(null)");
        return 0;
    }
}

void converter_loadTable(converter_t* converter, amxd_object_t* table) {
    amxd_object_for_each(instance, it, table) {
        amxd_object_t* object = amxc_container_of(it, amxd_object_t, it);
        unsigned long index = amxd_object_get_index(object);
        const char* name = amxd_object_get_name(object, AMXD_OBJECT_NAMED);
        converter_loadEntry(converter, index, name);
    }
    converter->table = table;
}

converter_t* converter_create(const char* name, converter_attributes_t attributes) {
    converter_t* converter = (converter_t*) calloc(1, sizeof(converter_t));
    converter->name = name;
    converter->attributes = attributes;
    converter->default_value = NULL;
    amxc_llist_append(&converters, &converter->it);
    return converter;
}

void converter_destroy(converter_t** converter) {
    when_null(converter, exit);
    when_null(*converter, exit);

    amxc_llist_clean(&((*converter)->list), converter_entry_destroy);
    amxc_llist_it_take(&(*converter)->it);
    free(*converter);
    *converter = NULL;

exit:
    return;
}

void converter_loadDefault(converter_t* converter, const char* str) {
    when_null_trace(converter, exit, ERROR, "No converter provided to set default");
    converter->default_value = str;

exit:
    return;
}

void converter_loadEntry(converter_t* converter, unsigned long bin, const char* str) {
    converter_entry_create(converter, bin, str);
}

const char* convert_bin2str(converter_t* converter, unsigned long bin) {
    char* flagstrptr = flagstrbuf;
    const char* str = NULL;
    int i = 0;
    int n = 0;

    *flagstrptr = '\0'; // Keep this before the first possible exit point so no previous result is returned
    when_null_trace(converter, exit, ERROR, "No converter provided");

    if(!(converter->attributes & converter_attribute_flags)) {
        return _bin2str(converter, bin);
    }

    for(i = 0; i < 32; i++) {
        if(bin & 1 << i) {
            str = _bin2str(converter, 1 << i);
            if(!*str) {
                continue;
            }
            flagstrptr += snprintf(flagstrptr, sizeof(flagstrbuf) - (flagstrptr - flagstrbuf), "%s%s", n++ ? " " : "", str);
            if(flagstrbuf + sizeof(flagstrbuf) < flagstrptr) {
                break;
            }
        }
    }

exit:
    return flagstrbuf;
}

unsigned long convert_str2bin(converter_t* converter, const char* str, bool* success) {
    char* flagstrptr = flagstrbuf;
    char* flag = flagstrbuf;
    char flagstrtmp = 0;
    unsigned long bin = 0;

    when_null_trace(converter, exit, ERROR, "No converter provided");
    when_null_trace(str, exit, ERROR, "No string provided to convert");

    if(!(converter->attributes & converter_attribute_flags)) {
        return _str2bin(converter, str, success);
    }

    snprintf(flagstrbuf, sizeof(flagstrbuf), "%s", str ? str : "");
    if(success) {
        *success = true;
    }
    do {
        if(!((*flagstrptr == '\0') || (*flagstrptr == ' ') || (*flagstrptr == '\t'))) {
            continue;
        }
        if(flagstrptr > flag) {
            flagstrtmp = *flagstrptr;
            *flagstrptr = '\0';
            bin |= _str2bin(converter, flag, success);
            if(success && !*success) {
                success = NULL;
            }
            *flagstrptr = flagstrtmp;
        }
        flag = flagstrptr + 1;
    } while(*flagstrptr++);

exit:
    return bin;
}

amxd_status_t _table_destroy(amxd_object_t* object,
                             UNUSED amxd_param_t* param,
                             amxd_action_t reason,
                             UNUSED const amxc_var_t* const args,
                             UNUSED amxc_var_t* const retval,
                             UNUSED void* priv) {
    amxd_status_t status = amxd_status_ok;
    unsigned long bin = 0;
    amxd_object_t* table = NULL;
    if(reason != action_object_destroy) {
        status = amxd_status_function_not_implemented;
        goto exit;
    }

    if((object == NULL) || (amxd_object_get_type(object) == amxd_object_template)) {
        goto exit;
    }

    table = amxd_object_get_parent(object);
    bin = amxd_object_get_index(object);
    amxc_llist_iterate(it, &converters) {
        converter_t* converter = amxc_container_of(it, converter_t, it);
        if(converter->table == table) {
            amxc_llist_for_each(eit, &converter->list) {
                converter_entry_t* entry = amxc_container_of(eit, converter_entry_t, it);
                if(entry->bin == bin) {
                    converter_entry_destroy(&entry->it);
                }
            }
        }
    }

exit:
    return status;
}

void _convert_entry_added(UNUSED const char* const sig_name,
                          const amxc_var_t* const data,
                          UNUSED void* const priv) {
    uint32_t index = amxc_var_dyncast(uint32_t, GET_ARG(data, "index"));
    const char* name = GET_CHAR(data, "name");
    amxd_object_t* table = amxd_dm_signal_get_object(netdev_get_dm(), data);

    amxc_llist_iterate(it, &converters) {
        converter_t* converter = amxc_container_of(it, converter_t, it);
        if(converter->table == table) {
            converter_entry_create(converter, index, name);
        }
    }
}
