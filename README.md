# netdev

## Summary

NetDev monitors the operating system's network devices.

## Description

The NetDev (Network Device) monitors the operating system's
network devices.
The monitoring is performed using Linux Netlink, for kernel to
userspace communication.

NetDev maintains its own data model with all monitored interfaces
and their parameters exported. It has active Netlink queries for
any changes performed by the kernel and the result is pushed to
its own data model.

Updates to its own data model are pushed to the kernel, using the
same Netlink interface.
