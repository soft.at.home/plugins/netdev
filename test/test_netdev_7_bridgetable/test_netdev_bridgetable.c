/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>

#include "netdev.h"
#include "dm_netdev.h"
#include "dm_convert.h"
#include "dm_link.h"
#include "dm_link_addr.h"
#include "dm_link_neigh.h"
#include "dm_link_route.h"
#include "dm_link_stats.h"
#include "netdev_bridgetable.h"
#include "test_netdev_bridgetable.h"
#include "../common/test_netdev_common_setup.h"
#include "../common/test_util.h"
#include "../common/common_functions.h"

static void s_add_mib(const char* path) {
    amxd_object_t* obj = amxd_object_findf(amxd_dm_get_root(test_setup_dm()), path);
    assert_non_null(obj);
    amxd_object_add_mib(obj, "bridge_port");
}

static bool s_path_exists(const char* path) {
    return amxd_dm_findf(test_setup_dm(), "%s", path) != NULL;
}

int test_netdev_bridgetable_setup(UNUSED void** state) {
    test_netdev_common_setup(state, true);

    // to initialize `netdev_get_dm()`
    _netdev_main(AMXO_START, test_setup_dm(), test_setup_parser());
    handle_events();

    assert_int_equal(0, amxo_parser_scan_mib_dir(test_setup_parser(), "../../mibs"));
    assert_int_equal(0, amxo_parser_load_mib(test_setup_parser(), test_setup_dm(), "bridge_port"));
    s_add_mib("NetDev.Link.eth101");
    s_add_mib("NetDev.Link.eth102");
    s_add_mib("NetDev.Link.eth103");

    return 0;
}

int test_netdev_bridgetable_teardown(UNUSED void** state) {
    _netdev_main(AMXO_STOP, test_setup_dm(), test_setup_parser());
    handle_events();
    test_netdev_common_teardown(state);

    return 0;
}

void test_handle_parseresult__new_entry(UNUSED void** state) {
    // GIVEN normal message and empty bridgetable
    netdev_parseresult_bridgetable_t parseresult = {.disappeared = false, .mac = "11:22:33:44:55:66", .netdev_index = 101 };
    handle_events();

    // WHEN handling that message
    netdev_bridgetable_handle_parseresult(&parseresult);
    handle_events();

    // THEN the bridgetable contains this entry
    assert_dm_str("NetDev.Link.101.BridgeTable.mac-11_22_33_44_55_66.", "MACAddress", "11:22:33:44:55:66");
}

void test_handle_parseresult__existing_entry(UNUSED void** state) {
    // GIVEN a bridgetable entry
    netdev_parseresult_bridgetable_t parseresult = {.disappeared = false, .mac = "11:22:33:44:55:66", .netdev_index = 101 };
    netdev_bridgetable_handle_parseresult(&parseresult);
    handle_events();

    // WHEN handling a message (remember: the bridgetable entry already exists)
    netdev_bridgetable_handle_parseresult(&parseresult);

    // THEN the bridgetable still contains this entry
    assert_dm_str("NetDev.Link.101.BridgeTable.mac-11_22_33_44_55_66.", "MACAddress", "11:22:33:44:55:66");
}

void test_handle_parseresult__move(UNUSED void** state) {
    // GIVEN a bridgetable entry, describing a MAC is at a certain port
    netdev_parseresult_bridgetable_t parseresult1 = {.disappeared = false, .mac = "11:22:33:44:55:66", .netdev_index = 101 };
    netdev_bridgetable_handle_parseresult(&parseresult1);
    handle_events();
    assert_non_null(amxd_dm_findf(test_setup_dm(), "NetDev.Link.101.BridgeTable.mac-11_22_33_44_55_66."));

    // WHEN the MAC appears at another port
    netdev_parseresult_bridgetable_t parseresult2 = {.disappeared = false, .mac = "11:22:33:44:55:66", .netdev_index = 102 };
    netdev_bridgetable_handle_parseresult(&parseresult2);
    handle_events();

    // THEN the bridgetable entry that says the MAC is at the old port, is removed
    assert_null(amxd_dm_findf(test_setup_dm(), "NetDev.Link.101.BridgeTable.mac-11_22_33_44_55_66."));
    // AND THEN an entry that says that the MAC is at the new port, exists
    assert_non_null(amxd_dm_findf(test_setup_dm(), "NetDev.Link.102.BridgeTable.mac-11_22_33_44_55_66."));
    assert_dm_str("NetDev.Link.102.BridgeTable.mac-11_22_33_44_55_66.", "MACAddress", "11:22:33:44:55:66");
}

void test_handle_parseresult__remove_entry(UNUSED void** state) {
    // GIVEN a bridgetable entry
    netdev_parseresult_bridgetable_t parseresult_appear =
    {.disappeared = false, .mac = "11:22:33:44:55:67", .netdev_index = 101 };
    netdev_bridgetable_handle_parseresult(&parseresult_appear);
    handle_events();
    assert_true(s_path_exists("NetDev.Link.101.BridgeTable.mac-11_22_33_44_55_67"));

    // WHEN handling a message that the entry disappears
    netdev_parseresult_bridgetable_t parseresult_disappear =
    {.disappeared = true, .mac = "11:22:33:44:55:67", .netdev_index = 101 };
    netdev_bridgetable_handle_parseresult(&parseresult_disappear);

    // THEN the bridgetable does not contain this entry anymore
    assert_false(s_path_exists("NetDev.Link.101.BridgeTable.mac-11_22_33_44_55_67."));
}

void test_handle_parseresult__remove_nonexisting_entry(UNUSED void** state) {
    // GIVEN an empty bridgetable entry
    assert_false(s_path_exists("NetDev.Link.101.BridgeTable.mac-11_22_33_44_55_67."));

    // WHEN handling a message that an entry disappears
    printf("Expected to produce an ERROR trace\n");
    fflush(stdout);
    netdev_parseresult_bridgetable_t parseresult_disappear =
    {.disappeared = true, .mac = "11:22:33:44:55:67", .netdev_index = 101 };
    netdev_bridgetable_handle_parseresult(&parseresult_disappear);

    // THEN nothing happens (and the entry is still not there)
    assert_false(s_path_exists("NetDev.Link.101.BridgeTable.mac-11_22_33_44_55_67."));
}

void test_handle_parseresult__remove_entry_nonexisting_netdevindex(UNUSED void** state) {
    // GIVEN a netdevindex for which there is no interface
    uint32_t nonexisting_netdevidx = 404;
    assert_true(s_path_exists("NetDev.Link.101"));
    assert_false(s_path_exists("NetDev.Link.404"));

    // WHEN handling a message that an entry disappears on that interface
    printf("Expected to produce an ERROR trace\n");
    fflush(stdout);
    netdev_parseresult_bridgetable_t parseresult_disappear =
    {.disappeared = true, .mac = "11:22:33:44:55:67", .netdev_index = nonexisting_netdevidx };
    netdev_bridgetable_handle_parseresult(&parseresult_disappear);

    // THEN nothing happens (and the interface is still not there)
    assert_false(s_path_exists("NetDev.Link.404"));
}