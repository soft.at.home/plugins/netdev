/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>

#include <arpa/inet.h>
#include <linux/rtnetlink.h>
#include <linux/if.h>
#include <linux/if_arp.h>

#include "netdev_netlink.h"
#include "netdev_rpc.h"
#include "netdev_bridgetable.h"
#include "dm_link.h"
#include "dm_link_addr.h"
#include "dm_netdev.h"
#include "dm_convert.h"
#include "../common/test_netdev_common_setup.h"
#include "../common/common_functions.h"
#include "dm_link_route.h"
#include "test_dm_link_route.h"

static neigh_t mock_default_neighbour = {
    .active = true,
    .index = 2,
    .link = 5,
};

static void test_netdev_setup_default_neignbour(void) {
    const char* mock_mac = "00:01:02:03:04:05";
    bridgetable_t* bridge = bridgetable_create();
    bridge->mac = strdup(mock_mac);
    bridge->netdev_index = 2;
    mock_default_neighbour.lladdr = *str2lladdr(mock_mac, NULL);
}

static link_t* add_test_link(void) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    link_t* link = NULL;
    amxd_object_t* link_obj = NULL;
    const int index = 48;

    // Msg coming from "ip link add dummy100 type dummy", index = 48
    msg->nlmsg = (struct nlmsghdr*) "\x50\x05\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x30\x00\x00\x00\x82\x00\x00\x00\xff\xff\xff\xff\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x0d\x00\xe8\x03\x00\x00\x05\x00\x10\x00\x02\x00\x00\x00\x05\x00\x11\x00\x00\x00\x00\x00\x08\x00\x04\x00\xdc\x05\x00\x00\x08\x00\x32\x00\x00\x00\x00\x00\x08\x00\x33\x00\x00\x00\x00\x00\x08\x00\x1b\x00\x00\x00\x00\x00\x08\x00\x1e\x00\x00\x00\x00\x00\x08\x00\x1f\x00\x01\x00\x00\x00\x08\x00\x28\x00\xff\xff\x00\x00\x08\x00\x29\x00\x00\x00\x01\x00\x08\x00\x20\x00\x01\x00\x00\x00\x05\x00\x21\x00\x01\x00\x00\x00\x09\x00\x06\x00\x6e\x6f\x6f\x70\x00\x00\x00\x00\x08\x00\x23\x00\x00\x00\x00\x00\x08\x00\x2f\x00\x00\x00\x00\x00\x08\x00\x30\x00\x00\x00\x00\x00\x05\x00\x27\x00\x00\x00\x00\x00\x24\x00\x0e\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0a\x00\x01\x00\x72\x16\x1a\xb4\x68\x0d\x00\x00\x0a\x00\x02\x00\xff\xff\xff\xff\xff\xff\x00\x00\xc4\x00\x17\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x64\x00\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x2b\x00\x05\x00\x02\x00\x00\x00\x00\x00\x10\x00\x12\x00\x0a\x00\x01\x00\x64\x75\x6d\x6d\x79\x00\x00\x00\x0c\x03\x1a\x00\x88\x00\x02\x00\x84\x00\x01\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\x27\x00\x00\xe8\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\x02\x0a\x00\x08\x00\x01\x00\x00\x00\x00\x00\x14\x00\x05\x00\xff\xff\x00\x00\x45\xe3\x19\x00\x54\x54\x00\x00\xe8\x03\x00\x00\xe4\x00\x02\x00\x00\x00\x00\x00\x40\x00\x00\x00\xdc\x05\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\xff\xff\xff\xff\xa0\x0f\x00\x00\xe8\x03\x00\x00\x00\x00\x00\x00\x80\x3a\x09\x00\x80\x51\x01\x00\x03\x00\x00\x00\x58\x02\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x60\xea\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x10\x27\x00\x00\xe8\x03\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\xee\x36\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\xff\xff\x00\x00\xff\xff\xff\xff\x2c\x01\x03\x00\x25\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x34\x00\x06\x00\x06\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x14\x00\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x05\x00\x08\x00\x00\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 16;

    // Make sure the link does not already exists
    link = link_find(index);
    assert_null(link);
    link_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100");
    assert_null(link_obj);

    // Call the handler and handle events so the link exist in the DM as well
    assert_int_equal(handleNewLink(msg), 0);
    handle_events();

    link = link_find(48);
    assert_non_null(link);
    link_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100");
    assert_non_null(link_obj);

    return link;
}

static route_t* add_test_route(void) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    route_t ws;
    route_t* route = NULL;
    amxd_object_t* route_obj = NULL;

    //Message coming from "ip a add 192.168.100.1/24 dev dummy100"
    msg->nlmsg = (struct nlmsghdr*) "\x3c\x00\x00\x00\x18\x00\x00\x06\x00\x00\x00\x00\x00\x00\x00\x00\x02\x18\x00\x00\xfe\x02\xfd\x01\x00\x00\x00\x00\x08\x00\x0f\x00\xfe\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x00\x08\x00\x07\x00\xc0\xa8\x64\x01\x08\x00\x04\x00\x30\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;

    // Make sure the routes does not yet exist
    assert_int_equal(msg2route(&ws, msg), 0);
    route = route_find(&ws);
    assert_null(route);

    // Call handler to add the routes
    assert_int_equal(handleNewRoute(msg), 0);
    handle_events();

    route = route_find(&ws);
    assert_non_null(route);
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", route->dyn_index);
    assert_non_null(route_obj);

    return route;
}

int test_netdev_setup(void** state) {
    test_netdev_setup_default_neignbour();

    test_netdev_common_setup(state, false);
    assert_int_equal(0, amxo_parser_scan_mib_dir(test_setup_parser(), "../../mibs"));
    _netdev_main(AMXO_START, test_setup_dm(), test_setup_parser());
    handle_events();

    return 0;
}

int test_netdev_teardown(void** state) {
    _netdev_main(AMXO_STOP, test_setup_dm(), test_setup_parser());
    handle_events();
    test_netdev_common_teardown(state);
    return 0;
}

void test_init_route_table_converter(UNUSED void** state) {
    converter_t* test_converter = init_route_table_converter();
    assert_non_null(test_converter);

    assert_string_equal(convert_bin2str(test_converter, RT_TABLE_UNSPEC), "unspec");
    assert_string_equal(convert_bin2str(test_converter, RT_TABLE_DEFAULT), "default");
    assert_string_equal(convert_bin2str(test_converter, RT_TABLE_MAIN), "main");
    assert_string_equal(convert_bin2str(test_converter, RT_TABLE_LOCAL), "local");

    // Any other bin value should return the bin value as a string (since no default is set)
    assert_string_equal(convert_bin2str(test_converter, 99), "99");
    assert_string_equal(convert_bin2str(test_converter, 35), "35");

    converter_destroy(&test_converter);

    // Make sure that an empty string is returned in case the converter is destroyed
    assert_string_equal(convert_bin2str(test_converter, RT_TABLE_MAIN), "");
}

void test_init_route_protocol_converter(UNUSED void** state) {
    converter_t* test_converter = init_route_protocol_converter();
    assert_non_null(test_converter);

    assert_string_equal(convert_bin2str(test_converter, RTPROT_UNSPEC), "unspec");
    assert_string_equal(convert_bin2str(test_converter, RTPROT_REDIRECT), "redirect");
    assert_string_equal(convert_bin2str(test_converter, RTPROT_KERNEL), "kernel");
    assert_string_equal(convert_bin2str(test_converter, RTPROT_BOOT), "boot");
    assert_string_equal(convert_bin2str(test_converter, RTPROT_STATIC), "static");

    // Any other bin value should return the bin value as a string (since no default is set)
    assert_string_equal(convert_bin2str(test_converter, 99), "99");
    assert_string_equal(convert_bin2str(test_converter, 35), "35");

    converter_destroy(&test_converter);

    // Make sure that an empty string is returned in case the converter is destroyed
    assert_string_equal(convert_bin2str(test_converter, RTPROT_KERNEL), "");
}

void test_init_route_scope_converter(UNUSED void** state) {
    converter_t* test_converter = init_route_scope_converter();
    assert_non_null(test_converter);

    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_UNIVERSE), "global");
    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_SITE), "site");
    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_HOST), "host");
    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_LINK), "link");
    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_NOWHERE), "nowhere");

    // Any other bin value should return the bin value as a string (since no default is set)
    assert_string_equal(convert_bin2str(test_converter, 99), "99");
    assert_string_equal(convert_bin2str(test_converter, 35), "35");

    converter_destroy(&test_converter);

    // Make sure that an empty string is returned in case the converter is destroyed
    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_HOST), "");
}

void test_init_route_type_converter(UNUSED void** state) {
    converter_t* test_converter = init_route_type_converter();
    assert_non_null(test_converter);

    assert_string_equal(convert_bin2str(test_converter, RTN_UNSPEC), "unspec");
    assert_string_equal(convert_bin2str(test_converter, RTN_UNICAST), "unicast");
    assert_string_equal(convert_bin2str(test_converter, RTN_LOCAL), "local");
    assert_string_equal(convert_bin2str(test_converter, RTN_BROADCAST), "broadcast");
    assert_string_equal(convert_bin2str(test_converter, RTN_ANYCAST), "anycast");
    assert_string_equal(convert_bin2str(test_converter, RTN_MULTICAST), "multicast");
    assert_string_equal(convert_bin2str(test_converter, RTN_BLACKHOLE), "blackhole");
    assert_string_equal(convert_bin2str(test_converter, RTN_UNREACHABLE), "unreachable");
    assert_string_equal(convert_bin2str(test_converter, RTN_PROHIBIT), "prohibit");
    assert_string_equal(convert_bin2str(test_converter, RTN_THROW), "throw");
    assert_string_equal(convert_bin2str(test_converter, RTN_NAT), "nat");

    // Any other bin value should return an empty string because the converter attribute is set to non
    assert_string_equal(convert_bin2str(test_converter, 99), "");
    assert_string_equal(convert_bin2str(test_converter, 35), "");

    converter_destroy(&test_converter);

    // Make sure that an empty string is returned in case the converter is destroyed
    assert_string_equal(convert_bin2str(test_converter, RTN_MULTICAST), "");
}

/*
 * Test the route_copy function
 * Expectations:
 *  - When providing invalid input the function should return -1
 *  - When valid inputs are provided the destination route should be a copy of the source*
 *      * with exception of index, dyn_index and object
 */
void test_route_copy(UNUSED void** state) {
    route_t route_src;
    route_t route_dst;

    memset(&route_src, 0, sizeof(route_t));
    memset(&route_dst, 0, sizeof(route_t));

    // Build up valid input
    route_src.family = AF_INET;
    route_src.dstlen = 24;
    route_src.table = RT_TABLE_MAIN;
    route_src.protocol = RTPROT_BOOT;
    route_src.scope = RT_SCOPE_UNIVERSE;
    route_src.type = RTN_UNICAST;
    memcpy(route_src.dst, str2addr(AF_INET, "192.168.0.0", NULL), addrlen(AF_INET));
    route_src.priority = 2;
    route_src.oif = 1;
    memcpy(route_src.gateway, str2addr(AF_INET, "192.168.0.1", NULL), addrlen(AF_INET));
    memcpy(route_src.prefsrc, str2addr(AF_INET, "192.168.0.168", NULL), addrlen(AF_INET));
    route_src.mtu = 1500;
    route_src.advmss = 6;
    route_src.hoplimit = 10;
    route_src.dyn_index = 5;
    route_src.index = 3;

    // First provide invalid input
    assert_int_equal(route_copy(&route_dst, NULL), -1);
    assert_int_equal(route_copy(NULL, &route_src), -1);

    // Provide valid input
    assert_int_equal(route_copy(&route_dst, &route_src), 0);

    // Check if copy was successful
    assert_int_equal(route_dst.family, route_src.family);
    assert_int_equal(route_dst.dstlen, route_src.dstlen);
    assert_int_equal(route_dst.table, route_src.table);
    assert_int_equal(route_dst.protocol, route_src.protocol);
    assert_int_equal(route_dst.scope, route_src.scope);
    assert_int_equal(route_dst.type, route_src.type);
    assert_int_equal(route_dst.priority, route_src.priority);
    assert_int_equal(route_dst.oif, route_src.oif);
    assert_int_equal(route_dst.mtu, route_src.mtu);
    assert_int_equal(route_dst.advmss, route_src.advmss);
    assert_int_equal(route_dst.hoplimit, route_src.hoplimit);
    assert_string_equal(addr2str(route_dst.family, route_dst.dst, false), addr2str(route_src.family, route_src.dst, false));
    assert_string_equal(addr2str(route_dst.family, route_dst.gateway, false), addr2str(route_src.family, route_src.gateway, false));
    assert_string_equal(addr2str(route_dst.family, route_dst.prefsrc, false), addr2str(route_src.family, route_src.prefsrc, false));

}

/*
 * Test the functions to create, find and destroy the route structs
 * Expectations:
 *  - When an route is created it should be active
 *  - When the correct information is provided it should be able be found
 *  - The found route should contain the same information
 *  - When the route_destroy is called it should be scheduled for deletion and no longer be able to be found
 */
void test_route_struct_functions(UNUSED void** state) {
    route_t* found_route = NULL;
    route_t* route = route_create();
    route_t ws;

    memset(&ws, 0, sizeof(route_t));
    assert_true(route->active);

    route->family = AF_INET;
    route->dstlen = 24;
    route->table = RT_TABLE_MAIN;
    route->protocol = RTPROT_BOOT;
    route->scope = RT_SCOPE_UNIVERSE;
    route->type = RTN_UNICAST;
    memcpy(route->dst, str2addr(AF_INET, "192.168.0.0", NULL), addrlen(AF_INET));
    route->priority = 2;
    route->oif = 1;

    route_copy(&ws, route);

    memcpy(route->gateway, str2addr(AF_INET, "192.168.0.1", NULL), addrlen(AF_INET));
    memcpy(route->prefsrc, str2addr(AF_INET, "192.168.0.168", NULL), addrlen(AF_INET));
    route->mtu = 1500;
    route->advmss = 6;
    route->hoplimit = 5;
    route->dyn_index = 10;
    route->index = 3;

    found_route = route_find(NULL);
    assert_null(found_route);
    found_route = route_find(&ws);
    assert_non_null(found_route);

    assert_int_equal(found_route->family, AF_INET);
    assert_int_equal(found_route->dstlen, 24);
    assert_int_equal(found_route->table, RT_TABLE_MAIN);
    assert_int_equal(found_route->protocol, RTPROT_BOOT);
    assert_int_equal(found_route->scope, RT_SCOPE_UNIVERSE);
    assert_int_equal(found_route->type, RTN_UNICAST);
    assert_int_equal(found_route->priority, 2);
    assert_int_equal(found_route->oif, 1);
    assert_int_equal(found_route->mtu, 1500);
    assert_int_equal(found_route->advmss, 6);
    assert_int_equal(found_route->hoplimit, 5);
    assert_int_equal(found_route->dyn_index, 10);
    assert_int_equal(found_route->index, 3);

    assert_string_equal(addr2str(found_route->family, found_route->dst, false), "192.168.0.0");
    assert_string_equal(addr2str(found_route->family, found_route->gateway, false), "192.168.0.1");
    assert_string_equal(addr2str(found_route->family, found_route->prefsrc, false), "192.168.0.168");

    ws.family = AF_INET6;
    found_route = route_find(&ws);
    assert_null(found_route);

    ws.family = AF_INET;
    ws.dstlen = 23;
    found_route = route_find(&ws);
    assert_null(found_route);

    ws.dstlen = 24;
    ws.table = RT_TABLE_DEFAULT;
    found_route = route_find(&ws);
    assert_null(found_route);

    ws.table = RT_TABLE_MAIN;
    ws.protocol = RTPROT_BABEL;
    found_route = route_find(&ws);
    assert_null(found_route);

    ws.protocol = RTPROT_BOOT;
    ws.scope = RT_SCOPE_LINK;
    found_route = route_find(&ws);
    assert_null(found_route);

    ws.scope = RT_SCOPE_UNIVERSE;
    ws.type = RTN_LOCAL;
    found_route = route_find(&ws);
    assert_null(found_route);

    ws.type = RTN_UNICAST;
    ws.priority = 1;
    found_route = route_find(&ws);
    assert_null(found_route);

    ws.priority = 2;
    ws.oif = 10;
    found_route = route_find(&ws);
    assert_null(found_route);

    ws.oif = 10;
    memcpy(route->dst, str2addr(AF_INET, "192.168.1.0", NULL), addrlen(AF_INET));
    found_route = route_find(&ws);
    assert_null(found_route);

    route_destroy(route, false);
    assert_false(route->active);
    found_route = route_find(&ws);
    assert_null(found_route);

    handle_events();
}

/*
 * Make sure msg2route can handle invalid inputs
 * Expectations:
 *  - Invalid inputs return a -1 instead of causing a valgrind error
 */
void test_msg2route_invalid_input(UNUSED void** state) {
    route_t route;
    rtnlmsg_t* msg = rtnlmsg_instance();

    assert_int_equal(msg2route(NULL, msg), -1);
    msg->nlmsg = NULL;
    assert_int_equal(msg2route(&route, msg), -1);
    msg = NULL;
    assert_int_equal(msg2route(&route, msg), -1);
}

/*
 * Make sure that netlink messages are converted to the route structure correctly
 * The test message comes from calling "ip a add 192.168.100.1/24 dev dummy100" and assumes that the dummy100 link is link 48
 * Expectations:
 *  - All parameters present in the netlink msg are set in the route struct
 */
void test_msg2route(UNUSED void** state) {
    route_t route;
    rtnlmsg_t* msg = rtnlmsg_instance();

    //Message coming from the route generated by "ip a add 192.168.100.1/24 dev dummy100"
    msg->nlmsg = (struct nlmsghdr*) "\x3c\x00\x00\x00\x18\x00\x00\x06\x00\x00\x00\x00\x00\x00\x00\x00\x02\x18\x00\x00\xfe\x02\xfd\x01\x00\x00\x00\x00\x08\x00\x0f\x00\xfe\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x00\x08\x00\x07\x00\xc0\xa8\x64\x01\x08\x00\x04\x00\x30\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;

    assert_int_equal(msg2route(&route, msg), 0);

    // parameters not set by the msg2route should be 0
    assert_int_equal(route.dyn_index, 0);
    assert_int_equal(route.index, 0);

    // Other parameters
    assert_int_equal(route.family, AF_INET);
    assert_int_equal(route.dstlen, 24);
    assert_int_equal(route.table, RT_TABLE_MAIN);
    assert_int_equal(route.protocol, RTPROT_KERNEL);
    assert_int_equal(route.scope, RT_SCOPE_LINK);
    assert_int_equal(route.type, RTN_UNICAST);
    assert_int_equal(route.oif, 48);
    assert_int_equal(route.priority, 0);
    assert_int_equal(route.mtu, 0);
    assert_int_equal(route.advmss, 0);
    assert_int_equal(route.hoplimit, 0);

    assert_string_equal(route.dst, route.dst);
    assert_string_equal(route.gateway, route.gateway);
    assert_string_equal(route.prefsrc, route.prefsrc);
}

/*
 * Start the mercy time and let it run out
 * Expectations:
 *  - After the timer runs out, the route should be scheduled for removing
 */
void test_route_expireMercy(UNUSED void** state) {
    route_t* route = route_create();
    assert_true(route->active);
    amxp_timer_start(route->mercytimer, 0);
    read_sig_alarm();
    assert_false(route->active);
    handle_events();
}


/*
 * Make sure the translation from route to transaction works properly
 * Expectations:
 *  - When provided with invalid inputs the function should fail
 *  - When provided with valid inputs the transactions should contain all values
 *      - When the converters are not initialized some values will be an empty string
 *      - When the converters are initialized the values should be correctly converted
 */
void test_create_transaction_from_route(UNUSED void** state) {
    int rv = -1;
    route_t* route = route_create();
    amxd_trans_t* trans = NULL;
    amxc_var_t* expected_data = NULL;

    // Build up valid route
    route->active = true;
    route->family = AF_INET;
    route->dstlen = 24;
    route->table = RT_TABLE_MAIN;
    route->protocol = RTPROT_BOOT;
    route->scope = RT_SCOPE_UNIVERSE;
    route->type = RTN_UNICAST;
    memcpy(route->dst, str2addr(AF_INET, "192.168.0.0", NULL), addrlen(AF_INET));
    route->priority = 2;
    route->oif = 100;
    memcpy(route->gateway, str2addr(AF_INET, "192.168.0.1", NULL), addrlen(AF_INET));
    memcpy(route->prefsrc, str2addr(AF_INET, "192.168.0.168", NULL), addrlen(AF_INET));
    route->mtu = 1500;
    route->advmss = 6;
    route->hoplimit = 5;
    route->dyn_index = 10;
    route->index = 3;

    // Call with invalid input
    trans = create_transaction_from_route(NULL, true);
    assert_null(trans);
    trans = create_transaction_from_route(NULL, false);
    assert_null(trans);

    // Call with valid input
    expected_data = read_json_from_file("test_data/trans_new_route_no_converters.json");
    trans = create_transaction_from_route(route, true);
    rv = validate_transaction(trans, "NetDev.Link.100.IPv4Route.", action_object_add_inst, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    expected_data = read_json_from_file("test_data/trans_write_route_no_converters.json");
    trans = create_transaction_from_route(route, false);
    rv = validate_transaction(trans, "NetDev.Link.100.IPv4Route.dyn10.", action_object_write, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    route_init_converters();

    expected_data = read_json_from_file("test_data/trans_new_route.json");
    trans = create_transaction_from_route(route, true);
    rv = validate_transaction(trans, "NetDev.Link.100.IPv4Route.", action_object_add_inst, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    expected_data = read_json_from_file("test_data/trans_write_route.json");
    trans = create_transaction_from_route(route, false);
    rv = validate_transaction(trans, "NetDev.Link.100.IPv4Route.dyn10.", action_object_write, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    route_cleanup();
}

/*
 * Provide the handleNewRoute function with invalid input parameters
 * Expectations:
 *  - The function should return '-1' and no error should be reported by Valgrind
 */
void test_handleNewRoute_invalid_input(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();

    msg->nlmsg = NULL;
    assert_int_equal(handleNewRoute(msg), -1);
    msg = NULL;
    assert_int_equal(handleNewRoute(msg), -1);
}

/*
 * Provides the handleNewRoute with an invalid scenario
 *  - The route test data is coming from the route created by doing "ip a add 192.168.100.1/24 dev dummy100" manually and storing the msg in this test
 *  - No valid link exist to which to add the route
 * Expectations:
 *  - Before calling the handleNewRoute no matching route should exist
 *  - handleNewRoute should return with issue
 *  - An active route struct should be found directly after calling the handleNewRoute (no dm instance yet)
 *  - After handling the events
 *      - The route structure should no longer be accessed since it should be invalid
 *      - The route should no longer be found by the route_find function
 *      - No route instance should exist in the datamodel
 */
void test_handleNewRoute_link_does_not_exist(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    route_t ws;
    route_t* route = NULL;
    amxd_object_t* route_obj = NULL;
    int dyn_index = 0;

    //Message coming from the route generated by "ip a add 192.168.100.1/24 dev dummy100"
    msg->nlmsg = (struct nlmsghdr*) "\x3c\x00\x00\x00\x18\x00\x00\x06\x00\x00\x00\x00\x00\x00\x00\x00\x02\x18\x00\x00\xfe\x02\xfd\x01\x00\x00\x00\x00\x08\x00\x0f\x00\xfe\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x00\x08\x00\x07\x00\xc0\xa8\x64\x01\x08\x00\x04\x00\x30\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;

    // Make sure the route does not yet exist
    assert_int_equal(msg2route(&ws, msg), 0);
    route = route_find(&ws);
    assert_null(route);

    // Call handler for route where the link does not exist
    assert_int_equal(handleNewRoute(msg), 0); // handle function should be successful even if the link does not exist

    // The route should exist as soon as the handler is called
    route = route_find(&ws);
    assert_non_null(route);
    assert_true(route->active);
    // Should not exist in the DM until events are handled
    dyn_index = route->dyn_index; // Save this so it can be used after handling events since we expect route to be removed then
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", route->dyn_index);
    assert_null(route_obj);

    handle_events();
    // The route struct should be removed since the transaction should fail
    route = route_find(&ws);
    assert_null(route);
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", dyn_index);
    assert_null(route_obj);
}

/*
 * Provides the handleNewRoute with a valid scenario
 *  - A valid dummy link is provided by calling the add_test_link function
 *  - The route test data is coming from the route created by manually addind routes in docker and storing the msg in this test
 * Expectations:
 *  - The test_link is added without a problem (this must be done before storing the route msg otherwise the add_test_link function will overwrite it)
 *  - Before calling the handleNewRoute no matching route should exist
 *  - handleNewRoute should return without issue
 *  - An active route struct should be found directly after calling the handleNewRoute (no dm instance yet)
 *  - After handling the events
 *      - The route struct should still be active
 *      - An route instance should exist in the datamodel
 *      - The data should match
 *  - When calling the handleNewRoute for the second time (update message)
 *      - The route struct should be updated
 *      - The existing dm instance should be updated
 *  - When calling route_destroy with the option set to true
 *      - Right after calling:
 *          - The route should become inactive
 *          - The route should no longer be found by the route_find function
 *          - The route instance should remain in the DM
 *      - After handling the events:
 *          - The route structure should no longer be accessed since it will be invalid
 *          - The route instance should be removed from the DM
 */
void test_handleNewRoute(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    route_t ws;
    route_t* route = NULL;
    link_t* test_link = NULL;
    amxd_object_t* route_obj = NULL;
    int dyn_index = 0;
    amxc_var_t params;

    // This shares the same rtnlmsg_instance so make sure to set the message for the test after adding the test link
    test_link = add_test_link();
    assert_non_null(test_link);

    //Message coming from the route generated by "ip r add 192.168.100.0/24 via 192.168.100.1 dev dummy100 mtu 1500"
    msg->nlmsg = (struct nlmsghdr*) "\x48\x00\x00\x00\x18\x00\x00\x06\x05\xf3\x58\x63\xa5\x0e\x00\x00\x02\x18\x00\x00\xfe\x03\x00\x01\x00\x00\x00\x00\x08\x00\x0f\x00\xfe\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x00\x0c\x00\x08\x00\x08\x00\x02\x00\xdc\x05\x00\x00\x08\x00\x05\x00\xc0\xa8\x64\x01\x08\x00\x04\x00\x30\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;

    // Make sure the route does not yet exist
    assert_int_equal(msg2route(&ws, msg), 0);
    route = route_find(&ws);
    assert_null(route);

    // Call handler to add the new routes
    assert_int_equal(handleNewRoute(msg), 0);

    // The route should exist as soon as the handler is called
    route = route_find(&ws);
    assert_non_null(route);
    assert_true(route->active);
    // Should not exist in the DM until events are handled
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", route->dyn_index);
    assert_null(route_obj);

    handle_events();
    route = route_find(&ws);
    assert_non_null(route);
    assert_true(route->active);
    assert_string_equal(addr2str(route->family, route->gateway, false), "192.168.100.1");
    dyn_index = route->dyn_index;
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", route->dyn_index);
    assert_non_null(route_obj);

    assert_int_equal(route->index, route_obj->index);
    assert_int_equal(route->oif, test_link->index);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(route_obj, &params, amxd_dm_access_protected);

    assert_true(route_obj == route->object);

    assert_int_equal(GET_INT32(&params, "AdvMSS"), 0);
    assert_string_equal(GET_CHAR(&params, "Dst"), "192.168.100.0");
    assert_int_equal(GET_INT32(&params, "DstLen"), 24);
    assert_string_equal(GET_CHAR(&params, "Gateway"), "192.168.100.1");
    assert_int_equal(GET_INT32(&params, "HopLimit"), 0);
    assert_int_equal(GET_INT32(&params, "MTU"), 1500);
    assert_string_equal(GET_CHAR(&params, "PrefSrc"), "");
    assert_int_equal(GET_INT32(&params, "Priority"), 0);
    assert_string_equal(GET_CHAR(&params, "Protocol"), "boot");
    assert_string_equal(GET_CHAR(&params, "Scope"), "global");
    assert_string_equal(GET_CHAR(&params, "Table"), "main");
    assert_string_equal(GET_CHAR(&params, "Type"), "unicast");
    amxc_var_clean(&params);

    // Change the MTU size from 1500 to 1000
    msg->nlmsg = (struct nlmsghdr*) "\x48\x00\x00\x00\x18\x00\x00\x06\x05\xf3\x58\x63\xa5\x0e\x00\x00\x02\x18\x00\x00\xfe\x03\x00\x01\x00\x00\x00\x00\x08\x00\x0f\x00\xfe\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x00\x0c\x00\x08\x00\x08\x00\x02\x00\xe8\x03\x00\x00\x08\x00\x05\x00\xc0\xa8\x64\x01\x08\x00\x04\x00\x30\x00\x00\x00";

    assert_int_equal(handleNewRoute(msg), 0);
    handle_events();

    route = route_find(&ws);
    assert_non_null(route);
    assert_true(route->active);
    assert_non_null(route->object);
    assert_int_equal(route->mtu, 1000);
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", route->dyn_index);
    assert_non_null(route_obj);
    assert_non_null(route_obj->priv);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(route_obj, &params, amxd_dm_access_protected);

    assert_true(route_obj == route->object);
    assert_int_equal(GET_INT32(&params, "AdvMSS"), 0);
    assert_string_equal(GET_CHAR(&params, "Dst"), "192.168.100.0");
    assert_int_equal(GET_INT32(&params, "DstLen"), 24);
    assert_string_equal(GET_CHAR(&params, "Gateway"), "192.168.100.1");
    assert_int_equal(GET_INT32(&params, "HopLimit"), 0);
    assert_int_equal(GET_INT32(&params, "MTU"), 1000);
    assert_string_equal(GET_CHAR(&params, "PrefSrc"), "");
    assert_int_equal(GET_INT32(&params, "Priority"), 0);
    assert_string_equal(GET_CHAR(&params, "Protocol"), "boot");
    assert_string_equal(GET_CHAR(&params, "Scope"), "global");
    assert_string_equal(GET_CHAR(&params, "Table"), "main");
    assert_string_equal(GET_CHAR(&params, "Type"), "unicast");
    amxc_var_clean(&params);

    route_destroy(route, true);
    assert_false(route->active);
    assert_null(route->object);
    route = route_find(&ws);
    assert_null(route);
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", dyn_index);
    assert_non_null(route_obj); // Should only be removed from the datamodel after events are handled

    handle_events();

    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", dyn_index);
    assert_null(route_obj);
}

/*
 * Provide the handleDelRoute function with invalid input parameters
 * Expectations:
 *  - The function should return '-1' and no error should be reported by Valgrind
 */
void test_handleDelRoute_invalid_input(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    assert_int_equal(handleDelRoute(NULL), -1);
    msg->nlmsg = NULL;
    assert_int_equal(handleDelRoute(msg), -1);
}

/*
 * Make sure the delete handler does not schedule routes to be deleted that are already inactive
 * Expectations:
 *  - After calling the delete handler (expected to fail because no active route could be found)
 *    - The instance should still exist in the DM
 *  - No mercy timer should be started so read_sig_alarm should timeout
 *    - The object value in the route struct should NOT be set to NULL
 *    - The datamodel objects priv value should NOT be set to NULL
 *    - The instance should still be available in the DM
 *  - After handling the events
 *    - The object value in the route struct should NOT be set to NULL
 *    - The datamodel objects priv value should NOT be set to NULL
 *    - The instance should still be available in the DM
 */
void test_handleDelRoute_inactive_route(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    route_t* test_route = NULL;
    link_t* test_link = NULL;
    amxd_object_t* route_obj = NULL;
    int dyn_index = 0;

    // This shares the same rtnlmsg_instance so make sure to set the message for the test after adding the test link
    test_link = add_test_link();
    assert_non_null(test_link);

    test_route = add_test_route();
    assert_non_null(test_route);
    dyn_index = test_route->dyn_index;

    test_route->active = false;

    // Call the handler to remove the route
    msg->nlmsg = (struct nlmsghdr*) "\x3c\x00\x00\x00\x19\x00\x00\x00\xd2\x04\x59\x63\x71\x25\x00\x00\x02\x18\x00\x00\xfe\x02\xfd\x01\x00\x00\x00\x00\x08\x00\x0f\x00\xfe\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x00\x08\x00\x07\x00\xc0\xa8\x64\x01\x08\x00\x04\x00\x30\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;
    assert_int_equal(handleDelRoute(msg), -1); // Should fail because no active route was found
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", dyn_index);
    assert_non_null(route_obj);
    assert_non_null(route_obj->priv);
    assert_non_null(test_route->object);

    read_sig_alarm(); // The timer should not be started so this should timeout
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", dyn_index);
    assert_non_null(route_obj);
    assert_non_null(route_obj->priv);
    assert_non_null(test_route->object);

    handle_events();
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", dyn_index);
    assert_non_null(route_obj);
    assert_non_null(route_obj->priv);
    assert_non_null(test_route->object);

    test_route->active = true;
    route_destroy(test_route, true);
    handle_events();
}

/*
 * Make sure the route delete messages are handled correctly
 * Expectations:
 *  - After calling the delete handler
 *    - The route should still be active and should still be found by route_find
 *    - The instance should still exist in the DM
 *  - After letting the mercytimer run out
 *    - The route should go to inactive and no longer be able to be found by route_find
 *    - The object value in the route struct should be set to NULL
 *    - The datamodel objects priv value should be set to NULL
 *    - The instance should still be available in the DM
 *  - After handling the events, the instance should be removed from the dm
 */
void test_handleDelRoute(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    route_t* route = NULL;
    route_t* test_route = NULL;
    link_t* test_link = NULL;
    amxd_object_t* route_obj = NULL;
    int dyn_index = 0;

    // This shares the same rtnlmsg_instance so make sure to set the message for the test after adding the test link
    test_link = add_test_link();
    assert_non_null(test_link);

    test_route = add_test_route();
    assert_non_null(test_route);
    dyn_index = test_route->dyn_index;

    // Call the handler to remove the route
    msg->nlmsg = (struct nlmsghdr*) "\x3c\x00\x00\x00\x19\x00\x00\x00\xd2\x04\x59\x63\x71\x25\x00\x00\x02\x18\x00\x00\xfe\x02\xfd\x01\x00\x00\x00\x00\x08\x00\x0f\x00\xfe\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x00\x08\x00\x07\x00\xc0\xa8\x64\x01\x08\x00\x04\x00\x30\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 12;
    assert_int_equal(handleDelRoute(msg), 0);
    route = route_find(test_route);
    assert_non_null(route);
    assert_true(route->active);
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", dyn_index);
    assert_non_null(route_obj);
    assert_non_null(route_obj->priv);

    read_sig_alarm(); // Let the mercytimer run out
    assert_false(route->active);
    assert_null(route->object);
    route = route_find(test_route);
    assert_null(route);
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", dyn_index);
    assert_non_null(route_obj);
    assert_null(route_obj->priv);

    handle_events();
    route_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", dyn_index);
    assert_null(route_obj);
}

static route_t* test_add_default_route(void) {
    route_t* test_route = NULL;

    test_route = add_test_route();
    assert_non_null(test_route);

    test_route->dstlen = 0;
    memcpy(test_route->dst, str2addr(AF_INET, "0.0.0.0", NULL), addrlen(AF_INET));

    test_route->family = AF_INET;
    test_route->oif = 5;
    test_route->index = 3;
    test_route->table = RT_TABLE_MAIN;
    test_route->protocol = RTPROT_BOOT;
    test_route->scope = RT_SCOPE_UNIVERSE;
    test_route->type = RTN_UNICAST;
    test_route->priority = 2;
    test_route->mtu = 1500;
    test_route->advmss = 6;
    test_route->hoplimit = 10;

    memcpy(test_route->gateway, str2addr(AF_INET, "192.168.0.1", NULL), addrlen(AF_INET));
    memcpy(test_route->prefsrc, str2addr(AF_INET, "192.168.0.168", NULL), addrlen(AF_INET));

    assert_true(set_if_default_route(test_route));

    test_route->def_neigh = &mock_default_neighbour;

    return test_route;
}

/*
 * Provides the GetDefaultRoute without any default routes
 * Expectations:
 *  - After calling GetDefaultRoute() the return value:
 *    - Is an empty htable
 */
void test_GetDefaultRoute_without_routes(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_init(&ret);

    route_clear();

    assert_int_equal(_GetDefaultRoute(NULL, NULL, NULL, &ret), 0);

    assert_int_equal(amxc_var_type_of(&ret), AMXC_VAR_ID_HTABLE);
    assert_null(amxc_var_get_first(&ret));

    amxc_var_clean(&ret);
}

/*
 * Provides the GetDefaultRoute with a valid scenario
 * Expectations:
 *  - After calling GetDefaultRoute() the return value:
 *    - Has the route as active
 *    - Has a valid default gateway
 *    - Has an valid interface index
 */
void test_GetDefaultRoute(UNUSED void** state) {
    amxc_var_t ret;
    amxc_var_init(&ret);

    route_clear();
    assert_non_null(add_test_link());
    test_add_default_route();

    assert_int_equal(_GetDefaultRoute(NULL, NULL, NULL, &ret), 0);

    assert_true(GET_BOOL(&ret, "Active"));
    assert_string_equal(GET_CHAR(&ret, "DefaultGateway"), "192.168.0.1");
    assert_int_equal(GET_INT32(&ret, "InterfaceIndex"), 5);
    assert_string_equal(GET_CHAR(&ret, "InterfaceName"), "br-lan");
    assert_string_equal(GET_CHAR(&ret, "MACAddress"), "00:01:02:03:04:05");
    assert_string_equal(GET_CHAR(&ret, "PhysBridgeInterface"), "eth0");

    amxc_var_clean(&ret);
}
