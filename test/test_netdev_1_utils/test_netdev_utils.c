/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>

#include <arpa/inet.h>

#include "netdev_util.h"

#include "test_netdev_utils.h"

int test_netdev_setup(UNUSED void** state) {
    return 0;
}

int test_netdev_teardown(UNUSED void** state) {
    return 0;
}

void test_family2str(UNUSED void** state) {
    assert_string_equal(family2str('\0'), "Unspec");
    assert_string_equal(family2str('a'), "Unspec");
    assert_string_equal(family2str('1'), "Unspec");
    assert_string_equal(family2str(' '), "Unspec");
    assert_string_equal(family2str(AF_INET), "IPv4");
    assert_string_equal(family2str(AF_INET6), "IPv6");
}

void test_addrlen(UNUSED void** state) {
    assert_int_equal(addrlen('\0'), 0);
    assert_int_equal(addrlen('a'), 0);
    assert_int_equal(addrlen('1'), 0);
    assert_int_equal(addrlen(' '), 0);
    assert_int_equal(addrlen(AF_INET), 4);
    assert_int_equal(addrlen(AF_INET6), 16);
}

void test_addr2str(UNUSED void** state) {
    unsigned char buf[sizeof(struct in6_addr)];
    const char* addr_str = "192.168.0.1";
    const char* ret_addr = NULL;
    static unsigned char zeroaddr[ADDRSPACE];
    memset(zeroaddr, 0, ADDRSPACE);

    inet_pton(AF_INET, addr_str, buf);
    ret_addr = addr2str(AF_INET, buf, false);
    assert_non_null(ret_addr);
    assert_string_equal(ret_addr, addr_str);

    ret_addr = addr2str(AF_INET, buf, true);
    assert_non_null(ret_addr);
    assert_string_equal(ret_addr, addr_str);

    addr_str = "fe80::900c:74ff:fedc:1f25";
    inet_pton(AF_INET6, addr_str, buf);
    ret_addr = addr2str(AF_INET6, buf, false);
    assert_non_null(ret_addr);
    assert_string_equal(ret_addr, addr_str);

    ret_addr = addr2str(AF_INET6, buf, true);
    assert_non_null(ret_addr);
    assert_string_equal(ret_addr, addr_str);

    // Zero address: if not optional, expect a valid address
    ret_addr = addr2str(AF_INET, zeroaddr, false);
    assert_non_null(ret_addr);
    assert_string_equal(ret_addr, "0.0.0.0");
    ret_addr = addr2str(AF_INET6, zeroaddr, false);
    assert_non_null(ret_addr);
    assert_string_equal(ret_addr, "::");

    // Zero address: if optional, expect empty string
    ret_addr = addr2str(AF_INET, zeroaddr, true);
    assert_non_null(ret_addr);
    assert_string_equal(ret_addr, "");
    ret_addr = addr2str(AF_INET6, zeroaddr, true);
    assert_non_null(ret_addr);
    assert_string_equal(ret_addr, "");

    // Invalid family
    ret_addr = addr2str(' ', zeroaddr, false);
    assert_non_null(ret_addr);
    assert_string_equal(ret_addr, "");
    ret_addr = addr2str(' ', zeroaddr, false);
    assert_non_null(ret_addr);
    assert_string_equal(ret_addr, "");
}

void test_addr2flag_v4(UNUSED void** state) {
    unsigned char buf[sizeof(struct in6_addr)];
    const char* addr_str = NULL;
    const char* ret_flag = NULL;
    static unsigned char zeroaddr[ADDRSPACE];
    memset(zeroaddr, 0, ADDRSPACE);

    // Addr = NULL
    ret_flag = addr2flag(AF_INET, NULL, false);
    assert_string_equal(ret_flag, "");
    ret_flag = addr2flag(AF_INET, NULL, true);
    assert_string_equal(ret_flag, "");

    // Zero address: if option expect empty string
    ret_flag = addr2flag(AF_INET, zeroaddr, true);
    assert_string_equal(ret_flag, "");

    // Zero address: if not option expect @any flag
    ret_flag = addr2flag(AF_INET, zeroaddr, false);
    assert_string_equal(ret_flag, "@any");

    // Test different IPs to match flag
    addr_str = "127.0.0.0";
    inet_pton(AF_INET, addr_str, buf);
    ret_flag = addr2flag(AF_INET, buf, false);
    assert_string_equal(ret_flag, "@loopback");

    addr_str = "10.0.0.0"; // Private IP/8
    inet_pton(AF_INET, addr_str, buf);
    ret_flag = addr2flag(AF_INET, buf, false);
    assert_string_equal(ret_flag, "@private");

    addr_str = "172.16.0.0"; // Private IP/12
    inet_pton(AF_INET, addr_str, buf);
    ret_flag = addr2flag(AF_INET, buf, false);
    assert_string_equal(ret_flag, "@private");

    addr_str = "192.168.0.0"; // Private IP/16
    inet_pton(AF_INET, addr_str, buf);
    ret_flag = addr2flag(AF_INET, buf, false);
    assert_string_equal(ret_flag, "@private");

    addr_str = "100.64.0.0";
    inet_pton(AF_INET, addr_str, buf);
    ret_flag = addr2flag(AF_INET, buf, false);
    assert_string_equal(ret_flag, "@cgn");

    addr_str = "224.0.0.0";
    inet_pton(AF_INET, addr_str, buf);
    ret_flag = addr2flag(AF_INET, buf, false);
    //assert_string_equal(ret_flag, "@mc"); // mc expected but this is reported as GUA

    addr_str = "0.0.0.0";
    inet_pton(AF_INET, addr_str, buf);
    ret_flag = addr2flag(AF_INET, buf, false);
    assert_string_equal(ret_flag, "@any");

    addr_str = "255.255.255.255";
    inet_pton(AF_INET, addr_str, buf);
    ret_flag = addr2flag(AF_INET, buf, false);
    assert_string_equal(ret_flag, "@broadcast");

    // empty string also returns @broadcast -> same as @none
    // @none can never be reached
    addr_str = "";
    inet_pton(AF_INET, addr_str, buf);
    ret_flag = addr2flag(AF_INET, buf, false);
    assert_string_equal(ret_flag, "@broadcast");

    addr_str = "1.2.3.4";
    inet_pton(AF_INET, addr_str, buf);
    ret_flag = addr2flag(AF_INET, buf, false);
    assert_string_equal(ret_flag, "@gua");
}

void test_addr2flag_v6(UNUSED void** state) {
    unsigned char buf[sizeof(struct in6_addr)];
    const char* addr_str = NULL;
    const char* ret_flag = NULL;
    static unsigned char zeroaddr[ADDRSPACE];
    memset(zeroaddr, 0, ADDRSPACE);

    // Addr = NULL
    ret_flag = addr2flag(AF_INET6, NULL, false);
    assert_string_equal(ret_flag, "");
    ret_flag = addr2flag(AF_INET6, NULL, true);
    assert_string_equal(ret_flag, "");

    // Zero address: if option expect empty string
    ret_flag = addr2flag(AF_INET6, zeroaddr, true);
    assert_string_equal(ret_flag, "");

    // Zero address: if not option expect @unknown flag
    ret_flag = addr2flag(AF_INET6, zeroaddr, false);
    assert_string_equal(ret_flag, "@unknown");

    // Test different IPs to match flag
    addr_str = "::1";
    inet_pton(AF_INET6, addr_str, buf);
    ret_flag = addr2flag(AF_INET6, buf, false);
    assert_string_equal(ret_flag, "@loopback");

    addr_str = "fe80::";
    inet_pton(AF_INET6, addr_str, buf);
    ret_flag = addr2flag(AF_INET6, buf, false);
    assert_string_equal(ret_flag, "@lla");

    addr_str = "ff00::";
    inet_pton(AF_INET6, addr_str, buf);
    ret_flag = addr2flag(AF_INET6, buf, false);
    assert_string_equal(ret_flag, "@mc");

    addr_str = "2002::";
    inet_pton(AF_INET6, addr_str, buf);
    ret_flag = addr2flag(AF_INET6, buf, false);
    assert_string_equal(ret_flag, "@gua");

    addr_str = "fc00::";
    inet_pton(AF_INET6, addr_str, buf);
    ret_flag = addr2flag(AF_INET6, buf, false);
    assert_string_equal(ret_flag, "@ula");

    addr_str = "aa00::";
    inet_pton(AF_INET6, addr_str, buf);
    ret_flag = addr2flag(AF_INET6, buf, false);
    assert_string_equal(ret_flag, "@unknown");
}

void test_str2addr_v4(UNUSED void** state) {
    const char* addr_str = NULL;
    const char* ret_str = NULL;
    unsigned char* ret_addr = NULL;
    char buf[INET6_ADDRSTRLEN];
    bool success = false;

    // No boolean
    addr_str = "127.0.0.1";
    ret_addr = str2addr(AF_INET, addr_str, NULL);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, addr_str);

    addr_str = "192.168.0.1";
    ret_addr = str2addr(AF_INET, addr_str, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, addr_str);

    addr_str = "255.255.255.255";
    ret_addr = str2addr(AF_INET, addr_str, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, addr_str);

    addr_str = "0.0.0.0";
    ret_addr = str2addr(AF_INET, addr_str, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "0.0.0.0");

    addr_str = "";
    ret_addr = str2addr(AF_INET, addr_str, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "0.0.0.0");

    ret_addr = str2addr(AF_INET, NULL, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "0.0.0.0");

    // From now on expected to fail
    addr_str = "192.168.0.1/24";
    ret_addr = str2addr(AF_INET, addr_str, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "0.0.0.0");

    addr_str = "192.168.0";
    ret_addr = str2addr(AF_INET, addr_str, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "0.0.0.0");

    addr_str = "alphabetical";
    ret_addr = str2addr(AF_INET, addr_str, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "0.0.0.0");

    addr_str = "1234";
    ret_addr = str2addr(AF_INET, addr_str, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "0.0.0.0");

    addr_str = "192.168.0.1";
    ret_addr = str2addr(' ', addr_str, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "0.0.0.0");

    ret_addr = str2addr(AF_INET6, addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "::");
}

void test_str2addr_v6(UNUSED void** state) {
    const char* addr_str = NULL;
    const char* ret_str = NULL;
    unsigned char* ret_addr = NULL;
    char buf[INET6_ADDRSTRLEN];
    bool success = false;

    // No boolean
    addr_str = "::1";
    ret_addr = str2addr(AF_INET6, addr_str, NULL);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, addr_str);

    // Valid input
    addr_str = "::1";
    ret_addr = str2addr(AF_INET6, addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, addr_str);

    addr_str = "fe80::";
    ret_addr = str2addr(AF_INET6, addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, addr_str);

    addr_str = "fe80::345e:bcdf:fcec:a193";
    ret_addr = str2addr(AF_INET6, addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, addr_str);

    addr_str = "::";
    ret_addr = str2addr(AF_INET6, addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "::");

    addr_str = "";
    ret_addr = str2addr(AF_INET6, addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "::");

    ret_addr = str2addr(AF_INET6, NULL, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_true(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "::");

    // From now on expected to fail
    addr_str = "fe80::283e:bcff::/64";
    ret_addr = str2addr(AF_INET6, addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "::");

    addr_str = "1:2:3:4:4:6:7";
    ret_addr = str2addr(AF_INET6, addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "::");

    addr_str = "alphabetical";
    ret_addr = str2addr(AF_INET6, addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "::");

    addr_str = "1234";
    ret_addr = str2addr(AF_INET6, addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "::");

    addr_str = "192.168.0.1";
    ret_addr = str2addr(' ', addr_str, &success);
    ret_str = inet_ntop(AF_INET6, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "::");

    addr_str = "fe80::345e:bcdf:fcec:a193";
    ret_addr = str2addr(AF_INET, addr_str, &success);
    ret_str = inet_ntop(AF_INET, ret_addr, buf, INET6_ADDRSTRLEN);
    assert_false(success);
    assert_non_null(ret_str);
    assert_string_equal(ret_str, "0.0.0.0");
}

void test_addr_isnull(UNUSED void** state) {
    unsigned char* addr = NULL;
    bool success = false;

    addr = str2addr(AF_INET6, "fe80::345e:bcdf:fcec:a193", &success);
    assert_true(success);
    assert_false(addr_isNull(AF_INET6, addr));
    addr = str2addr(AF_INET, "192.168.10.1", &success);
    assert_true(success);
    assert_false(addr_isNull(AF_INET, addr));

    assert_true(addr_isNull(' ', NULL));
    assert_true(addr_isNull(' ', addr));
    assert_true(addr_isNull(AF_INET, NULL));
    assert_true(addr_isNull(AF_INET6, NULL));
    addr = str2addr(AF_INET, "", &success);
    assert_true(success);
    assert_true(addr_isNull(AF_INET, addr));
    assert_true(addr_isNull(AF_INET6, addr));

    addr = str2addr(AF_INET, "0.0.0.0", &success);
    assert_true(success);
    assert_true(addr_isNull(AF_INET, addr));
    addr = str2addr(AF_INET6, "::", &success);
    assert_true(success);
    assert_true(addr_isNull(AF_INET6, addr));
}

void test_uptime(UNUSED void** state) {
    long uptime_start = uptime();
    sleep(5);
    long uptime_stop = uptime();

    assert_true((uptime_stop >= uptime_start + 4) && (uptime_stop <= uptime_start + 6));
}

void test_deferred_trans_create_clean(UNUSED void** state) {
    char* priv_data = "This is just some data to test as priv";
    const char* return_data = NULL;
    deferred_trans_t* deferred_trans = NULL;
    amxd_trans_t* transaction = NULL;
    amxd_trans_new(&transaction);

    assert_false(deferred_trans_create(NULL, transaction, priv_data));

    assert_true(deferred_trans_create(&deferred_trans, NULL, priv_data));
    assert_non_null(deferred_trans);
    assert_non_null(deferred_trans->priv);
    return_data = (char*) deferred_trans->priv;
    assert_string_equal(return_data, priv_data);
    assert_null(deferred_trans->transaction);
    deferred_trans_clean(&deferred_trans);
    assert_null(deferred_trans);

    assert_true(deferred_trans_create(&deferred_trans, transaction, NULL));
    assert_non_null(deferred_trans);
    assert_null(deferred_trans->priv);
    assert_non_null(deferred_trans->transaction);
    assert_int_equal(&(*(deferred_trans)->transaction), transaction);
    deferred_trans_clean(&deferred_trans);
    assert_null(deferred_trans);

    amxd_trans_new(&transaction);
    assert_true(deferred_trans_create(&deferred_trans, transaction, priv_data));
    assert_non_null(deferred_trans);
    assert_non_null(deferred_trans->priv);
    return_data = (char*) deferred_trans->priv;
    assert_string_equal(return_data, priv_data);
    assert_non_null(deferred_trans->transaction);
    assert_int_equal(&(*(deferred_trans)->transaction), transaction);
    deferred_trans_clean(&deferred_trans);
    assert_null(deferred_trans);
}
