/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>

#include <arpa/inet.h>
#include <linux/rtnetlink.h>
#include <linux/if.h>
#include <linux/if_arp.h>

#include "netdev_netlink.h"
#include "dm_link_addr.h"
#include "dm_link.h"
#include "dm_netdev.h"
#include "dm_convert.h"
#include "../common/test_netdev_common_setup.h"
#include "../common/common_functions.h"
#include "test_dm_link_addr.h"

unsigned int if_nametoindex(const char* ifname);

static link_t* add_test_link(void) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    link_t* link = NULL;
    amxd_object_t* link_obj = NULL;
    const int index = 48;

    // Msg coming from "ip link add dummy100 type dummy", index = 48
    msg->nlmsg = (struct nlmsghdr*) "\x50\x05\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x30\x00\x00\x00\x82\x00\x00\x00\xff\xff\xff\xff\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x0d\x00\xe8\x03\x00\x00\x05\x00\x10\x00\x02\x00\x00\x00\x05\x00\x11\x00\x00\x00\x00\x00\x08\x00\x04\x00\xdc\x05\x00\x00\x08\x00\x32\x00\x00\x00\x00\x00\x08\x00\x33\x00\x00\x00\x00\x00\x08\x00\x1b\x00\x00\x00\x00\x00\x08\x00\x1e\x00\x00\x00\x00\x00\x08\x00\x1f\x00\x01\x00\x00\x00\x08\x00\x28\x00\xff\xff\x00\x00\x08\x00\x29\x00\x00\x00\x01\x00\x08\x00\x20\x00\x01\x00\x00\x00\x05\x00\x21\x00\x01\x00\x00\x00\x09\x00\x06\x00\x6e\x6f\x6f\x70\x00\x00\x00\x00\x08\x00\x23\x00\x00\x00\x00\x00\x08\x00\x2f\x00\x00\x00\x00\x00\x08\x00\x30\x00\x00\x00\x00\x00\x05\x00\x27\x00\x00\x00\x00\x00\x24\x00\x0e\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0a\x00\x01\x00\x72\x16\x1a\xb4\x68\x0d\x00\x00\x0a\x00\x02\x00\xff\xff\xff\xff\xff\xff\x00\x00\xc4\x00\x17\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x64\x00\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x2b\x00\x05\x00\x02\x00\x00\x00\x00\x00\x10\x00\x12\x00\x0a\x00\x01\x00\x64\x75\x6d\x6d\x79\x00\x00\x00\x0c\x03\x1a\x00\x88\x00\x02\x00\x84\x00\x01\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x10\x27\x00\x00\xe8\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\x02\x0a\x00\x08\x00\x01\x00\x00\x00\x00\x00\x14\x00\x05\x00\xff\xff\x00\x00\x45\xe3\x19\x00\x54\x54\x00\x00\xe8\x03\x00\x00\xe4\x00\x02\x00\x00\x00\x00\x00\x40\x00\x00\x00\xdc\x05\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\xff\xff\xff\xff\xa0\x0f\x00\x00\xe8\x03\x00\x00\x00\x00\x00\x00\x80\x3a\x09\x00\x80\x51\x01\x00\x03\x00\x00\x00\x58\x02\x00\x00\x10\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00\x60\xea\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\xff\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00\x10\x27\x00\x00\xe8\x03\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\xee\x36\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\xff\xff\x00\x00\xff\xff\xff\xff\x2c\x01\x03\x00\x25\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x34\x00\x06\x00\x06\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x14\x00\x07\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x05\x00\x08\x00\x00\x00\x00\x00";
    msg->buflen = 8192;
    msg->hdrlen = 16;

    // Make sure the link does not already exists
    link = link_find(index);
    assert_null(link);
    link_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100");
    assert_null(link_obj);

    // Call the handler and handle events so the link exist in the DM as well
    assert_int_equal(handleNewLink(msg), 0);
    handle_events();

    link = link_find(48);
    assert_non_null(link);
    link_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100");
    assert_non_null(link_obj);

    return link;
}

static addr_t* add_test_addr(void) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    addr_t ws;
    addr_t* addr = NULL;
    amxd_object_t* addr_obj = NULL;

    //Message coming from "ip a add 192.168.100.1/24 dev dummy100"
    msg->nlmsg = (struct nlmsghdr*) "\x54\x00\x00\x00\x14\x00\x00\x00\xf4\x62\x56\x63\x7f\x16\x00\x00\x02\x18\x80\x00\x30\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x01\x08\x00\x02\x00\xc0\xa8\x64\x01\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x08\x00\x80\x00\x00\x00\x14\x00\x06\x00\xff\xff\xff\xff\xff\xff\xff\xff\xe7\xdb\x0e\x00\xe7\xdb\x0e\x00";
    msg->buflen = 8192;
    msg->hdrlen = 8;

    // Make sure the address does not yet exist
    assert_int_equal(msg2addr(&ws, msg), 0);
    addr = addr_find(&ws);
    assert_null(addr);

    // Call handler to add the addres
    assert_int_equal(handleNewAddr(msg), 0);
    handle_events();

    addr = addr_find(&ws);
    assert_non_null(addr);
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", addr->dyn_index);
    assert_non_null(addr_obj);

    return addr;
}

int test_netdev_setup(void** state) {
    test_netdev_common_setup(state, false);
    assert_int_equal(0, amxo_parser_scan_mib_dir(test_setup_parser(), "../../mibs"));
    _netdev_main(AMXO_START, test_setup_dm(), test_setup_parser());
    handle_events();

    return 0;
}

int test_netdev_teardown(void** state) {
    _netdev_main(AMXO_STOP, test_setup_dm(), test_setup_parser());
    handle_events();
    test_netdev_common_teardown(state);
    return 0;
}

void test_init_addr_flags_converter(UNUSED void** state) {
    converter_t* test_converter = init_addr_flags_converter();
    assert_non_null(test_converter);

    assert_string_equal(convert_bin2str(test_converter, IFA_F_SECONDARY), "secondary");
    assert_string_equal(convert_bin2str(test_converter, IFA_F_NODAD), "nodad");
    assert_string_equal(convert_bin2str(test_converter, IFA_F_OPTIMISTIC), "optimistic");
    assert_string_equal(convert_bin2str(test_converter, IFA_F_DADFAILED), "dadfailed");
    assert_string_equal(convert_bin2str(test_converter, IFA_F_HOMEADDRESS), "homeaddress");
    assert_string_equal(convert_bin2str(test_converter, IFA_F_DEPRECATED), "deprecated");
    assert_string_equal(convert_bin2str(test_converter, IFA_F_TENTATIVE), "tentative");
    assert_string_equal(convert_bin2str(test_converter, IFA_F_PERMANENT), "permanent");

    // For converter_attribute_flags it should be possible to get a combination of flags
    assert_string_equal(convert_bin2str(test_converter, IFA_F_SECONDARY | IFA_F_NODAD |
                                        IFA_F_OPTIMISTIC | IFA_F_DADFAILED | IFA_F_HOMEADDRESS |
                                        IFA_F_DEPRECATED | IFA_F_TENTATIVE | IFA_F_PERMANENT),
                        "secondary nodad optimistic dadfailed homeaddress deprecated tentative permanent");

    // Any other bin value should return an empty string
    assert_string_equal(convert_bin2str(test_converter, 65536), "");

    converter_destroy(&test_converter);

    // Make sure that an empty string is returned in case the converter is destroyed
    assert_string_equal(convert_bin2str(test_converter, IFA_F_DEPRECATED), "");
}

void test_init_addr_scope_converter(UNUSED void** state) {
    converter_t* test_converter = init_addr_scope_converter();
    assert_non_null(test_converter);

    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_UNIVERSE), "global");
    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_SITE), "site");
    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_HOST), "host");
    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_LINK), "link");
    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_NOWHERE), "nowhere");

    // Any other bin value should return the bin value as a string (since no default is set)
    assert_string_equal(convert_bin2str(test_converter, 99), "99");
    assert_string_equal(convert_bin2str(test_converter, 35), "35");

    converter_destroy(&test_converter);

    // Make sure that an empty string is returned in case the converter is destroyed
    assert_string_equal(convert_bin2str(test_converter, RT_SCOPE_LINK), "");
}

/*
 * Test the functions to create, find and destroy the address structs
 * Expectations:
 *  - When an addr is created it should be active
 *  - When the correct information is provided it should be able be found
 *  - The found addr should contain the same information
 *  - When the addr_destroy is called it should be scheduled for deletion and no longer be able to be found
 */
void test_addr_struct_functions(UNUSED void** state) {
    addr_t* found_addr = NULL;
    addr_t* addr = addr_create();
    addr_t ws;

    memset(&ws, 0, sizeof(addr_t));

    assert_true(addr->active);

    addr->link = 1;
    addr->family = AF_INET;
    addr->dyn_index = 987;
    memcpy(addr->address, str2addr(AF_INET, "192.168.1.1", NULL), addrlen(AF_INET));

    found_addr = addr_find(NULL);
    assert_null(found_addr);
    found_addr = addr_find(&ws);
    assert_null(found_addr);
    ws.link = 1;
    found_addr = addr_find(&ws);
    assert_null(found_addr);
    ws.family = AF_INET;
    found_addr = addr_find(&ws);
    assert_null(found_addr);
    memcpy(ws.address, str2addr(AF_INET, "192.168.1.1", NULL), addrlen(AF_INET));
    found_addr = addr_find(&ws);
    assert_non_null(found_addr);
    assert_true(found_addr->active);
    assert_int_equal(found_addr->link, addr->link);
    assert_int_equal(found_addr->family, addr->family);
    assert_int_equal(found_addr->dyn_index, addr->dyn_index); // This is information that is not in the provided address ws
    assert_string_equal(addr2str(found_addr->family, found_addr->address, false), "192.168.1.1");

    addr_destroy(addr, false);
    assert_false(addr->active);
    found_addr = addr_find(&ws);
    assert_null(found_addr);

    handle_events();
}

/*
 * Make sure msg2addr can handle invalid inputs
 * Expectations:
 *  - Invalid inputs return a -1 instead of causing a valgrind error
 */
void test_msg2addr_invalid_input(UNUSED void** state) {
    addr_t addr;
    rtnlmsg_t* msg = rtnlmsg_instance();

    assert_int_equal(msg2addr(NULL, msg), -1);
    msg->nlmsg = NULL;
    assert_int_equal(msg2addr(&addr, msg), -1);
    msg = NULL;
    assert_int_equal(msg2addr(&addr, msg), -1);
}

/*
 * Make sure that netlink messages are converted to the addr structure correctly
 * The test message comes from calling "ip a add 192.168.100.1/24 dev dummy100" and assumes that the dummy100 link is link 48
 * Expectations:
 *  - All parameters present in the netlink msg are set in the address struct
 */
void test_msg2addr(UNUSED void** state) {
    addr_t addr;
    rtnlmsg_t* msg = rtnlmsg_instance();

    //Message coming from "ip a add 192.168.100.1/24 dev dummy100"
    msg->nlmsg = (struct nlmsghdr*) "\x54\x00\x00\x00\x14\x00\x00\x00\xf4\x62\x56\x63\x7f\x16\x00\x00\x02\x18\x80\x00\x30\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x01\x08\x00\x02\x00\xc0\xa8\x64\x01\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x08\x00\x80\x00\x00\x00\x14\x00\x06\x00\xff\xff\xff\xff\xff\xff\xff\xff\xe7\xdb\x0e\x00\xe7\xdb\x0e\x00";
    msg->buflen = 8192;
    msg->hdrlen = 8;

    assert_int_equal(msg2addr(&addr, msg), 0);

    assert_true(addr.created_timestamp > 0);
    assert_int_equal(addr.dyn_index, 0); // Should not be set by this function
    assert_int_equal(addr.family, AF_INET);
    assert_int_equal(addr.flags, IFA_F_PERMANENT);
    assert_int_equal(addr.index, 0);
    assert_int_equal(addr.link, 48); // The dummy100 link has index 48 in the message
    assert_int_equal(addr.prefixlen, 24);
    assert_int_equal(addr.scope, 0);
    assert_int_equal(addr.preferred_lifetime, 0);
    assert_int_equal(addr.valid_lifetime, 0);
    assert_true(addr.updated_timestamp > 0);
    assert_string_equal(addr2str(addr.family, addr.address, false), "192.168.100.1");
    assert_string_equal(addr2str(addr.family, addr.peer, true), "");
}

/*
 * Test the addr_copy function
 * Expectations:
 *  - When providing invalid input the function should return -1
 *  - When valid inputs are provided the destination address should be a copy of the source*
 *      * With exception of: index, dyn_index, object
 */
void test_addr_copy(UNUSED void** state) {
    addr_t addr_src;
    addr_t* addr_dst = addr_create();
    rtnlmsg_t* msg = rtnlmsg_instance();

    //Message coming from "ip a add 192.168.100.1/24 dev dummy100"
    msg->nlmsg = (struct nlmsghdr*) "\x54\x00\x00\x00\x14\x00\x00\x00\xf4\x62\x56\x63\x7f\x16\x00\x00\x02\x18\x80\x00\x30\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x01\x08\x00\x02\x00\xc0\xa8\x64\x01\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x08\x00\x80\x00\x00\x00\x14\x00\x06\x00\xff\xff\xff\xff\xff\xff\xff\xff\xe7\xdb\x0e\x00\xe7\xdb\x0e\x00";
    msg->buflen = 8192;
    msg->hdrlen = 8;

    assert_int_equal(addr_copy(addr_dst, NULL), -1);
    assert_int_equal(addr_copy(NULL, &addr_src), -1);

    assert_int_equal(msg2addr(&addr_src, msg), 0);

    // Set some values just for testing
    addr_src.index = 1;     // This value should not be copied
    addr_src.dyn_index = 2; // This value should not be copied
    addr_src.scope = 3;
    memcpy(addr_src.peer, str2addr(AF_INET, "192.168.1.1", NULL), addrlen(AF_INET));
    addr_src.preferred_lifetime = 1000;
    addr_src.valid_lifetime = 2000;

    assert_int_equal(addr_copy(addr_dst, &addr_src), 0);
    assert_int_equal(addr_dst->created_timestamp, addr_src.created_timestamp);
    assert_int_equal(addr_dst->dyn_index, 0); // Should not be set by this function
    assert_int_equal(addr_dst->index, 0);     // Should not be set by this function
    assert_int_equal(addr_dst->family, addr_src.family);
    assert_int_equal(addr_dst->flags, addr_src.flags);
    assert_int_equal(addr_dst->link, addr_src.link); // The dummy100 link has index 48 in the message
    assert_int_equal(addr_dst->prefixlen, addr_src.prefixlen);
    assert_int_equal(addr_dst->scope, addr_src.scope);
    assert_int_equal(addr_dst->preferred_lifetime, addr_src.preferred_lifetime);
    assert_int_equal(addr_dst->valid_lifetime, addr_src.valid_lifetime);
    assert_int_equal(addr_dst->updated_timestamp, addr_src.updated_timestamp);
    assert_string_equal(addr2str(addr_dst->family, addr_dst->address, false), addr2str(addr_src.family, addr_src.address, false));
    assert_string_equal(addr2str(addr_dst->family, addr_dst->peer, false), addr2str(addr_src.family, addr_src.address, false));

    addr_destroy(addr_dst, false);
    handle_events();
}

/*
 * Start the mercy time and let it run out
 * Expectations:
 *  - After the timer runs out, the addr should be scheduled for removing
 */
void test_addr_expireMercy(UNUSED void** state) {
    addr_t* addr = addr_create();
    assert_true(addr->active);
    amxp_timer_start(addr->mercytimer, 0);
    read_sig_alarm();
    assert_false(addr->active);
    handle_events();
}

/*
 * Make sure the translation from addr to transaction works properly
 * Expectations:
 *  - When provided with invalid inputs the function should fail
 *  - When provided with valid inputs the transactions should contain all values
 *      - When the converters are not initialized some values will be an empty string
 *      - When the converters are initialized the values should be correctly converted
 */
void test_create_transaction_from_addr(UNUSED void** state) {
    int rv = -1;
    addr_t* addr = addr_create();
    amxd_trans_t* trans = NULL;
    amxc_var_t* expected_data = NULL;

    addr->active = true;
    memcpy(addr->address, str2addr(AF_INET, "192.168.100.1", NULL), addrlen(AF_INET));
    memcpy(addr->peer, str2addr(AF_INET, "192.168.1.1", NULL), addrlen(AF_INET));
    addr->prefixlen = 24;
    addr->dyn_index = 10;
    addr->family = AF_INET;
    addr->flags = IFA_F_SECONDARY | IFA_F_NODAD | IFA_F_OPTIMISTIC | IFA_F_DADFAILED | IFA_F_HOMEADDRESS | IFA_F_DEPRECATED | IFA_F_TENTATIVE | IFA_F_PERMANENT;
    addr->link = 100;
    addr->scope = RT_SCOPE_UNIVERSE;
    addr->preferred_lifetime = 1000;
    addr->valid_lifetime = 2000;
    addr->created_timestamp = 20;
    addr->updated_timestamp = 60;

    // Call with invalid input
    trans = create_transaction_from_addr(NULL, true);
    assert_null(trans);
    trans = create_transaction_from_addr(NULL, false);
    assert_null(trans);

    // Call with valid input
    expected_data = read_json_from_file("test_data/trans_new_addr_no_converters.json");
    trans = create_transaction_from_addr(addr, true);
    rv = validate_transaction(trans, "NetDev.Link.100.IPv4Addr.", action_object_add_inst, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    expected_data = read_json_from_file("test_data/trans_write_addr_no_converters.json");
    trans = create_transaction_from_addr(addr, false);
    rv = validate_transaction(trans, "NetDev.Link.100.IPv4Addr.dyn10.", action_object_write, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    addr_init_converters();

    expected_data = read_json_from_file("test_data/trans_new_addr.json");
    trans = create_transaction_from_addr(addr, true);
    rv = validate_transaction(trans, "NetDev.Link.100.IPv4Addr.", action_object_add_inst, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    expected_data = read_json_from_file("test_data/trans_write_addr.json");
    trans = create_transaction_from_addr(addr, false);
    rv = validate_transaction(trans, "NetDev.Link.100.IPv4Addr.dyn10.", action_object_write, expected_data);
    assert_int_equal(rv, 0);
    amxd_trans_delete(&trans);
    amxc_var_delete(&expected_data);

    addr_cleanup();
}

/*
 * Provide the handleNewAddr function with invalid input parameters
 * Expectations:
 *  - The function should return '-1' and no error should be reported by Valgrind
 */
void test_handleNewAddr_invalid_input(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();

    msg->nlmsg = NULL;
    assert_int_equal(handleNewAddr(msg), -1);
    msg = NULL;
    assert_int_equal(handleNewAddr(msg), -1);
}

/*
 * Provides the handleNewAddr with an invalid scenario
 *  - The addr test data is coming from doing "ip a add 192.168.100.1/24 dev dummy100" manually and storing the msg in this test
 *  - No valid link exist to which to add the address
 * Expectations:
 *  - Before calling the handleNewAddr no matching address should exist
 *  - handleNewAddr should return with issue
 *  - An active addr struct should be found directly after calling the handleNewAddr (no dm instance yet)
 *  - After handling the events
 *      - The addr structure should no longer be accessed since it should be invalid
 *      - The addr should no longer be found by the addr_find function
 *      - No addr instance should exist in the datamodel
 */
void test_handleNewAddr_link_does_not_exist(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    addr_t ws;
    addr_t* addr = NULL;
    amxd_object_t* addr_obj = NULL;
    int dyn_index = 0;

    //Message coming from "ip a add 192.168.100.1/24 dev dummy100"
    msg->nlmsg = (struct nlmsghdr*) "\x54\x00\x00\x00\x14\x00\x00\x00\xf4\x62\x56\x63\x7f\x16\x00\x00\x02\x18\x80\x00\x30\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x01\x08\x00\x02\x00\xc0\xa8\x64\x01\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x08\x00\x80\x00\x00\x00\x14\x00\x06\x00\xff\xff\xff\xff\xff\xff\xff\xff\xe7\xdb\x0e\x00\xe7\xdb\x0e\x00";
    msg->buflen = 8192;
    msg->hdrlen = 8;

    // Make sure the address does not yet exist
    assert_int_equal(msg2addr(&ws, msg), 0);
    addr = addr_find(&ws);
    assert_null(addr);

    // Call handler for addr where the link does not exist
    assert_int_equal(handleNewAddr(msg), 0); // handle function should be successful even if the link does not exist

    // The addr should exist as soon as the handler is called
    addr = addr_find(&ws);
    assert_non_null(addr);
    assert_true(addr->active);
    // Should not exist in the DM until events are handled
    dyn_index = addr->dyn_index; // Save this so it can be used after handling events since we expect addr to be removed then
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", addr->dyn_index);
    assert_null(addr_obj);

    handle_events();
    // The addr struct should be removed since the transaction should fail
    addr = addr_find(&ws);
    assert_null(addr);
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", dyn_index);
    assert_null(addr_obj);

}

/*
 * Provides the handleNewAddr with a valid scenario
 *  - A valid dummy link is provided by calling the add_test_link function
 *  - The addr test data is coming from doing "ip a add 192.168.100.1/24 dev dummy100" manually and storing the msg in this test
 * Expectations:
 *  - The test_link is added without a problem (this must be done before storing the address msg otherwise the add_test_link function will overwrite it)
 *  - Before calling the handleNewAddr no matching address should exist
 *  - handleNewAddr should return without issue
 *  - An active addr struct should be found directly after calling the handleNewAddr (no dm instance yet)
 *  - After handling the events
 *      - The addr struct should still be active
 *      - An addr instance should exist in the datamodel
 *      - The data should match
 *  - When calling the handleNewAddr for the second time (update message)
 *      - The addr struct should be updated
 *      - The existing dm instance should be updated
 *  - When calling addr_destroy with the option set to true
 *      - Right after calling:
 *          - The addr should become inactive
 *          - The addr should no longer be found by the addr_find function
 *          - The addr instance should remain in the DM
 *      - After handling the events:
 *          - The addr structure should no longer be accessed since it will be invalid
 *          - The addr instance should be removed from the DM
 */
void test_handleNewAddr(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    addr_t ws;
    addr_t* addr = NULL;
    link_t* test_link = NULL;
    amxd_object_t* addr_obj = NULL;
    int dyn_index = 0;
    amxc_var_t params;

    // This shares the same rtnlmsg_instance so make sure to set the message for the test after adding the test link
    test_link = add_test_link();
    assert_non_null(test_link);

    //Message coming from "ip a add 192.168.100.1/24 dev dummy100"
    msg->nlmsg = (struct nlmsghdr*) "\x54\x00\x00\x00\x14\x00\x00\x00\xf4\x62\x56\x63\x7f\x16\x00\x00\x02\x18\x80\x00\x30\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x01\x08\x00\x02\x00\xc0\xa8\x64\x01\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x08\x00\x80\x00\x00\x00\x14\x00\x06\x00\xff\xff\xff\xff\xff\xff\xff\xff\xe7\xdb\x0e\x00\xe7\xdb\x0e\x00";
    msg->buflen = 8192;
    msg->hdrlen = 8;

    // Make sure the address does not yet exist
    assert_int_equal(msg2addr(&ws, msg), 0);
    addr = addr_find(&ws);
    assert_null(addr);

    // Call handler to add the new addres
    assert_int_equal(handleNewAddr(msg), 0);

    // The addr should exist as soon as the handler is called
    addr = addr_find(&ws);
    assert_non_null(addr);
    assert_true(addr->active);
    // Should not exist in the DM until events are handled
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Route.dyn%d", addr->dyn_index);
    assert_null(addr_obj);

    handle_events();
    addr = addr_find(&ws);
    assert_non_null(addr);
    assert_true(addr->active);
    assert_string_equal(addr2str(addr->family, addr->address, false), "192.168.100.1");
    dyn_index = addr->dyn_index;
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", addr->dyn_index);
    assert_non_null(addr_obj);

    assert_int_equal(addr->index, addr_obj->index);
    assert_int_equal(addr->link, test_link->index);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(addr_obj, &params, amxd_dm_access_protected);

    assert_true(addr_obj == addr->object);
    assert_string_equal(GET_CHAR(&params, "Address"), "192.168.100.1");
    assert_int_equal(GET_INT32(&params, "PrefixLen"), 24);
    assert_string_equal(GET_CHAR(&params, "Flags"), "permanent");
    assert_string_equal(GET_CHAR(&params, "Scope"), "global");
    assert_string_equal(GET_CHAR(&params, "TypeFlags"), "@private");
    amxc_var_clean(&params);

    // Change the PrefixLen from an existing addr from 24 to 23
    msg->nlmsg = (struct nlmsghdr*) "\x54\x00\x00\x00\x14\x00\x00\x00\xf4\x62\x56\x63\x7f\x16\x00\x00\x02\x17\x80\x00\x30\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x01\x08\x00\x02\x00\xc0\xa8\x64\x01\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x08\x00\x80\x00\x00\x00\x14\x00\x06\x00\xff\xff\xff\xff\xff\xff\xff\xff\xe7\xdb\x0e\x00\xe7\xdb\x0e\x00";
    assert_int_equal(handleNewAddr(msg), 0);
    handle_events();

    addr = addr_find(&ws);
    assert_non_null(addr);
    assert_true(addr->active);
    assert_non_null(addr->object);
    assert_int_equal(addr->prefixlen, 23);
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", addr->dyn_index);
    assert_non_null(addr_obj);
    assert_non_null(addr_obj->priv);

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxd_object_get_params(addr_obj, &params, amxd_dm_access_protected);

    assert_true(addr_obj == addr->object);
    assert_string_equal(GET_CHAR(&params, "Address"), "192.168.100.1");
    assert_int_equal(GET_INT32(&params, "PrefixLen"), 23);
    assert_string_equal(GET_CHAR(&params, "Flags"), "permanent");
    assert_string_equal(GET_CHAR(&params, "Scope"), "global");
    assert_string_equal(GET_CHAR(&params, "TypeFlags"), "@private");
    amxc_var_clean(&params);

    addr_destroy(addr, true);
    assert_false(addr->active);
    assert_null(addr->object);
    addr = addr_find(&ws);
    assert_null(addr);
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", dyn_index);
    assert_non_null(addr_obj); // Should only be removed from the datamodel after events are handled

    handle_events();

    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", dyn_index);
    assert_null(addr_obj);
}

/*
 * Provide the handleDelAddr function with invalid input parameters
 * Expectations:
 *  - The function should return '-1' and no error should be reported by Valgrind
 */
void test_handleDelAddr_invalid_input(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    assert_int_equal(handleDelAddr(NULL), -1);
    msg->nlmsg = NULL;
    assert_int_equal(handleDelAddr(msg), -1);
}

/*
 * Make sure the delete handler does not schedule addresses to be deleted that are already inactive
 * Expectations:
 *  - After calling the delete handler (expected to fail because no active addr could be found)
 *    - The instance should still exist in the DM
 *  - No mercy timer should be started so read_sig_alarm should timeout
 *    - The object value in the addr struct should NOT be set to NULL
 *    - The datamodel objects priv value should NOT be set to NULL
 *    - The instance should still be available in the DM
 *  - After handling the events
 *    - The object value in the addr struct should NOT be set to NULL
 *    - The datamodel objects priv value should NOT be set to NULL
 *    - The instance should still be available in the DM
 */
void test_handleDelAddr_inactive_addr(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    addr_t* test_addr = NULL;
    link_t* test_link = NULL;
    amxd_object_t* addr_obj = NULL;
    int dyn_index = 0;

    // This shares the same rtnlmsg_instance so make sure to set the message for the test after adding the test link
    test_link = add_test_link();
    assert_non_null(test_link);

    test_addr = add_test_addr();
    assert_non_null(test_addr);
    dyn_index = test_addr->dyn_index;

    test_addr->active = false;

    // Call the handler to remove the addr
    msg->nlmsg = (struct nlmsghdr*) "\x54\x00\x00\x00\x15\x00\x00\x00\xfc\x62\x56\x63\x80\x16\x00\x00\x02\x18\x80\x00\x30\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x01\x08\x00\x02\x00\xc0\xa8\x64\x01\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x08\x00\x80\x00\x00\x00\x14\x00\x06\x00\xff\xff\xff\xff\xff\xff\xff\xff\xe7\xdb\x0e\x00\xe7\xdb\x0e\x00";
    msg->buflen = 8192;
    msg->hdrlen = 8;
    assert_int_equal(handleDelAddr(msg), -1); // Should fail because no active addr was found
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", dyn_index);
    assert_non_null(addr_obj);
    assert_non_null(addr_obj->priv);
    assert_non_null(test_addr->object);

    read_sig_alarm(); // The timer should not be started so this should timeout
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", dyn_index);
    assert_non_null(addr_obj);
    assert_non_null(addr_obj->priv);
    assert_non_null(test_addr->object);

    handle_events();
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", dyn_index);
    assert_non_null(addr_obj);
    assert_non_null(addr_obj->priv);
    assert_non_null(test_addr->object);

    test_addr->active = true;
    addr_destroy(test_addr, true);
    handle_events();
}

/*
 * Make sure the address delete messages are handled correctly
 * Expectations:
 *  - After calling the delete handler
 *    - The addr should still be active and should still be found by addr_find
 *    - The instance should still exist in the DM
 *  - After letting the mercytimer run out
 *    - The addr should go to inactive and no longer be able to be found by addr_find
 *    - The object value in the addr struct should be set to NULL
 *    - The datamodel objects priv value should be set to NULL
 *    - The instance should still be available in the DM
 *  - After handling the events, the instance should be removed from the dm
 */
void test_handleDelAddr(UNUSED void** state) {
    rtnlmsg_t* msg = rtnlmsg_instance();
    addr_t* addr = NULL;
    addr_t* test_addr = NULL;
    link_t* test_link = NULL;
    amxd_object_t* addr_obj = NULL;
    int dyn_index = 0;

    // This shares the same rtnlmsg_instance so make sure to set the message for the test after adding the test link
    test_link = add_test_link();
    assert_non_null(test_link);

    test_addr = add_test_addr();
    assert_non_null(test_addr);
    dyn_index = test_addr->dyn_index;

    // Call the handler to remove the addr
    msg->nlmsg = (struct nlmsghdr*) "\x54\x00\x00\x00\x15\x00\x00\x00\xfc\x62\x56\x63\x80\x16\x00\x00\x02\x18\x80\x00\x30\x00\x00\x00\x08\x00\x01\x00\xc0\xa8\x64\x01\x08\x00\x02\x00\xc0\xa8\x64\x01\x0d\x00\x03\x00\x64\x75\x6d\x6d\x79\x31\x30\x30\x00\x00\x00\x00\x08\x00\x08\x00\x80\x00\x00\x00\x14\x00\x06\x00\xff\xff\xff\xff\xff\xff\xff\xff\xe7\xdb\x0e\x00\xe7\xdb\x0e\x00";
    msg->buflen = 8192;
    msg->hdrlen = 8;
    assert_int_equal(handleDelAddr(msg), 0);
    addr = addr_find(test_addr);
    assert_non_null(addr);
    assert_true(addr->active);
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", dyn_index);
    assert_non_null(addr_obj);
    assert_non_null(addr_obj->priv);

    read_sig_alarm(); // Let the mercytimer run out
    assert_false(addr->active);
    assert_null(addr->object);
    addr = addr_find(test_addr);
    assert_null(addr);
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", dyn_index);
    assert_non_null(addr_obj);
    assert_null(addr_obj->priv);

    handle_events();
    addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100.IPv4Addr.dyn%d", dyn_index);
    assert_null(addr_obj);
}


/* Overload the system function call by the plugin
   only valid for this test */
unsigned int if_nametoindex(UNUSED const char* ifname) {
    unsigned int rv_index = 48; // id if intf dummy100
    return rv_index;
}

ssize_t recv(UNUSED int sockfd, void* buf, UNUSED size_t len, UNUSED int flags) {
    struct nlmsghdr hdr;
    ssize_t rv_len = sizeof(struct nlmsghdr);
    memset(&hdr, 0, rv_len);
    hdr.nlmsg_len = rv_len;
    hdr.nlmsg_type = NLMSG_NOOP;
    memcpy(buf, &hdr, rv_len);
    return rv_len;
}

ssize_t sendmsg(UNUSED int socket, UNUSED const struct msghdr* message, UNUSED int flags) {
    struct iovec* iov = message->msg_iov;
    rtnlmsg_t* msg = rtnlmsg_instance();
    uint8_t type;

    msg->nlmsg = iov->iov_base;
    msg->buflen = iov->iov_len;
    msg->hdrlen = 8;

    type = rtnlmsg_type(msg);

    switch(type) {
    case RTM_NEWADDR:
        assert_int_equal(handleNewAddr(msg), 0);
        break;
    case RTM_DELADDR:
        assert_int_equal(handleDelAddr(msg), 0);
        read_sig_alarm();
        break;
    case RTM_GETADDR:
        break;
    default: break;
    }

    // Call handler to delete or add the address
    handle_events();

    return 1;
}

void test_rpc_AddIPAddress(UNUSED void** state) {
    amxd_object_t* intf_root_obj = NULL;
    amxd_object_t* intf_addr_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    uint32_t number_entries = 0;
    cstring_t ip = NULL;

    link_t* test_link = NULL;
    test_link = add_test_link();
    assert_non_null(test_link);

    intf_root_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100");
    assert_non_null(intf_root_obj);

    number_entries = amxd_object_get_value(uint32_t, intf_root_obj, "IPv4AddrNumberOfEntries", NULL);
    assert_int_equal(number_entries, 0);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "IPAddress", "192.168.100.2");
    amxc_var_add_key(uint32_t, &args, "PrefixLen", 24);
    amxc_var_add_key(bool, &args, "IPv4", true);
    assert_int_equal(amxd_object_invoke_function(intf_root_obj, "AddIPAddress", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    number_entries = amxd_object_get_value(uint32_t, intf_root_obj, "IPv4AddrNumberOfEntries", NULL);
    assert_int_equal(number_entries, 1);

    intf_addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.48.IPv4Addr.1");
    assert_non_null(intf_addr_obj);
    ip = amxd_object_get_value(cstring_t, intf_addr_obj, "Address", NULL);
    assert_non_null(ip);
    assert_string_equal(ip, "192.168.100.2");
    free(ip);
}

void test_rpc_DeleteIPAddress(UNUSED void** state) {
    amxd_object_t* intf_root_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    link_t* test_link = NULL;
    addr_t* test_addr = NULL;
    uint32_t number_entries = 0;

    test_link = add_test_link();
    assert_non_null(test_link);

    test_addr = add_test_addr();
    assert_non_null(test_addr);

    intf_root_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100");
    assert_non_null(intf_root_obj);

    number_entries = amxd_object_get_value(uint32_t, intf_root_obj, "IPv4AddrNumberOfEntries", NULL);
    assert_int_equal(number_entries, 1);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "IPAddress", "192.168.100.1");
    amxc_var_add_key(uint32_t, &args, "PrefixLen", 24);
    amxc_var_add_key(bool, &args, "IPv4", true);
    assert_int_equal(amxd_object_invoke_function(intf_root_obj, "DeleteIPAddress", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    number_entries = amxd_object_get_value(uint32_t, intf_root_obj, "IPv4AddrNumberOfEntries", NULL);
    assert_int_equal(number_entries, 0);
}

void test_rpc_UpdateIPAddress(UNUSED void** state) {
    amxd_object_t* intf_root_obj = NULL;
    amxd_object_t* intf_addr_obj = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    unsigned int dyn_index = 0;
    link_t* test_link = NULL;
    addr_t* test_addr = NULL;
    uint32_t number_entries = 0;
    cstring_t ip = NULL;

    test_link = add_test_link();
    assert_non_null(test_link);

    test_addr = add_test_addr();
    assert_non_null(test_addr);
    dyn_index = test_addr->dyn_index;

    // Initial test: change from default ip 192.168.100.1 -> 192.168.100.2
    intf_root_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.dummy100");
    assert_non_null(intf_root_obj);

    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "OldIPAddress", addr2str(test_addr->family, test_addr->address, false));
    amxc_var_add_key(uint32_t, &args, "OldPrefixLen", test_addr->prefixlen);
    amxc_var_add_key(cstring_t, &args, "IPAddress", "192.168.100.2");
    amxc_var_add_key(uint32_t, &args, "PrefixLen", 24);
    amxc_var_add_key(bool, &args, "IPv4", true);
    assert_int_equal(amxd_object_invoke_function(intf_root_obj, "UpdateIPAddress", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    number_entries = amxd_object_get_value(uint32_t, intf_root_obj, "IPv4AddrNumberOfEntries", NULL);
    assert_int_equal(number_entries, 1);

    intf_addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.48.IPv4Addr.dyn%d", dyn_index + 1);
    assert_non_null(intf_addr_obj);
    ip = amxd_object_get_value(cstring_t, intf_addr_obj, "Address", NULL);
    assert_non_null(ip);
    assert_string_equal(ip, "192.168.100.2");
    free(ip);

    // specific test: set the same ip 192.168.100.2 -> 192.168.100.2
    amxc_var_init(&ret);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "OldIPAddress", "192.168.100.2");
    amxc_var_add_key(uint32_t, &args, "OldPrefixLen", 24);
    amxc_var_add_key(cstring_t, &args, "IPAddress", "192.168.100.2");
    amxc_var_add_key(uint32_t, &args, "PrefixLen", 24);
    amxc_var_add_key(bool, &args, "IPv4", true);
    assert_int_equal(amxd_object_invoke_function(intf_root_obj, "UpdateIPAddress", &args, &ret), amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    number_entries = amxd_object_get_value(uint32_t, intf_root_obj, "IPv4AddrNumberOfEntries", NULL);
    assert_int_equal(number_entries, 1);

    intf_addr_obj = amxd_dm_findf(test_setup_dm(), "NetDev.Link.48.IPv4Addr.dyn%d", dyn_index + 1);
    assert_non_null(intf_addr_obj);
    ip = amxd_object_get_value(cstring_t, intf_addr_obj, "Address", NULL);
    assert_non_null(ip);
    assert_string_equal(ip, "192.168.100.2");
    free(ip);
}
